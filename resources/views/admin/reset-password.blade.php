<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    @include('include.admin.head')
</head>
<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

    <div id="main-wrapper" data-navbarbg="skin6" data-theme="light" data-layout="vertical" data-sidebartype="full" data-boxed-layout="full">

        @include('include.admin.header')

        <div class="page-wrapper">

            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Add Event</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Reset Password</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="card card-body">
                            <form class="form-horizontal m-t-30" id='change_password_form'>
                                <input type='hidden' name='_token' id='token' value='{{csrf_token()}}'/>

                                <div class="form-group">
                                    <label>Old Password</label>
                                    <input type="password" name='old_password' placeholder="Old Password" class="form-control" required="">
                                </div>

                                <div class="form-group">
                                    <label>New Password</label>
                                    <input type="password" name="new_password" placeholder="New Password" class="form-control" required="">
                                </div>

                                <div class="form-group">
                                    <label>Re-Type Password</label>
                                    <input name="confirm_password" type="password" placeholder="Re-Type Password" class="form-control">
                                </div>

                                

                                <div class="form-group">
                                    <button type="submit" class="btn btn-info">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

            <footer class="footer text-center">
                All Rights Reserved.
                <!-- <a href="https://wrappixel.com">WrapPixel</a>. -->
            </footer>
        </div>

    </div>

    @include('include.admin.footer')
</body>
   <script> 
$('#change_password_form').submit(function(e){
 e.preventDefault();
//$('.main-div2').css({'display':'block'});
   $.ajax({
        url:path+'admin/changePassword',
        type:'post',
        data:new FormData(this),
        contentType:false, 
        processData:false,
        success:function(data,status)
        {
         // $('.main-div2').css({'display':'none'});
         if(data == 1)
         {
                swal({
                  title: 'Success!',
                  text: "Password has been changed successfully",
                  type: 'success',
                  showCancelButton: false,
                  confirmButtonColor: '#3085d6',
                  confirmButtonText: 'Ok'
              }).then(function (result) {
                  if (result.value) {
                    window.location.href = path+'admin/home';
                  }
              });

        }else if(data == 2)
         {
            $("#error_old_password").text('');
               $("#error_new_password").text('');
               $("#error_confirm_password_edit").text(''); 
                 $('#error_old_password').text('Old password is incorrect');
         }
        },
        error:function(data,status)
        {$('.main-div2').css({'display':'none'});
         var error = $.parseJSON(data.responseText);
                $("#error_old_password").text('');
               $("#error_new_password").text('');
               $("#error_confirm_password_edit").text(''); 
               if(typeof(error['old_password']) != "undefined" && error['old_password'] !== null) {
               $("#error_old_password").text(error['old_password'][0]);
            }
            if(typeof(error['new_password']) != "undefined" && error['new_password'] !== null) {
               $("#error_new_password").text(error['new_password'][0]);
            }
            if(typeof(error['confirm_password']) != "undefined" && error['confirm_password'] !== null) {
               $("#error_confirm_password_edit").text(error['confirm_password'][0]);
            }
        }
   });
});
</script>

</html>