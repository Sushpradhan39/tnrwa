<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    @include('include.admin.head')
</head>
<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

    <div id="main-wrapper" data-navbarbg="skin6" data-theme="light" data-layout="vertical" data-sidebartype="full" data-boxed-layout="full">

        @include('include.admin.header')

        <div class="page-wrapper">

            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Edit Event</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Edit Event</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="card card-body">
                            <div class="row">
                              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <form class="form-horizontal m-t-30" id='edit_event_form'>
                                  <input type='hidden' name='_token' id='token' value='{{csrf_token()}}'/>
                                  <input type="hidden" name="event_id" value="{{$event->id}}">
                                  <div class="form-group">
                                      <label>Title</label>
                                      <input type="text" class="form-control" name="title" required="" value="{{$event->title}}">
                                  </div>

                                  <div class="form-group">
                                      <label>Sub Title</label>
                                      <input type="text" class="form-control" name="sub_title"  value="{{$event->sub_title}}">
                                  </div>

                                  <div class="form-group">
                                      <label>Description</label>
                                      <textarea class="form-control" rows="4" cols="50" name="description" required="">{{$event->description}}</textarea>
                                  </div>

                                  <div class="form-group">
                                      <label>Vedio Link</label>
                                      <textarea class="form-control" rows="4" cols="50" name="vedio_links" >{{$event->vedio_links}}</textarea>
                                  </div>

                                  <div class="form-group">
                                      <label>Display Image</label>
                                      <input type="file" class="form-control" name="display_image" >
                                  </div>
                                
                                   <div class="form-group">
                                    <label>Drive link</label>
                                    <input type="text" class="form-control" name="drive_link" value="{{$event->drive_link}}">
                                    </div>

                                  <div class="form-group">
                                      <button type="submit" class="btn btn-info">Update</button>
                                  </div>
                               </form>
                            </div>

                           <!--  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                              <form id="add_other_images_form">
                                <input type='hidden' name='_token' id='token' value='{{csrf_token()}}'/>
                                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <button type="button" class="btn add_img">Add New Image</button>
                                    <div style="float: left;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 add_img_panel">
                                      <div style="float: left;" class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <input type="hidden" name="event_id" value="{{$event->id}}">
                                       <input type="file" class="form-control" name="other_images[]" multiple required="">
                                      </div>
                                      <div style="text-align: center;float: left;" class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <button class="btn btn-info" type="submit">Add</button>
                                      </div>
                                    </div>
                                  </div>
                                  </form>
                                    <div style="float: left;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                      <table class="img_table table">
                                          <thead>
                                              <th>Image</th>
                                              <th>Action</th>
                                          </thead>
                                          <tbody>
                                            @if(@$event->images)
                                            @foreach($event->images as $key => $image)
                                            <tr>
                                              <td><img width="80" src="{{ asset('assets/images/events/'.$image->image_name) }}" /></td>
                                              <td id="delete_image" image_id="{{@$image->id}}" style="vertical-align: middle;"><button title="Delete" class="btn btn-danger"><i class="m-r-10 mdi mdi-delete"></i></button></td>
                                            </tr>
                                             @endforeach
                                              @endif
                                          </tbody>
                                      </table>
                                    </div>
                                </div> -->
                              </div>
                        </div>
                    </div>
                </div>
 <input type="hidden" id="token" name="_token" value="{{csrf_token()}}"/>
            </div>

            <footer class="footer text-center">
                All Rights Reserved.
                <!-- <a href="https://wrappixel.com">WrapPixel</a>. -->
            </footer>
        </div>

    </div>

    @include('include.admin.footer')
</body>
<script type="text/javascript">
  $(function(){

    $('.img_table').DataTable({
      "searching": false,
      "bLengthChange": false,
        "scrollY": "450px",
        "paging": false,
    });

    $(".add_img").click(function(){
      $(".add_img_panel").toggle()
    })
  })
</script>
<script type="text/javascript">
   var token = $('#token').val();
       $(document).on('click','#delete_image',function(){
        var image_id = $(this).attr('image_id');

              swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then(function (result) {
            if (result.value) {
        $.ajax({
           url:path+'admin/delete_event_image',
           type:'delete',
           data:{_token:token,image_id:image_id},
           success:function(data,status)
                 {

                         swal({
                              title: 'Deleted',
                              text: "Selected product has been deleted.",
                              type: 'success',
                              showCancelButton: false,
                              confirmButtonColor: '#3085d6',
                              confirmButtonText: 'Ok'
                            }).then(function (result) {
                              if (result.value) {
                                location.reload();
                              }
                         });
                }
        });
         }
           });

       });

   </script>
<script type="text/javascript">
    $('#add_other_images_form').submit(function(e){
          e.preventDefault();

          $.ajax({
           url:path+'admin/add_other_event_images',
           type:'post',
           data:new FormData(this),
           contentType:false,
           processData:false,
           success:function(data,status)
           {
            swal({
             title: 'Success',
                                        
                 type: 'success',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Ok'
                }).then(function (result) 
                {
                  if (result.value) {
                  location.reload();
                                    }
            });
           }
       });
    });
</script>

<script type="text/javascript">
    $('#edit_event_form').submit(function(e){
          e.preventDefault();

          $.ajax({
           url:path+'admin/update-event',
           type:'post',
           data:new FormData(this),
           contentType:false,
           processData:false,
           success:function(data,status)
           {
             swal({
             title: 'Success',
                                        
                 type: 'success',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Ok'
                }).then(function (result) 
                {
                  if (result.value) {
                  location.reload();
                                    }
            });
           }
       });
    });
</script>
</html>
