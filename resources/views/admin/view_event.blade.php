<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    @include('include.admin.head')
</head>
<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

    <div id="main-wrapper" data-navbarbg="skin6" data-theme="light" data-layout="vertical" data-sidebartype="full" data-boxed-layout="full">

        @include('include.admin.header')

        <div class="page-wrapper">

            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">View Events</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Basic Table</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="card card-body">

                            <div class="table-responsive-sm">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Title</th>
                                            <th scope="col">Sub Title</th>

                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php $count = 0; ?>
                                        @foreach($events as $key => $event)
                                        <tr>
                                            <th scope="row">{{$count += 1}}</td>
                                            <td>{{$event->title}}</td>
                                            <td>{{$event->sub_title}}</td>
                                            <td>
                                                <a href="{{ asset('admin/edit-event/'.@$event->id) }}" style="text-align: center" class="btn btn-info"><i class="m-r-10 mdi mdi-lead-pencil"></i></a>
                                                <button id="delete_event" event_id="{{@$event->id}}" style="text-align: center" class="btn btn-danger"><i class="m-r-10 mdi mdi-delete"></i></button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
               <input type="hidden" id="token" name="_token" value="{{csrf_token()}}"/>
            </div>

            <footer class="footer text-center">
                All Rights Reserved
                <!-- <a href="https://wrappixel.com">WrapPixel</a>. -->
            </footer>

        </div>

    </div>

    @include('include.admin.footer')
</body>

<script type="text/javascript">
   var token = $('#token').val();
       $(document).on('click','#delete_event',function(){
        var event_id = $(this).attr('event_id');
              swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then(function (result) {
            if (result.value) {
        $.ajax({
           url:path+'admin/delete_event',
           type:'delete',
           data:{_token:token,event_id:event_id},
           success:function(data,status){

                         swal({
                              title: 'Deleted',
                              text: "Selected product has been deleted.",
                              type: 'success',
                              showCancelButton: false,
                              confirmButtonColor: '#3085d6',
                              confirmButtonText: 'Ok'
                            }).then(function (result) {
                              if (result.value) {
                                location.reload();
                              }
                         });
                }
        });
         }
           });

       });

   </script>
</html>
