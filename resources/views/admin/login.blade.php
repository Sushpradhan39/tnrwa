<!DOCTYPE html>
<html>
<head>
    @include('include.admin.head')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

    <script>
$(document).ready(function(){


      $('.btn-forgot').click(function(){
      	// alert('hi');
        $('.login-form-div').addClass('hide');
        $('.forgot-form-div').removeClass('hide');

      });

      $('.send-otp').click(function(){
        $('.email-div').addClass('hide');
        $('.otp-div').removeClass('hide');
      });

      $('.verify-otp').click(function(){
        $('.otp-div').addClass('hide');
        $('.password-div').removeClass('hide');
      });
      });
   </script>
   <!-- <script>
   $(document).ready(function(){
   	 $('.btn2').click(function(){
   	 	alert('hi');
   	 });
   });
   </script> -->
<style>
body{background-color: #eaae61}
.login-form-div{
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  width: 450px;
  height: 400px;
  margin-top: auto;
  margin-bottom: auto;
  margin-left: auto;
  margin-right: auto;
  border: 1px solid #34ade6;
  background-color: #fff;
  padding-top: 5%;
}
.login-logo{
  text-align: center;
  margin-bottom: 11%;
}
.error
{text-align:center;}
.right input,
.right input::-webkit-input-placeholder{
  font-size: 17px;
}
.right input {
    display: inline-block;
        width: 100%;
    height: 38px;
    border-radius: 0px;
    /* border-top: none; */
    /* border-left: none; */
    /* border-right: none; */
    box-shadow: 1px 1px 1px transparent;
}
.btn {
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
}
.btn-login {
      width: 45%;
    background-color: #28b779;
/*    border: 1px solid #40ddbf;*/
    color: #fff;
    text-transform: uppercase;
    border-radius: 0;
/*    border-bottom: 3px solid #28bb9f;*/
}
.btn-login:hover
{color: white;}

</style>
 </head>
<body class="login-body">

<div class="fixed-nav">
    <div class="container-fluid">
<div class="login-form-div col-lg-8 col-lg-offset-3 col-md-8 col-sm-8 col-xs-12 no-padding">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 no-padding padd login-form">
				<form class="form-horizontal" id="admin_auth_form" >

					<!-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 left">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left-content">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ig">
								<h3>Innovative Technology</h3>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 list no-padding">
								<ol>
									<li>Manage your Products</li>
									<li>Create Good Result</li>
									<li>Check Delivery</li>
								</ol>
							</div>
						</div>
					</div>	 -->

					<div class="col-lg-12 col-md-6 col-sm-6 col-xs-6 right login">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding login-logo">
                    <h3>Admin Panel</h3>
								</div>
							</div>
	                           <input type='hidden' name='_token' value='{{csrf_token()}}'>
							<div class="form-group">
							 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mpadd">
								<span class="user-login">
									<i class="fa fa-user-o"></i>
									</span>
									<input type="text" class="form-control" name="mobile_number" placeholder="Mobile Number">
							 </div>
								<div class="custom-tool-tip">
									<p class="error" id='error_mobile_number'></p>
								</div>
							</div>

							<div class="form-group">
							 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mpadd">
								    <span class="user-pass">
									<i class="fa fa-lock"></i>
									</span>
									<input type="password" class="form-control" name="password" placeholder="Password">
							 </div>
								<div class="custom-tool-tip">
									<p class="error" id='error_password'></p>
								</div>
							</div>

							<div class="form-group">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" align="center">
									<button id="test" type='submit' class="btn btn-login"><span style="display:none;" class="admin-login-loading"><img src="{{ asset('assets/images/loading.svg') }}"></span>&nbsp;Login</button>
								</div>
                <div class="custom-tool-tip">
                        <p class="error" id='error_login' style="text-align: center;"></p>
                </div>
							</div>

							<div class="form-group">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding" align="center">
									<!-- <a href="{{ asset('forgetpassword') }}">Forgot password?</a> -->
							<!-- 		   <p><button type="button" class="btn btn-forgot"><i class="fa fa-lock"></i>&nbsp;Forgot Password?</button></p> -->


								</div>
							</div>


						</div>
					</div>

				</form>

			</div>
		</div>
	</div>


</div>

</div>


</body>
<script type="text/javascript">
$('#admin_auth_form').submit(function(e){
e.preventDefault();
//$('.admin-login-loading').show();
  $.ajax({
    url:path+'admin/auth',
    type:'post',
    data:new FormData(this),
    contentType:false,
    processData:false,
    success:function(data,status){

  //    $('.admin-login-loading').hide();
          if(data == 1){
              window.location.href = path+'admin/view-event';
          }else{
              $("#error_mobile_number").text('');
             $("#error_password").text('');
              $('#error_login').text('Mobile no & Password do not match');
       }
   },
   error:function(data)
   {
// $('.admin-login-loading').hide();
      var error = $.parseJSON(data.responseText);
    $("#error_mobile_number").text('');
    $("#error_password").text('');
    $('#error_login').text('');
        if(typeof(error['mobile_number']) != "undefined" && error['mobile_number'] !== null) {
        $("#error_mobile_number").text(error['mobile_number'][0]);
    }

    if(typeof(error['password']) != "undefined" && error['password'] !== null) {
        $("#error_password").text(error['password'][0]);
    }
   }
  });
});
</script>

</html>
