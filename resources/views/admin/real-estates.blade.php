<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    @include('include.admin.head')
    <style>
    .card .overview {
      max-height: 60px;
      border-bottom: 1px solid #D1D1D1;
      padding: 10px;
      text-align: center;
    }
    .card .overview h3 {
        font-size: 16px;
        font-weight: 600;
        margin: 0;
      }

      .card .overview h5 {
      font-size: 12px;
      font-weight: 400;
      margin: 5px 0 0;
    }

    .solid-border-right {
      border-right: 1px solid #f4f6f6;
    }

    .rent-property-card .detail-summary .summary-row .info-card .semi-bold {
      margin: 0;
      color: #707070;
      font-weight: 600;
    }

    .card .detail-summary {
      border-style: double;
      border-color: #E1E1E1;
      margin-right: 10px;
    }

    .card .detail-summary h5 {
      font-size: 14px;
      font-weight: 500;
      margin-top: 20px;
      display: inline-block;
    }

    .rent-property-card .detail-summary .summary-row .info-card {
      padding-top: 10px;
      padding-left: 45px;
    }
    .card .detail-summary .summary-row .solid-border-right {
      border-right: 1px solid #D1D1D1;
      min-height: 60px;
      max-height: 60px;
    }
    .card .detail-summary .summary-row .property-facing {
      background-position: -14px -265px;
    }

    .item-slick.slick-slide.slick-current.slick-active {
    outline: none !important;
    }

    .slider-for {
    margin-bottom: 15px;
    }
    .slider-for img {
    width: 100%;
    min-height: 100%;
    }

    .slider-nav {
    margin: auto;
    }

    .slick-slide.slick-current {
      position: absolute !important;
      left: 0 !important;
    }

    .slick-slide {
      width: 100% !important;
      position: absolute !important;
      left: 0 !important;
    }

    .slider-nav .item-slick {
    max-width: 240px;
    margin-right: 15px;
    outline: none !important;
    cursor: pointer;
    }
    .slider-nav .item-slick img {
    max-width: 100%;
    background-size: cover;
    background-position: center;
    }

    .slick-arrow {
      position: absolute;
      top: 10%;
      z-index: 1111;
      font-size: 30px;
      padding: 7px;
      color: #fff;
    }

    .slick-prev {
    left: 0;
    }

    .slick-next {
    right: 0;
    }

    .mfp-zoom-out-cur, .mfp-zoom-out-cur .mfp-image-holder .mfp-close:hover {
    cursor: pointer;
    }

    .mfp-container:hover {
    cursor: default;
    }

    .image-source-link {
    color: #98C3D1;
    }

    .mfp-with-zoom.mfp-bg {
    opacity: 0;
    transition: all 0.3s ease-out;
    }

    .mfp-with-zoom.mfp-ready .mfp-container {
    opacity: 1;
    }

    .mfp-with-zoom.mfp-ready.mfp-bg {
    opacity: 0.8;
    }

    .mfp-with-zoom.mfp-removing.mfp-bg {
    opacity: 0;
    }

    /*BLUR background */
    * {
    transition: filter 0.25s ease;
    }

    .mfp-wrap ~ * {
    filter: blur(5px);
    }

    .mfp-ready .mfp-figure {
    opacity: 0;
    }

    /* start state */
    /* animate in */
    /* animate out */
    .mfp-zoom-in .mfp-figure, .mfp-zoom-in .mfp-iframe-holder .mfp-iframe-scaler {
    opacity: 0;
    transition: all 0.3s ease-out;
    transform: scale(0.95);
    }

    .mfp-zoom-in .mfp-preloader {
    opacity: 0;
    transition: all 0.3s ease-out;
    }

    .mfp-zoom-in.mfp-image-loaded .mfp-figure, .mfp-zoom-in.mfp-ready .mfp-iframe-holder .mfp-iframe-scaler {
    opacity: 1;
    transform: scale(1);
    }

    .mfp-zoom-in.mfp-ready .mfp-preloader {
    opacity: 0.8;
    }

    .mfp-zoom-in.mfp-removing .mfp-figure, .mfp-zoom-in.mfp-removing .mfp-iframe-holder .mfp-iframe-scaler {
    transform: scale(0.95);
    opacity: 0;
    }

    .mfp-zoom-in.mfp-removing .mfp-preloader {
    opacity: 0;
    }

    .mfp-iframe-scaler {
    overflow: visible;
    }

    .mfp-zoom-out-cur {
    cursor: auto;
    }

    .mfp-zoom-out-cur .mfp-image-holder .mfp-close {
    cursor: pointer;
    }

    </style>
</head>
<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

    <div id="main-wrapper" data-navbarbg="skin6" data-theme="light" data-layout="vertical" data-sidebartype="full" data-boxed-layout="full">

        @include('include.admin.header')

        <div class="page-wrapper">

            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">View Events</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Basic Table</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="card card-body">

                            <div class="table-responsive-sm">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">owner_name</th>
                                            <th scope="col">mobile_number</th>
                                            <th scope="col">flat_specification</th>
                                            <th scope="col">flat_address</th>

                                            <th scope="col">Status</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php $count = 0; ?>
                                        @foreach($data as $key => $data_each)
                                        <tr>
                                            <th scope="row">{{$count += 1}}</td>
                                            <td>{{$data_each->owner_name}}</td>
                                            <td>{{$data_each->mobile_number}}</td>
                                            <td>{{$data_each->flat_specification}}</td>
                                            <td>{{$data_each->flat_address}}</td>
                                            <td>

                                              <select id="status" real_estate_id="{{$data_each->id}}">
                                                @if(@$status == 'pending')
                                                <option value="pending" selected="">Pending</option>
                                                <option value="active">Active</option>
                                                <option value="rejected">Rejected</option>
                                                <option value="closed">Closed</option>
                                                @elseif(@$status == 'active')
                                                <option value="active" selected="">Active</option>
                                                <option value="rejected">Rejected</option>
                                                <option value="closed">Closed</option>
                                                @elseif(@$status == 'rejected')
                                                <option value="active">Active</option>
                                                <option value="rejected" selected="">Rejected</option>
                                                <option value="closed">Closed</option>
                                                @elseif(@$status == 'closed')
                                                <option value="closed">Closed</option>
                                                @endif
                                              </select>

                                            </td>
                                            <td>
                                             <!--  <a href="{{ asset('admin/edit-event/'.@$data_each->id) }}" style="text-align: center" class="btn btn-info"><i class="m-r-10 mdi mdi-lead-pencil"></i></a> -->
                                                <button id="view_more" real_estate_id="{{@$data_each->id}}" style="text-align: center" class="btn btn-info">View</button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
               <input type="hidden" id="token" name="_token" value="{{csrf_token()}}"/>
            </div>

            <footer class="footer text-center">
                All Rights Reserved
                <!-- <a href="https://wrappixel.com">WrapPixel</a>. -->
            </footer>

        </div>

    </div>



    <!-- model starts -->

 <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="get_more_details_modal">
    <div class="modal-dialog modal-lg modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
               <h5 class="modal-title" id="flat_address"></h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
               </button>
             </div>
             <div class="modal-body">
                 <div class="overview open_detail_page" data-id="ff8081816c713de6016c74d19978588d" data-url="https://www.nobroker.in/property/buy/1-BHK-apartment-for-sale-in-Thane-West-mumbai/ff8081816c713de6016c74d19978588d/detail">
                     <div class="row">
                         <div itemprop="valueReference" itemscope="" itemtype="http://schema.org/PropertyValue" class="col-sm-3 solid-border-right" style="border-right:1px solid #EBEBEB;">
                             <h3>
                                 <meta itemprop="name" content="property size">
                                 <span id="railway_station">0.5</span><!-- <span itemprop="unitText"> km</span> -->
                                 <meta itemprop="value" content="630">
                             </h3>
                             <h5>
                             Railway station
                             </h5>
                         </div>
                         <div itemprop="valueReference" itemscope="" itemtype="http://schema.org/PropertyValue" class="col-sm-3 solid-border-right" style="border-right:1px solid #EBEBEB;">
                             <h3>
                                 <meta itemprop="name" content="property size">
                                 <span id="build_up_area">630</span>/<span id="carpet_area">630</span><span itemprop="unitText"> sqft</span>
                                 <meta itemprop="value" content="630">
                             </h3>
                             <h5>
                             Builtup / Carpet
                             </h5>
                         </div>
                         <div class="col-sm-3 solid-border-right" itemprop="valueReference" itemscope="" itemtype="http://schema.org/PropertyValue">
                             <h3>
                             <meta itemprop="name" content="Estimated EMI">
                             <i class="icon-inr icon-x icon inr_resale" title="Rs"></i> <span id="deposit">61,062</span>
                             <meta itemprop="value" content="61062/Month">
                             </h3>
                             <h5>
                             Deposit
                             </h5>
                         </div>
                         <div class="col-sm-3" itemprop="valueReference" itemscope="" itemtype="http://schema.org/PropertyValue">
                             <h3>
                             <meta itemprop="name" content="Property Value">
                             <i class="icon-inr icon-x icon inr_resale" title="Rs"></i><span id="rent"></span>
                             <meta itemprop="value" content="8700000">
                             </h3>
                             <h5>
                             <i class="icon-inr icon-x icon inr_resale" title="Rs"></i> Rent
                             </h5>
                         </div>
                     </div>
                 </div>
                 <div class="row mt-4">
                     <div class="col-md-4">
                         <div class="slider-for" id="modal_image">
                             @if(@$data_each->images)
                             @foreach(@$data_each->images as $image)
                             <a href="{{ asset('assets/images/real-estate/'.@$image->image_name) }}" class="item-slick"><img src="{{ asset('assets/images/real-estate/'.@$image->image_name) }}" alt="Alt"></a>
                             @endforeach
                             @endif
                             <!-- <a href="http://farm9.staticflickr.com/8382/8558295631_0f56c1284f_b.jpg" class="item-slick"><img src="http://farm9.staticflickr.com/8382/8558295631_0f56c1284f_b.jpg" alt="Alt"></a>
                             <a href="http://farm9.staticflickr.com/8225/8558295635_b1c5ce2794_b.jpg" class="item-slick"><img src="http://farm9.staticflickr.com/8225/8558295635_b1c5ce2794_b.jpg" alt="Alt"></a>
                             <a href="http://farm9.staticflickr.com/8383/8563475581_df05e9906d_b.jpg" class="item-slick"><img src="http://farm9.staticflickr.com/8383/8563475581_df05e9906d_b.jpg" alt="Alt"></a> -->
                         </div>
                         <!-- <img src="https://images.nobroker.in/images/ff8081816c713de6016c74d19978588d/ff8081816c713de6016c74d19978588d_38382_85657_large.jpg" style="height: auto; width: 100%; top: -79px;"> -->
                     </div>
                     <div class="col-md-8">
                         <div class="row detail-summary open_detail_page" data-id="ff8081816c713de6016c74d19978588d" data-url="https://www.nobroker.in/property/buy/1-BHK-apartment-for-sale-in-Thane-West-mumbai/ff8081816c713de6016c74d19978588d/detail">
                             <div class="col-sm-12">
                                 <div class="row summary-row border-bottom text-align-left">

                                     <div class="info-card property-facing col-sm-6 col-xs-6 solid-border-right text-align-left">

                                         <h5 class="semi-bold" id="facing"></h5>
                                         <div class="list-card-subtext"> Facing </div>
                                     </div>
                                     <div class="info-card property-age col-sm-6 col-xs-6 text-align-left" itemprop="valueReference" itemscope="" itemtype="http://schema.org/PropertyValue">
                                         <h5 class="semi-bold" id="floor">3rd </h5>
                                         <div class="list-card-subtext"> Floor</div>
                                     </div>
                                 </div>
                                 <div class="row summary-row text-align-left border-bottom">
                                     <div class="info-card property-bathrooms col-sm-6 col-xs-6 solid-border-right text-align-left">
                                         <h5 class="semi-bold" id="flat_specification">  </h5>
                                         <div class="list-card-subtext">Flat Specification</div>
                                     </div>
                                     <div class="info-card property-parking col-sm-6 col-xs-6 ">
                                         <h5 class="semi-bold" id="preferred_tenant_type"> </h5>
                                         <div class="list-card-subtext"> Tenant Type </div>
                                     </div>
                                 </div>
                                 <div class="row summary-row text-align-left">
                                     <div class="info-card property-bathrooms col-sm-6 col-xs-6 solid-border-right text-align-left">
                                         <h5 class="semi-bold" id="oc_recieved"> </h5>
                                         <div class="list-card-subtext">O.C Recieved</div>
                                     </div>
                                     <div class="info-card property-parking col-sm-6 col-xs-6 ">
                                         <h5 class="semi-bold" id="parking"></h5>
                                         <div class="list-card-subtext"> Parking </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>

                 <div class="mt-4"></div>
                 <div class="row">
                     <div class="col-md-6">
                         <div class="card card-body">
                             <div class="border-bottom pt-3 pb-3">
                                <!-- <i class="fa fa-user mr-2 text-dark"></i> -->
                                <strong class="text-dark">Available</strong>
                                <span class="float-right" id="availabe"></span>
                             </div>
                             <div class="border-bottom pt-3 pb-3">
                                <!-- <i class="fa fa-user mr-2 text-dark"></i> -->
                                <strong class="text-dark">Condition of flat</strong>
                                <span class="float-right" id="flat_condition"></span>
                             </div>
                             <div class="border-bottom pt-3 pb-3">
                                <!-- <i class="fa fa-user mr-2 text-dark"></i> -->
                                <strong class="text-dark">Maintenance cleared upto</strong>
                                <span class="float-right" id="maintenance_cleared_upto"></span>
                             </div>
                             <div class="pt-3 pb-3">
                                <!-- <i class="fa fa-user mr-2 text-dark"></i> -->
                                <strong class="text-dark">Other Dues</strong>
                                <span class="float-right" id="other_unclear_dues"></span>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-6">
                         <div class="card card-body">
                             <div class="border-bottom pt-3 pb-3">
                                <!-- <i class="fa fa-user mr-2 text-dark"></i> -->
                                <strong class="text-dark">Lift</strong>
                                <span class="float-right" id="lift"></span>
                             </div>
                             <div class="border-bottom pt-3 pb-3">
                                <!-- <i class="fa fa-user mr-2 text-dark"></i> -->
                                <strong class="text-dark">Gas Pipeline</strong>
                                <span class="float-right" id="gas_pipeline"></span>
                             </div>
                             <div class="border-bottom pt-3 pb-3">
                                <!-- <i class="fa fa-user mr-2 text-dark"></i> -->
                                <strong class="text-dark">24 HRs. Secyrity, CCTV</strong>
                                <span class="float-right" id="cctv"></span>
                             </div>
                             <div class="pt-3 pb-3">
                                <!-- <i class="fa fa-user mr-2 text-dark"></i> -->
                                <strong class="text-dark">Water Supply</strong>
                                <span class="float-right" id="water_supply"></span>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-12">
                         <h5>Porperty Details</h5>
                         <p id="description"></p>
                     </div>
                 </div>

             <!-- <div class="modal-footer">
                 <button type="button" class="btn btn-primary">Save changes</button> --
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
             </div> -->
        </div>
      </div>
    </div>
</div>
    @include('include.admin.footer')
</body>
<script type="text/javascript">
  var token = $('#token').val();
  $(document).on('change','#status',function(){
    var real_estate_id = $(this).attr("real_estate_id");
    var status = $(this).val();

    $.ajax({
      url:path+'admin/update-real-estate-status',
      type:'post',
      data:{_token:token,real_estate_id:real_estate_id,status:status},
      success:function(data,status){
        alert(data);
      }
    });

  });
</script>
<!-- <script type="text/javascript">

       $(document).on('click','#delete_event',function(){
        var event_id = $(this).attr('event_id');
              swal({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then(function (result) {
            if (result.value) {
        $.ajax({
           url:path+'admin/delete_event',
           type:'delete',
           data:{_token:token,event_id:event_id},
           success:function(data,status){

                         swal({
                              title: 'Deleted',
                              text: "Selected product has been deleted.",
                              type: 'success',
                              showCancelButton: false,
                              confirmButtonColor: '#3085d6',
                              confirmButtonText: 'Ok'
                            }).then(function (result) {
                              if (result.value) {
                                location.reload();
                              }
                         });
                }
        });
         }
           });

       });

   </script> -->

   <script type="text/javascript">
     $(document).on('click','#view_more',function(){
      var real_estate_id = $(this).attr('real_estate_id');
      $.ajax({
          'url':path+'get-real-estate-details',
          type:"get",
          data:{_token:token,real_estate_id:real_estate_id},
          success:function(data,status){
                  $('#flat_address').text(data['flat_specification']+' flat for '+data['type']+' in '+data['flat_address']);
                  $('#build_up_area').text(data['build_up_area']);
                  $('#carpet_area').text(data['carpet_area']);
                  $('#deposit').text(data['deposit']);
                  $('#rent').text(data['rent']);
                  $('#facing').text(data['vastu']);
                  $('#floor').text(data['floor']);
                  $('#flat_specification').text(data['flat_specification']);
                  $('#preferred_tenant_type').text(data['preferred_tenant_type']);
                  $('#oc_recieved').text(data['oc_recieved']);
                  $('#parking').text(data['parking']);
                  $('#availabe').text(data['available_from']+' - '+data['available_to']);
                  $('#flat_condition').text(data['flat_condition']);
                  $('#maintenance_cleared_upto').text(data['maintenance_cleared_upto']);
                  $('#other_unclear_dues').text(data['other_unclear_dues']);
                  $('#lift').text(data['lift']);
                  $('#gas_pipeline').text(data['gas_pipeline']);
                  $('#cctv').text(data['cctv']);
                  $('#water_supply').text(data['water_supply']);
                  $('#description').text(data['description']);
                  var image_data = '';

                  if (data['images']) {
                    $.each(data['images'],function(key,value){

                      image_data += '<a href="'+path+'/assets/images/real-estate/'+value['image_name']+'" class="item-slick"><img src="'+path+'/assets/images/real-estate/'+value['image_name']+'" alt="Alt"></a>';

                    });
                  }

                  console.log(image_data);
                  $('#modal_image').html(image_data);

          }
      });
      $("#get_more_details_modal").modal('show');
     })
   </script>
</html>
