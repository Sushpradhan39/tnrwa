<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    @include('include.admin.head')
</head>
<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>

    <div id="main-wrapper" data-navbarbg="skin6" data-theme="light" data-layout="vertical" data-sidebartype="full" data-boxed-layout="full">

        @include('include.admin.header')

        <div class="page-wrapper">

            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Add News</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Add News</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <div class="card card-body">
                            <form class="form-horizontal m-t-30" id='news_form'>
                   <input type='hidden' name='_token' id='token' value='{{csrf_token()}}'/>
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" class="form-control" name="title" required="">
                                </div>

                                <div class="form-group">
                                    <label>Sub Title</label>
                                    <input type="text" class="form-control" name="sub_title" >
                                </div>

                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" rows="4" cols="50" name="description" required=""></textarea>
                                </div>

                                 <div class="form-group">
                                    <label>Vedio Links</label>
                                    <textarea class="form-control" rows="4" cols="50" name="vedio_links" ></textarea>
                                </div>

                                <div class="form-group">
                                    <label>Display Image</label>
                                    <input type="file" class="form-control" name="display_image" required="">
                                </div>

                                 <div class="form-group">
                                    <label>Drive link</label>
                                    <input type="text" class="form-control" name="drive_link" >
                                </div>

                               <!--  <div class="form-group">
                                    <label>Other Images</label>
                                    <input type="file" class="form-control" name="other_images[]" multiple>
                                </div> -->

                                <div class="form-group">
                                    <button type="submit" class="btn btn-info">Create News</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

            <footer class="footer text-center">
                All Rights Reserved.
                <!-- <a href="https://wrappixel.com">WrapPixel</a>. -->
            </footer>
        </div>

    </div>

    @include('include.admin.footer')
</body>
<script type="text/javascript">
    $('#news_form').submit(function(e){
          e.preventDefault();

          $.ajax({
           url:path+'admin/add_news',
           type:'post',
           data:new FormData(this),
           contentType:false,
           processData:false,
           success:function(data,status)
           {
             swal({
             title: 'Success',
                                        
                 type: 'success',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Ok'
                }).then(function (result) 
                {
                  if (result.value) {
                  location.reload();
                                    }
            });
           }
       });
    });
</script>

</html>
