<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>TNRWA - About Us</title>
  @include('include.front.head')
</head>
<body>

  <!--================ Header Menu Area start =================-->
  @include('include.front.header')
  <!--================Header Menu Area =================-->


  <!--================Hero Banner Area Start =================-->
  <section class="hero-banner hero-banner-sm">
    <div class="container text-center">
      <h2>About Us</h2>
      <nav aria-label="breadcrumb" class="banner-breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ asset('/') }}">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">About</li>
        </ol>
      </nav>
    </div>
  </section>
  <!--================Hero Banner Area End =================-->

  <!--================ Sponsor section Start =================-->
  <section class="section-padding--small sponsor-bg">
    <div class="container">
      <div class="section-intro text-center pb-80px">
        <p class="section-intro__title"></p>
        <h2 class="primary-text">Member's Together .. Societies Together</h2>
        <img src="img/home/section-style.png" alt="">
      </div>

        <div class="row">
            <ul class="custom_members">
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/peter.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mr. Peter Francis Augustine Fernandes</h4>
                      <p>President-TNRWA</p>
                      <p>Sportsman - Football Coach</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mr. Vinod Daulatrao Desai</h4>
                      <p>Vice President-TNRWA</p>
                      <p>Businessman</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/nitin.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mr. Nitin Balkrishna Nikam</h4>
                      <p>Gen. Secretary-TNRWA</p>
                      <p>Advocate</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mr. Mahesh Krishna Naik</h4>
                      <p>Jt. Secretary-TNRWA</p>
                      <p>Tax Inspector</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mr. Ramesh Raosaheb Kamble</h4>
                      <p>Treasurer-TNRWA</p>
                      <p>Businessman</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mr. Amit Bhagwan Kale</h4>
                      <p>Member-TNRWA </p>
                      <p>Businessman</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mr. Amit Bhagwan Kale</h4>
                      <p>Member-TNRWA </p>
                      <p>Businessman</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mr. Sanju Ramchandra Kale</h4>
                      <p>Member-TNRWA</p>
                      <p>Civil Engineer</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mr. Rajesh Anant More</h4>
                      <p>Member-TNRWA </p>
                      <p>Businessman</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mr. Karthik Subramanian Iyer</h4>
                      <p>Member-TNRWA</p>
                      <p>Chartered Accountant</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mr. Vivek Raghunath Bhosale</h4>
                      <p>Member-TNRWA</p>
                      <p>Architect</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Dr. Rajshekhar Gurupad Sinhasan</h4>
                      <p>Member-TNRWA</p>
                      <p>M.B.B.S Doctor</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/pjgore.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mr. Padmakumar Jinasa Gore</h4>
                      <p>President-TNRWA</p>
                      <p>Sportsman - Football Coach</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Dr. Manish Delhiwala</h4>
                      <p>Member-TNRWA</p>
                      <p>M.B.B.S Doctor</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Dr. Arvind J. Vatkar</h4>
                      <p>Member-TNRWA</p>
                      <p>M.S. Ortho</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/madhura.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mrs. Madhura Desai</h4>
                      <p>Member-TNRWA</p>
                      <p>MBA</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mr. Namdeo Gadekar</h4>
                      <p>Member-TNRWA</p>
                      <p>Social Worker</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mr. Dilip Suryavanshi</h4>
                      <p>Member-TNRWA</p>
                      <p>Businessman</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mr. Dilip Shinde</h4>
                      <p>Member-TNRWA</p>
                      <p>Engineer</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mr. Ajeet Manwani</h4>
                      <p>Member-TNRWA</p>
                      <p>Advocate</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mr. Pranal Padmakumar Gore</h4>
                      <p>Member-TNRWA</p>
                      <p>MBA, IT Software Developer</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mr. Dnyaneshwar Kondaji Kale</h4>
                      <p>Member-TNRWA</p>
                      <p>Environmentalist</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mrs. Shraddha Ashok Sawant</h4>
                      <p>Member-TNRWA</p>
                      <p>House wife</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mr. Nandu Parab</h4>
                      <p>Member-TNRWA</p>
                      <p>Sanitory Inspector</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mrs. Vijaya Jagdale</h4>
                      <p>Member-TNRWA</p>
                      <p>Lion's Club member</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Dr. Amit J. Vatkar</h4>
                      <p>Member-TNRWA</p>
                      <p>Paediatric Neurologist</p>
                  </div>
              </li>

              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mr. Manoj Patel</h4>
                      <p>Member-TNRWA</p>
                      <p>Excise Inspector</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mr. Bharat Savla</h4>
                      <p>Member-TNRWA</p>
                      <p>Businessman</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mrs. Sujata Suryavanshi</h4>
                      <p>Member-TNRWA</p>
                      <p>Teacher</p>
                  </div>
              </li>
              <li>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="{{ asset("assets/front/img/gallery/members/no-img.png") }}" />
                  </div>
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <h4>Mr. Janardhan P. Vatkar</h4>
                      <p>Member-TNRWA</p>
                      <p>Sr. Manager QC, HPCL</p>
                  </div>
              </li>
            </ul>
        </div>
    </div>
  </section>
  <!--================ Sponsor section End =================-->

  <!-- ================ start footer Area ================= -->
@include('include.front.footer')
  <!-- ================ End footer Area ================= -->

</script>

</body>
</html>
