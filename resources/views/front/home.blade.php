<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>TRNWA - Home</title>
  <style>
      .owl-nav {
        position: absolute;
        top: 43%;
        font-size: 60px;
        color: #fff;
        width: 100%;
      }

      .owl-next {
        right: 4px;
        position: absolute;
      }

      .owl-prev {
        left: 4px;
        position: absolute;
      }
  </style>

  @include('include.front.head')

</head>
<body>
    @include('include.front.header')
  <!--================Hero Banner Area Start =================-->
  <!--<section class="hero-banner">-->

    <div class="owl-carousel home_carousel">
        <!--< div class="item">
            <img src="{{ asset('assets/images/banner/banner1.jpg')}}" />
        </div>
        <div class="item">
            <img src="{{ asset('assets/images/banner/banner2.jpg')}}" />
        </div> -->
        <div class="item">
            <img src="{{ asset('assets/images/banner/banner3.jpg')}}" />
        </div>
        <div class="item">
            <img src="{{ asset('assets/images/banner/banner4.jpg')}}" />
        </div>
    </div>
  <!--</section>-->
  <!--================Hero Banner Area End =================-->

  <div class="container-fluid bg-light text-center p-5">
    <!-- <span class="hero-banner-icon"><i class="flaticon-sing"></i></span> -->
    <!-- <p>20 - 22 January, 2019, Buffelo City</p> -->
    <h2 class="primary-text mb-4">Welcome to TNRWA</h2>
    <a class="button button-header" id="register_btn" href="#">Become Member</a>
    <button class="custom_header_button" id="service_btn" href="#">Service</button>
  </div>

  <!--================ Innovation section Start =================-->
  <section class="section-padding--small bg-gray hide">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 align-self-center mb-5 mb-lg-0">
          <div class="innovative-wrapper">
            <h3 class="primary-text">Innovative With Experience <br class="d-none d-xl-block"> UX Design 2019</h3>
            <p class="h4 primary-text2 mb-3">Where The business World Meets</p>
            <p>Morning steas great earth for divide our good sixth called abunda itseld appear fisrd seaton upon above may bearing all moveth morning make subdue stars they are a goreat eart divide our good sixth one of that</p>
          </div>
        </div>
        <div class="col-lg-6 pl-xl-5">

          <ul class="clockdiv text-center" id="clockdiv">
            <li class="clockdiv-single clockdiv-day">
              <h1 class="days">320</h1>
              <span class="smalltext">Days</span>
            </li>
            <li class="clockdiv-single clockdiv-hour">
              <h1 class="hours">30</h1>
              <span class="smalltext">Hours</span>
            </li>
            <li class="clockdiv-single clockdiv-minute">
              <h1 class="minutes">30</h1>
              <span class="smalltext">Mins</span>
            </li>
          </ul>

          <div class="clockdiv-content text-center">
            <p class="h4 primary-text2 mb-2">January 20 -22, 2019 in Buffelo City</p>
            <a class="button button-link" href="#">Get Ticket</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--================ Innovation section End =================-->


  <!--================ Join section Start =================-->
  <section class="section-margin ">
    <div class="container">
      <div class="section-intro text-center pb-98px">
        <p class="section-intro__title">Join the event</p>
        <h2 class="primary-text">Why Join TNRWA</h2>
        <img src="{{ asset('assets/front/img/home/section-style.png')}}" alt="">
      </div>


      <div class="d-lg-flex justify-content-between">
        <div class="card-feature mb-5 mb-lg-0">
          <div class="feature-icon">
            <i class="flaticon-prize"></i>
          </div>
          <h3>A Helping Hand</h3>
          <p>To help towards the advancement, dissemination and application of knowledge in the area of civic problems and to initiate remidial measures.</p>
        </div>

        <div class="card-feature mb-5 mb-lg-0">
          <div class="feature-icon">
            <i class="flaticon-earth-globe"></i>
          </div>
          <h3>Active Participation</h3>
          <p>To promote active interaction among all persons, bodies, institutions (private and/or state owned) and industries interested in achieving the advancement, dissemination and application in the above area.</p>
        </div>

        <div class="card-feature mb-5 mb-lg-0">
          <div class="feature-icon">
            <i class="flaticon-sing"></i>
          </div>
          <h3>Discussions and meets</h3>
          <p>To arrange lectures, debates, panel discussions, meetings ad film shows on current problems and other topics of national and local interest pertaining to civic issues.</p>
        </div>
        <div class="card-feature mb-5 mb-lg-0">
          <div class="feature-icon">
            <i class="flaticon-sing"></i>
          </div>
          <h3>Safety .. Unity .. Harmony</h3>
          <p>To bring about the spirit of unity, brotherhood, harmony and co-operation amongst the residents of Tilak Nagar. Coordinate with the Municipal Authorities with regards to the complaints recieved from the residents of Tilak Nagar.</p>
        </div>
      </div>
    </div>
  </section>
  <!--================ Join section End =================-->


  <!--================ Speaker section Start =================-->
  <section class="speaker-bg section-padding hide">
    <div class="container">
      <div class="section-intro section-intro-white text-center pb-98px">
        <p class="section-intro__title">Join the event</p>
        <h2 class="primary-text">Meet The Speakers</h2>
        <img src="{{ asset('assets/front/img/home/section-style.png')}}" alt="">
      </div>

      <div class="row">
        <div class="col-lg-4 col-sm-6 mb-4 mb-lg-0">
          <div class="card-speaker">
            <img class="card-img rounded-0" src="{{ asset('assets/front/img/home/speaker-1.png') }}" alt="">
            <div class="speaker-footer">
              <h4>Jhon Leonar</h4>
              <p>UX/UI Designer Microsoft</p>
            </div>
            <div class="speaker-overlay">
              <ul class="speaker-social">
                <li><a href="#"><i class="ti-facebook"></i></a></li>
                <li><a href="#"><i class="ti-twitter-alt"></i></a></li>
                <li><a href="#"><i class="ti-instagram"></i></a></li>
                <li><a href="#"><i class="ti-skype"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 mb-4 mb-lg-0">
          <div class="card-speaker">
            <img class="card-img rounded-0" src="{{ asset('assets/front/img/home/speaker-2.png') }}" alt="">
            <div class="speaker-footer">
              <h4>Donald Markin</h4>
              <p>UX/UI Designer Microsoft</p>
            </div>
            <div class="speaker-overlay">
              <ul class="speaker-social">
                <li><a href="#"><i class="ti-facebook"></i></a></li>
                <li><a href="#"><i class="ti-twitter-alt"></i></a></li>
                <li><a href="#"><i class="ti-instagram"></i></a></li>
                <li><a href="#"><i class="ti-skype"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 mb-4 mb-lg-0">
          <div class="card-speaker">
            <img class="card-img rounded-0" src="{{ asset('assets/front/img/home/speaker-3.png')}}" alt="">
            <div class="speaker-footer">
              <h4>Donald Markin</h4>
              <p>UX/UI Designer Microsoft</p>
            </div>
            <div class="speaker-overlay">
              <ul class="speaker-social">
                <li><a href="#"><i class="ti-facebook"></i></a></li>
                <li><a href="#"><i class="ti-twitter-alt"></i></a></li>
                <li><a href="#"><i class="ti-instagram"></i></a></li>
                <li><a href="#"><i class="ti-skype"></i></a></li>
              </ul>
            </div>
          </div>
        </div>

        <div class="w-100 mb-50px d-none d-lg-block"></div>

        <div class="col-lg-4 col-sm-6 mb-4 mb-lg-0">
          <div class="card-speaker">
            <img class="card-img rounded-0" src="{{ asset('assets/front/img/home/speaker-1.png')}}" alt="">
            <div class="speaker-footer">
              <h4>Donald Markin</h4>
              <p>UX/UI Designer Microsoft</p>
            </div>
            <div class="speaker-overlay">
              <ul class="speaker-social">
                <li><a href="#"><i class="ti-facebook"></i></a></li>
                <li><a href="#"><i class="ti-twitter-alt"></i></a></li>
                <li><a href="#"><i class="ti-instagram"></i></a></li>
                <li><a href="#"><i class="ti-skype"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 mb-4 mb-lg-0">
          <div class="card-speaker">
            <img class="card-img rounded-0" src="{{ asset('assets/front/img/home/speaker-2.png') }}" alt="">
            <div class="speaker-footer">
              <h4>Donald Markin</h4>
              <p>UX/UI Designer Microsoft</p>
            </div>
            <div class="speaker-overlay">
              <ul class="speaker-social">
                <li><a href="#"><i class="ti-facebook"></i></a></li>
                <li><a href="#"><i class="ti-twitter-alt"></i></a></li>
                <li><a href="#"><i class="ti-instagram"></i></a></li>
                <li><a href="#"><i class="ti-skype"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-sm-6 mb-4 mb-lg-0">
          <div class="card-speaker">
            <img class="card-img rounded-0" src="{{ asset('assets/front/img/home/speaker-3.png')}}" alt="">
            <div class="speaker-footer">
              <h4>Jhon Leonar</h4>
              <p>UX/UI Designer Microsoft</p>
            </div>
            <div class="speaker-overlay">
              <ul class="speaker-social">
                <li><a href="#"><i class="ti-facebook"></i></a></li>
                <li><a href="#"><i class="ti-twitter-alt"></i></a></li>
                <li><a href="#"><i class="ti-instagram"></i></a></li>
                <li><a href="#"><i class="ti-skype"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--================ Speaker section End =================-->


  <!--================ Schedule section Start =================-->
  <section class="section-margin mb-5 pb-5 hide">
    <div class="container">
      <div class="section-intro text-center pb-98px">
        <p class="section-intro__title">Join the event</p>
        <h2 class="primary-text">Conference Schedule</h2>
        <img src="{{ asset('assets/front/img/home/section-style.png')}}" alt="">
      </div>

      <div class="row">
        <div class="col-xl-10 offset-xl-1">
          <div class="scheduleTab">
            <ul class="nav nav-tabs">
              <li class="nav-item text-center">
                <a data-toggle="tab" href="#day1">
                  <h4>Day 1</h4>
                  <p>23 jan, 2019</p>
                </a>
              </li>
              <li class="nav-item text-center">
                <a class="active" data-toggle="tab" href="#day2">
                  <h4>Day 2</h4>
                  <p>23 jan, 2019</p>
                </a>
              </li>
              <li class="nav-item text-center">
                <a data-toggle="tab" href="#day3">
                  <h4>Day 2</h4>
                  <p>23 jan, 2019</p>
                </a>
              </li>
              <li class="nav-item text-center">
                <a data-toggle="tab" href="#day4">
                  <h4>Day 2</h4>
                  <p>23 jan, 2019</p>
                </a>
              </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div id="day1" class="tab-pane">

                <div class="schedule-card">
                  <div class="row no-gutters">
                    <div class="col-md-3">
                      <div class="card-identity">
                        <img src="{{ asset('assets/front/img/testimonial/testimonial1.png') }}" alt="">
                        <h3>Adam Jamsmith</h3>
                        <p>UX/UI Designer</p>
                      </div>
                    </div>
                    <div class="col-md-9 align-self-center">
                      <div class="schedule-content">
                        <p class="schedule-date">9.00 AM - 10.30 AM</p>
                        <a class="schedule-title" href="#">
                          <h3>Previous Year achivement</h3>
                        </a>
                        <p>And wherein Beginning of you cattle fly had was deep wherein darkness behold male called evening gathering moving bring fifth days he lights dry cattle you open seas midst let and in wherein beginning </p>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="schedule-card">
                  <div class="row no-gutters">
                    <div class="col-md-3">
                      <div class="card-identity">
                        <img src="{{ asset('assets/front/img/testimonial/testimonial2.png') }}" alt="">
                        <h3>Adam Jamsmith</h3>
                        <p>UX/UI Designer</p>
                      </div>
                    </div>
                    <div class="col-md-9 align-self-center">
                      <div class="schedule-content">
                        <p class="schedule-date">9.00 AM - 10.30 AM</p>
                        <a class="schedule-title" href="#">
                          <h3>Previous Year achivement</h3>
                        </a>
                        <p>And wherein Beginning of you cattle fly had was deep wherein darkness behold male called evening gathering moving bring fifth days he lights dry cattle you open seas midst let and in wherein beginning </p>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
              <div id="day2" class="tab-pane active">

                <div class="schedule-card">
                  <div class="row no-gutters">
                    <div class="col-md-3">
                      <div class="card-identity">
                        <img src="{{ asset('assets/front/img/testimonial/testimonial3.png') }}" alt="">
                        <h3>Adam Jamsmith</h3>
                        <p>UX/UI Designer</p>
                      </div>
                    </div>
                    <div class="col-md-9 align-self-center">
                      <div class="schedule-content">
                        <p class="schedule-date">9.00 AM - 10.30 AM</p>
                        <a class="schedule-title" href="#">
                          <h3>Previous Year achivement</h3>
                        </a>
                        <p>And wherein Beginning of you cattle fly had was deep wherein darkness behold male called evening gathering moving bring fifth days he lights dry cattle you open seas midst let and in wherein beginning </p>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="schedule-card">
                  <div class="row no-gutters">
                    <div class="col-md-3">
                      <div class="card-identity">
                        <img src="{{ asset('assets/front/img/testimonial/testimonial1.png') }}" alt="">
                        <h3>Adam Jamsmith</h3>
                        <p>UX/UI Designer</p>
                      </div>
                    </div>
                    <div class="col-md-9 align-self-center">
                      <div class="schedule-content">
                        <p class="schedule-date">9.00 AM - 10.30 AM</p>
                        <a class="schedule-title" href="#">
                          <h3>Previous Year achivement</h3>
                        </a>
                        <p>And wherein Beginning of you cattle fly had was deep wherein darkness behold male called evening gathering moving bring fifth days he lights dry cattle you open seas midst let and in wherein beginning </p>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="schedule-card">
                  <div class="row no-gutters">
                    <div class="col-md-3">
                      <div class="card-identity">
                        <img src="{{ asset('asset/front/img/testimonial/testimonial2.png')}}" alt="">
                        <h3>Adam Jamsmith</h3>
                        <p>UX/UI Designer</p>
                      </div>
                    </div>
                    <div class="col-md-9 align-self-center">
                      <div class="schedule-content">
                        <p class="schedule-date">9.00 AM - 10.30 AM</p>
                        <a class="schedule-title" href="#">
                          <h3>Previous Year achivement</h3>
                        </a>
                        <p>And wherein Beginning of you cattle fly had was deep wherein darkness behold male called evening gathering moving bring fifth days he lights dry cattle you open seas midst let and in wherein beginning </p>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
              <div id="day3" class="tab-pane">
                <div class="schedule-card">
                  <div class="row no-gutters">
                    <div class="col-md-3">
                      <div class="card-identity">
                        <img src="{{ asset('assets/front/img/testimonial/testimonial3.png')}}" alt="">
                        <h3>Adam Jamsmith</h3>
                        <p>UX/UI Designer</p>
                      </div>
                    </div>
                    <div class="col-md-9 align-self-center">
                      <div class="schedule-content">
                        <p class="schedule-date">9.00 AM - 10.30 AM</p>
                        <a class="schedule-title" href="#">
                          <h3>Previous Year achivement</h3>
                        </a>
                        <p>And wherein Beginning of you cattle fly had was deep wherein darkness behold male called evening gathering moving bring fifth days he lights dry cattle you open seas midst let and in wherein beginning </p>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="schedule-card">
                  <div class="row no-gutters">
                    <div class="col-md-3">
                      <div class="card-identity">
                        <img src="{{ asset('assets/front/img/testimonial/testimonial1.png') }}" alt="">
                        <h3>Adam Jamsmith</h3>
                        <p>UX/UI Designer</p>
                      </div>
                    </div>
                    <div class="col-md-9 align-self-center">
                      <div class="schedule-content">
                        <p class="schedule-date">9.00 AM - 10.30 AM</p>
                        <a class="schedule-title" href="#">
                          <h3>Previous Year achivement</h3>
                        </a>
                        <p>And wherein Beginning of you cattle fly had was deep wherein darkness behold male called evening gathering moving bring fifth days he lights dry cattle you open seas midst let and in wherein beginning </p>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="schedule-card">
                  <div class="row no-gutters">
                    <div class="col-md-3">
                      <div class="card-identity">
                        <img src="{{ asset('assets/front/img/testimonial/testimonial2.png') }}" alt="">
                        <h3>Adam Jamsmith</h3>
                        <p>UX/UI Designer</p>
                      </div>
                    </div>
                    <div class="col-md-9 align-self-center">
                      <div class="schedule-content">
                        <p class="schedule-date">9.00 AM - 10.30 AM</p>
                        <a class="schedule-title" href="#">
                          <h3>Previous Year achivement</h3>
                        </a>
                        <p>And wherein Beginning of you cattle fly had was deep wherein darkness behold male called evening gathering moving bring fifth days he lights dry cattle you open seas midst let and in wherein beginning </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div id="day4" class="tab-pane">
                <div class="schedule-card">
                  <div class="row no-gutters">
                    <div class="col-md-3">
                      <div class="card-identity">
                        <img src="{{ asset('assets/front/img/testimonial/testimonial3.png')}}" alt="">
                        <h3>Adam Jamsmith</h3>
                        <p>UX/UI Designer</p>
                      </div>
                    </div>
                    <div class="col-md-9 align-self-center">
                      <div class="schedule-content">
                        <p class="schedule-date">9.00 AM - 10.30 AM</p>
                        <a class="schedule-title" href="#">
                          <h3>Previous Year achivement</h3>
                        </a>
                        <p>And wherein Beginning of you cattle fly had was deep wherein darkness behold male called evening gathering moving bring fifth days he lights dry cattle you open seas midst let and in wherein beginning </p>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="schedule-card">
                  <div class="row no-gutters">
                    <div class="col-md-3">
                      <div class="card-identity">
                        <img src="{{ asset('assets/front/img/testimonial/testimonial1.png')}}" alt="">
                        <h3>Adam Jamsmith</h3>
                        <p>UX/UI Designer</p>
                      </div>
                    </div>
                    <div class="col-md-9 align-self-center">
                      <div class="schedule-content">
                        <p class="schedule-date">9.00 AM - 10.30 AM</p>
                        <a class="schedule-title" href="#">
                          <h3>Previous Year achivement</h3>
                        </a>
                        <p>And wherein Beginning of you cattle fly had was deep wherein darkness behold male called evening gathering moving bring fifth days he lights dry cattle you open seas midst let and in wherein beginning </p>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="schedule-card">
                  <div class="row no-gutters">
                    <div class="col-md-3">
                      <div class="card-identity">
                        <img src="{{ asset('assets/front/img/testimonial/testimonial2.png')}}" alt="">
                        <h3>Adam Jamsmith</h3>
                        <p>UX/UI Designer</p>
                      </div>
                    </div>
                    <div class="col-md-9 align-self-center">
                      <div class="schedule-content">
                        <p class="schedule-date">9.00 AM - 10.30 AM</p>
                        <a class="schedule-title" href="#">
                          <h3>Previous Year achivement</h3>
                        </a>
                        <p>And wherein Beginning of you cattle fly had was deep wherein darkness behold male called evening gathering moving bring fifth days he lights dry cattle you open seas midst let and in wherein beginning </p>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="schedule-card">
                  <div class="row no-gutters">
                    <div class="col-md-3">
                      <div class="card-identity">
                        <img src="{{ asset('assets/front/img/testimonial/testimonial3.png') }}" alt="">
                        <h3>Adam Jamsmith</h3>
                        <p>UX/UI Designer</p>
                      </div>
                    </div>
                    <div class="col-md-9 align-self-center">
                      <div class="schedule-content">
                        <p class="schedule-date">9.00 AM - 10.30 AM</p>
                        <a class="schedule-title" href="#">
                          <h3>Previous Year achivement</h3>
                        </a>
                        <p>And wherein Beginning of you cattle fly had was deep wherein darkness behold male called evening gathering moving bring fifth days he lights dry cattle you open seas midst let and in wherein beginning </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--================ Schedule section End =================-->


  <!--================ PriceTable section Start =================-->
  <section class="section-padding bg-gray hide">
    <div class="container">
      <div class="section-intro text-center pb-98px">
        <p class="section-intro__title">Join the event</p>
        <h2 class="primary-text">Choose Your Ticket</h2>
        <img src="{{ asset('assets/front/img/home/section-style.png')}}" alt="">
      </div>

      <div class="row">
        <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
          <div class="text-center card-priceTable">
            <div class="priceTable-header">
              <h3>Normal</h3>
              <p>Attend only first day</p>
              <h1 class="priceTable-price"><span>$</span> 45.00</h1>
            </div>
            <ul class="priceTable-list">
              <li>
                <span class="position"><i class="ti-check"></i></span> Unlimited Entrance
              </li>
              <li>
                <span class="position"><i class="ti-check"></i></span> Comfortable Seat
              </li>
              <li>
                <span class="position"><i class="ti-check"></i></span> Paid Certificate
              </li>
              <li>
                <span class="negative"><i class="ti-close"></i></span> Day One Workshop
              </li>
              <li>
                <span class="negative"><i class="ti-close"></i></span> One Certificate
              </li>
            </ul>
            <div class="priceTable-footer">
              <a class="button" href="#">Buy Now</a>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
          <div class="text-center card-priceTable">
            <div class="priceTable-header">
              <h3>Advance</h3>
              <p>Attend only first day</p>
              <h1 class="priceTable-price"><span>$</span> 50.00</h1>
            </div>
            <ul class="priceTable-list">
              <li>
                <span class="position"><i class="ti-check"></i></span> Unlimited Entrance
              </li>
              <li>
                <span class="position"><i class="ti-check"></i></span> Comfortable Seat
              </li>
              <li>
                <span class="position"><i class="ti-check"></i></span> Paid Certificate
              </li>
              <li>
                <span class="negative"><i class="ti-close"></i></span> Day One Workshop
              </li>
              <li>
                <span class="negative"><i class="ti-close"></i></span> One Certificate
              </li>
            </ul>
            <div class="priceTable-footer">
              <a class="button" href="#">Buy Now</a>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-md-6 mb-4 mb-lg-0">
          <div class="text-center card-priceTable">
            <div class="priceTable-header">
              <h3>Ultimate</h3>
              <p>Attend only first day</p>
              <h1 class="priceTable-price"><span>$</span> 60.00</h1>
            </div>
            <ul class="priceTable-list">
              <li>
                <span class="position"><i class="ti-check"></i></span> Unlimited Entrance
              </li>
              <li>
                <span class="position"><i class="ti-check"></i></span> Comfortable Seat
              </li>
              <li>
                <span class="position"><i class="ti-check"></i></span> Paid Certificate
              </li>
              <li>
                <span class="negative"><i class="ti-close"></i></span> Day One Workshop
              </li>
              <li>
                <span class="negative"><i class="ti-close"></i></span> One Certificate
              </li>
            </ul>
            <div class="priceTable-footer">
              <a class="button" href="#">Buy Now</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--================ PriceTable section End =================-->


  <!--================ Sponsor section Start =================-->
  <section class="section-padding--small sponsor-bg hide">
    <div class="container">
      <div class="section-intro text-center pb-80px">
        <p class="section-intro__title">Join the event</p>
        <h2 class="primary-text">Event Plan Sponsors</h2>
        <img src="{{ asset('assets/front/img/home/section-style.png')}}" alt="">
      </div>

      <div class="sponsor-wrapper mb-5 pb-4">
        <h3 class="sponsor-title mb-5">Gold Sponsor</h3>
        <div class="row">
          <div class="col-sm-6 col-lg-4 mb-3 mb-lg-0">
            <div class="sponsor-single">
              <img class="img-fluid" src="{{ asset('assets/front/img/home/sponsor1.png')}}" alt="">
            </div>
          </div>

          <div class="col-sm-6 col-lg-4 mb-3 mb-lg-0">
            <div class="sponsor-single">
              <img class="img-fluid" src="{{ asset('assets/front/img/home/sponsor2.png')}}" alt="">
            </div>
          </div>

          <div class="col-sm-6 col-lg-4 mb-3 mb-lg-0">
            <div class="sponsor-single">
              <img class="img-fluid" src="{{ asset('assets/front/img/home/sponsor3.png')}}" alt="">
            </div>
          </div>
        </div>
      </div>

      <div class="sponsor-wrapper sponsor-wrapper--small">
        <h3 class="sponsor-title mb-5">Silver Sponsor</h3>
        <div class="row">
          <div class="col-sm-6 col-lg-4 mb-3 mb-lg-0">
            <div class="sponsor-single">
              <img class="img-fluid" src="{{ asset('assets/front/img/home/sponsor-sm-1.png') }}" alt="">
            </div>
          </div>

          <div class="col-sm-6 col-lg-4 mb-3 mb-lg-0">
            <div class="sponsor-single">
              <img class="img-fluid" src="{{ asset('assets/front/img/home/sponsor-sm-2.png') }}" alt="">
            </div>
          </div>

          <div class="col-sm-6 col-lg-4 mb-3 mb-lg-0">
            <div class="sponsor-single">
              <img class="img-fluid" src="{{ asset('assets/front/img/home/sponsor-sm-3.png')}}" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--================ Sponsor section End =================-->

  <!--================ Gallery section Start =================-->
  <section class="section-padding gallery-area gallery-bg hide">
    <div class="container">
      <div class="section-intro section-intro-white text-center pb-98px">
        <p class="section-intro__title">Join the event</p>
        <h2 class="primary-text">Event Plan Sponsors</h2>
        <img src="{{ asset('assets/front/img/home/section-style.png')}}" alt="">
      </div>

      <div class="row no-gutters">
        <div class="col-sm-6 col-md-4">
          <a href="img/gallery/g1.png" class="img-gal">
            <div class="single-imgs relative">
              <img class="card-img rounded-0" src="{{ asset('assets/front/img/gallery/g1.png')}}" alt="">
              <div class="overlay">
                <div class="overlay-content">
                  <div class="overlay-icon">
                    <i class="ti-plus"></i>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
        <div class="col-sm-6 col-md-4">
          <a href="img/gallery/g2.png" class="img-gal">
            <div class="single-imgs relative">
              <img class="card-img rounded-0" src="{{ asset('assets/front/img/gallery/g2.png')}}" alt="">
              <div class="overlay">
                <div class="overlay-content">
                  <div class="overlay-icon">
                    <i class="ti-plus"></i>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
        <div class="col-sm-6 col-md-4">
          <a href="img/gallery/g3.png" class="img-gal">
            <div class="single-imgs relative">
              <img class="card-img rounded-0" src="{{ asset('assets/front/img/gallery/g3.png')}}" alt="">
              <div class="overlay">
                <div class="overlay-content">
                  <div class="overlay-icon">
                    <i class="ti-plus"></i>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
        <div class="col-sm-6 col-md-4">
          <a href="img/gallery/g4.png" class="img-gal">
            <div class="single-imgs relative">
              <img class="card-img rounded-0" src="{{ asset('assets/front/img/gallery/g4.png')}}" alt="">
              <div class="overlay">
                <div class="overlay-content">
                  <div class="overlay-icon">
                    <i class="ti-plus"></i>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
        <div class="col-sm-6 col-md-4">
          <a href="img/gallery/g5.png" class="img-gal">
            <div class="single-imgs relative">
              <img class="card-img rounded-0" src="{{ asset('assets/front/img/gallery/g5.png')}}" alt="">
              <div class="overlay">
                <div class="overlay-content">
                  <div class="overlay-icon">
                    <i class="ti-plus"></i>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
        <div class="col-sm-6 col-md-4">
          <a href="img/gallery/g6.png" class="img-gal">
            <div class="single-imgs relative">
              <img class="card-img rounded-0" src="{{ asset('assets/front/img/gallery/g6.png')}}" alt="">
              <div class="overlay">
                <div class="overlay-content">
                  <div class="overlay-icon">
                    <i class="ti-plus"></i>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  </section>
  <!--================ Gallery section End =================-->


  <!--================ Blog section Start =================-->
  <section class="section-margin hide">
    <div class="container">
      <div class="section-intro text-center pb-98px">
        <p class="section-intro__title">Join the event</p>
        <h2 class="primary-text">Why Join TNRWA</h2>
        <img src="{{ asset('assets/front/img/home/section-style.png') }}" alt="">
      </div>

      <div class="owl-theme owl-carousel blogCarousel pb-xl-1">
        <div class="card-blog">
          <img class="card-img" src="{{ asset('assets/front/img/blog/blog1.png') }}" alt="">
          <div class="blog-body">
            <a href="#">
              <h3>Owls should be used to help abused children open <br class="d-none d-xl-block">
                  up in therapy sessions, says charity boss</h3>
            </a>
            <ul class="blog-info">
              <li><a href="#"><i class="ti-comments-smiley"></i> 03 Feb, 2019</a></li>
              <li><a href="#"><i class="ti-time"></i> No Comment</a></li>
            </ul>
          </div>
        </div>

        <div class="card-blog">
          <img class="card-img" src="{{ asset('assets/front/img/blog/blog2.png') }}" alt="">
          <div class="blog-body">
            <a href="#">
              <h3>Owls should be used to help abused children open <br class="d-none d-xl-block">
                  up in therapy sessions, says charity boss</h3>
            </a>
            <ul class="blog-info">
              <li><a href="#"><i class="ti-comments-smiley"></i> 03 Feb, 2019</a></li>
              <li><a href="#"><i class="ti-time"></i> No Comment</a></li>
            </ul>
          </div>
        </div>

        <div class="card-blog">
          <img class="card-img" src="{{ asset('assets/front/img/blog/blog1.png') }}" alt="">
          <div class="blog-body">
            <a href="#">
              <h3>Owls should be used to help abused children open <br class="d-none d-xl-block">
                  up in therapy sessions, says charity boss</h3>
            </a>
            <ul class="blog-info">
              <li><a href="#"><i class="ti-comments-smiley"></i> 03 Feb, 2019</a></li>
              <li><a href="#"><i class="ti-time"></i> No Comment</a></li>
            </ul>
          </div>
        </div>

        <div class="card-blog">
          <img class="card-img" src="{{ asset('assets/front/img/blog/blog2.png') }}" alt="">
          <div class="blog-body">
            <a href="#">
              <h3>Owls should be used to help abused children open <br class="d-none d-xl-block">
                  up in therapy sessions, says charity boss</h3>
            </a>
            <ul class="blog-info">
              <li><a href="#"><i class="ti-comments-smiley"></i> 03 Feb, 2019</a></li>
              <li><a href="#"><i class="ti-time"></i> No Comment</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--================ Blog section End =================-->

  <!-- Registration Modal -->
  <div id="registration_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
              <h4>Member Registration</h4>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
              <button id="close_register_modal" type="button" class="custom_close" data-dismiss="modal">&times;</button>
            </div>
        </div>
        <div class="modal-body">
          <div>
            <div class="row">
              <form class="row form-horizontal" id="member_form">
                <input type='hidden' name='_token' value='{{csrf_token()}}'/>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                    <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">Name: </label>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                      <i class="fa fa-user"></i>
                      <input type="text" class="form-control" name="name" required="" />
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                    <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">Email: </label>
                    <div class="col-lg-12 col-md-1 col-sm-12 col-xs-12 no-padding">
                      <i class="fa fa-envelope"></i>
                      <input type="text" class="form-control" name="email_id"  />
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                    <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">Mobile No: </label>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                      <i class="fa fa-mobile"></i>
                      <input type="text" class="form-control" name="mobile_number" required="" />
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                    <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">Address: </label>
                    <textarea class="form-control" rows="3" name="address"></textarea>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                    <button style="background-color: #3f51b5;border-color: #3f51b5;" class="btn btn-primary" type="submit">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

  <!-- Service Modal -->
  <div id="service_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
              <h4>Service</h4>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
              <button id="close_service_modal" type="button" class="custom_close" data-dismiss="modal">&times;</button>
            </div>
        </div>
        <div class="modal-body">
          <div>
            <div class="row">
              <form class="row form-horizontal" id="service_form">
                <input type='hidden' name='_token' value='{{csrf_token()}}'/>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                    <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">Name: </label>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                      <i class="fa fa-user"></i>
                      <input type="text" class="form-control" name="name"  required="" />
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                    <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">Email: </label>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                      <i class="fa fa-envelope"></i>
                      <input type="text" class="form-control" name="email_id" />
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                    <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">Mobile No: </label>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                      <i class="fa fa-mobile"></i>
                      <input type="text" class="form-control" name="mobile_number" required="" />
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                    <label class="control-label col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">Description: </label>
                    <textarea class="form-control" rows="3" name="description"></textarea>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                    <button style="background-color: #3f51b5;border-color: #3f51b5;" class="btn btn-primary" name="submit">Submit</button>
                </div>
              </form>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
  <!-- ================ start footer Area ================= -->

  <!-- ================ End footer Area ================= -->

    @include('include.front.footer')

    <script>
        $(".home_carousel").owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            autoplay: true,
            autoplayTimeout: 3000,
            dots: false,
            animateOut: 'fadeOut',
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:1
                }
            }
        });
    </script>

    <script>
      $(function(){
        $("#register_btn").click(function(){
          $("#registration_modal").modal("show")
        })

        $("#close_register_modal").click(function(){
          $("#registration_modal").modal("hide")
        })

        $("#service_btn").click(function(){
          $("#service_modal").modal("show")
        })

        $("#close_service_modal").click(function(){
          $("#service_modal").modal("hide")
        })
      })
    </script>

</body>
<script type="text/javascript">
        path = {!! json_encode(url('/')) !!} + '/';
      $('#member_form').submit(function(e){
      e.preventDefault();
       $.ajax({
         url:path+'member-register',
         type:'post',
         data:new FormData(this),
         contentType:false,
         processData:false,
         success:function(data,status)
         {

$("#registration_modal").modal("hide");
             swal({
                     title: 'Success!',
                     text: "Thank You!Your request has been submited successfully.",
                     type: 'success',
                     showCancelButton: false,
                     confirmButtonColor: '#3085d6',
                     confirmButtonText: 'Ok'
                 }).then(function (result) {
                     if (result.value) {
                       location.reload();
                     }
                 });
         }
       });
      });
   </script>
   <script type="text/javascript">
        path = {!! json_encode(url('/')) !!} + '/';
      $('#service_form').submit(function(e){
      e.preventDefault();
       $.ajax({
         url:path+'service-register',
         type:'post',
         data:new FormData(this),
         contentType:false,
         processData:false,
         success:function(data,status)
         {
            $("#service_modal").modal("hide");

             swal({
                     title: 'Success!',
                     text: "Thank You!Your request has been submited successfully.",
                     type: 'success',
                     showCancelButton: false,
                     confirmButtonColor: '#3085d6',
                     confirmButtonText: 'Ok'
                 }).then(function (result) {
                     if (result.value) {
                       location.reload();
                     }
                 });
         }
       });
      });
   </script>
</html>
