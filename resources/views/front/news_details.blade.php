<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>TNRWA - News</title>
	@include('include.front.head')
</head>
<body>
	@include('include.front.header')
	<section class="hero-banner hero-banner-sm">
		<div class="container text-center">
	      	<h2>{{@$news->title}}</h2>
					<p>{{@$news->sub_title}}</p>
	      	<nav aria-label="breadcrumb" class="banner-breadcrumb">
		        <ol class="breadcrumb hide">
		          	<li class="breadcrumb-item"><a href="#">Home</a></li>
		          	<li class="breadcrumb-item"><a href="">News</a></li>
		          	<li class="breadcrumb-item active" aria-current="page">Gallery</li>
		        </ol>
		    </nav>
				<p style="font-size: 17px;">Usually when we hear or read something new, we just compare it to our own ideas. If it is the same, we accept it and say that it is correct. If it is not, we say it is incorrect. In either case, we learn nothing.</p>
		</div>
	</section>
	<section class="section-padding--large gallery-area">
		<div class="container">

			<div style="font-size: 20px;margin-bottom: 35px;" class="row">
				<div class="innovative-wrapper">
		            <p>{{@$news->description}}</p>
		        </div>
			</div>
@if(@$news->vedio_links)
				<div class="row no-gutters" style="margin-bottom: 20px;">
					<div style="padding-bottom: 50px;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center pb-98px">
						<h2 class="primary-text">News Videos</h2>
						<img src="{{ asset('assets/front/img/home/section-style.png') }}" />
					</div>

					<div style="padding: 5px;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						
						<?php $vedio_links = explode(',', $news->vedio_links) ?>
						@foreach($vedio_links as $key => $link)
						<div style="float: left" class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 card_background card-speaker no-padding">
									<iframe style="border: none;" width="100%" height="250" src="{{@$link}}"
	        					allowfullscreen="allowfullscreen"
	        					mozallowfullscreen="mozallowfullscreen"
	        					msallowfullscreen="msallowfullscreen"
	        					oallowfullscreen="oallowfullscreen"
	        					webkitallowfullscreen="webkitallowfullscreen"
									></iframe>
								</div>
							</div>
							@endforeach
                          
					</div>
				</div><!-- https://drive.google.com/open?id=1y7Pc73jZPevePy8CrVSrR8wXIJ0peDix -->
				 @endif
          @if(@$news->drive_link)
			<div class="row no-gutters">

				<div style="padding-bottom: 50px;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center pb-98px">
					<h2 class="primary-text">News Images</h2>
					<img src="{{ asset('assets/front/img/home/section-style.png') }}" />
				</div>

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<iframe src="{{'https://drive.google.com/embeddedfolderview?id='.@$news->drive_link.'#grid'}}" style="width:100%; height:600px; border:0;" allowfullscreen="true" frameborder="0">></iframe>
				</div>
        	</div>
        @endif

		</div>
	</section>
	@include('include.front.footer')
</body>
</html>
