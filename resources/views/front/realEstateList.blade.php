<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>TNRWA - Real Estate Form</title>
  @include('include.front.head')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.5/daterangepicker.min.css" />
  <style>
  .card .overview {
    max-height: 60px;
    border-bottom: 1px solid #D1D1D1;
    padding: 10px;
    text-align: center;
}
  .card .overview h3 {
      font-size: 16px;
      font-weight: 600;
      margin: 0;
    }

    .card .overview h5 {
    font-size: 12px;
    font-weight: 400;
    margin: 5px 0 0;
}

.solid-border-right {
    border-right: 1px solid #f4f6f6;
}

.rent-property-card .detail-summary .summary-row .info-card .semi-bold {
    margin: 0;
    color: #707070;
    font-weight: 600;
}

.card .detail-summary {
    border-style: double;
    border-color: #E1E1E1;
    margin-right: 10px;
}

.card .detail-summary h5 {
    font-size: 14px;
    font-weight: 500;
    margin-top: 20px;
    display: inline-block;
}

.rent-property-card .detail-summary .summary-row .info-card {
    padding-top: 10px;
    padding-left: 45px;
}
.card .detail-summary .summary-row .solid-border-right {
    border-right: 1px solid #D1D1D1;
    min-height: 60px;
    max-height: 60px;
}
.card .detail-summary .summary-row .property-facing {
    background-position: -14px -265px;
}

.item-slick.slick-slide.slick-current.slick-active {
  outline: none !important;
}

.slider-for {
  margin-bottom: 15px;
}
.slider-for img {
  width: 100%;
  min-height: 100%;
}

.slider-nav {
  margin: auto;
}

.slick-slide.slick-current {
    position: absolute !important;
    left: 0 !important;
}

.slick-slide {
    width: 100% !important;
    position: absolute !important;
    left: 0 !important;
}

.slider-nav .item-slick {
  max-width: 240px;
  margin-right: 15px;
  outline: none !important;
  cursor: pointer;
}
.slider-nav .item-slick img {
  max-width: 100%;
  background-size: cover;
  background-position: center;
}

.slick-arrow {
    position: absolute;
    top: 10%;
    z-index: 1111;
    font-size: 30px;
    padding: 7px;
    color: #fff;
}

.slick-prev {
  left: 0;
}

.slick-next {
  right: 0;
}

.mfp-zoom-out-cur, .mfp-zoom-out-cur .mfp-image-holder .mfp-close:hover {
  cursor: pointer;
}

.mfp-container:hover {
  cursor: default;
}

.image-source-link {
  color: #98C3D1;
}

.mfp-with-zoom.mfp-bg {
  opacity: 0;
  transition: all 0.3s ease-out;
}

.mfp-with-zoom.mfp-ready .mfp-container {
  opacity: 1;
}

.mfp-with-zoom.mfp-ready.mfp-bg {
  opacity: 0.8;
}

.mfp-with-zoom.mfp-removing.mfp-bg {
  opacity: 0;
}

/*BLUR background */
* {
  transition: filter 0.25s ease;
}

.mfp-wrap ~ * {
  filter: blur(5px);
}

.mfp-ready .mfp-figure {
  opacity: 0;
}

/* start state */
/* animate in */
/* animate out */
.mfp-zoom-in .mfp-figure, .mfp-zoom-in .mfp-iframe-holder .mfp-iframe-scaler {
  opacity: 0;
  transition: all 0.3s ease-out;
  transform: scale(0.95);
}

.mfp-zoom-in .mfp-preloader {
  opacity: 0;
  transition: all 0.3s ease-out;
}

.mfp-zoom-in.mfp-image-loaded .mfp-figure, .mfp-zoom-in.mfp-ready .mfp-iframe-holder .mfp-iframe-scaler {
  opacity: 1;
  transform: scale(1);
}

.mfp-zoom-in.mfp-ready .mfp-preloader {
  opacity: 0.8;
}

.mfp-zoom-in.mfp-removing .mfp-figure, .mfp-zoom-in.mfp-removing .mfp-iframe-holder .mfp-iframe-scaler {
  transform: scale(0.95);
  opacity: 0;
}

.mfp-zoom-in.mfp-removing .mfp-preloader {
  opacity: 0;
}

.mfp-iframe-scaler {
  overflow: visible;
}

.mfp-zoom-out-cur {
  cursor: auto;
}

.mfp-zoom-out-cur .mfp-image-holder .mfp-close {
  cursor: pointer;
}


  </style>
</head>
<body>

  <!--================ Header Menu Area start =================-->
  @include('include.front.header')
  <!--================Header Menu Area =================-->


  <!--================Hero Banner Area Start =================-->
  <section class="hero-banner hero-banner-sm">
    <div class="container text-center">
      <h2>Real Estate</h2>
      <nav aria-label="breadcrumb" class="banner-breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ asset('/') }}">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Real Estate</li>
        </ol>
      </nav>
    </div>
  </section>
  <!--================Hero Banner Area End =================-->

  <!--================ Sponsor section Start =================-->
  <section class="section-padding--small sponsor-bg">


 <div class="container">

    <div class="row">
        <div class="col-md-12">

        <div class="form-inline float-left">
            <label>Apply filter:</label>

            <div class="form-group ml-1">
                <select class="form-control" name="type_filter" id="type_filter">
                    <option value="all" @if($type == 'all') selected @endif>All</option>
                    <option value="sale" @if($type == 'sale') selected @endif>Sale</option>
                    <option value="rent" @if($type == 'rent') selected @endif>rent</option>
                </select>
            </div>
             <div class="form-group ml-2">
                <select  class="form-control" name="spec_filter" id="spec_filter">
                    <option value="all" @if($flat_specification == 'all') selected @endif>All</option>
                     <option value="1 RK" @if($flat_specification == '1 RK') selected @endif>1 RK</option>
                      <option value="1 BHK" @if($flat_specification == '1 BHK') selected @endif>1 BHK</option>
                      <option value="2 BHK" @if($flat_specification == '2 BHK') selected @endif>2 BHK</option>
                      <option value="3 BHK" @if($flat_specification == '3 BHK') selected @endif>3 BHK</option>
                </select>
            </div>
        </div>

            <a href="{{ asset('/real-estate/create') }}" class="btn btn-primary float-right mb-3">Add property</a>
            <div class="clearfix"></div>
        </div>
@foreach($data as $data_each)
        <div class="col-md-6">
            <div class="card mb-4">
                <div class="card-header bg-white">
                    <h5 class="card-title mb-0">{{$data_each->flat_specification}} for sale/rent</h5>
                    <span class="sub-title">{{$data_each->flat_address}}</span>
                </div>
                <div class="card-body">
                    <div class="overview open_detail_page" data-id="ff8081816c713de6016c74d19978588d" data-url="https://www.nobroker.in/property/buy/1-BHK-apartment-for-sale-in-Thane-West-mumbai/ff8081816c713de6016c74d19978588d/detail">
                        <div class="row">
                            <div itemprop="valueReference" itemscope="" itemtype="http://schema.org/PropertyValue" class="col-sm-3 solid-border-right" style="border-right:1px solid #EBEBEB;">
                                <h3>
                                    <meta itemprop="name" content="property size">
                                    <span>{{$data_each->distance_from_nearest_railway_station}}</span><span itemprop="unitText"> km</span>
                                    <meta itemprop="value" content="630">
                                </h3>
                                <h5>
                                Railway station
                                </h5>
                            </div>
                            <div itemprop="valueReference" itemscope="" itemtype="http://schema.org/PropertyValue" class="col-sm-3 solid-border-right" style="border-right:1px solid #EBEBEB;">
                                <h3>
                                    <meta itemprop="name" content="property size">
                                    <span>{{$data_each->carpet_area}}</span><span itemprop="unitText"> sqft</span>
                                    <meta itemprop="value" content="630">
                                </h3>
                                <h5>
                                <!-- Builtup / Carpet -->
                                Carpet
                                </h5>
                            </div>
                            <div class="col-sm-3 solid-border-right" itemprop="valueReference" itemscope="" itemtype="http://schema.org/PropertyValue">
                                <h3>
                                <meta itemprop="name" content="Estimated EMI">
                                <i class="icon-inr icon-x icon inr_resale" title="Rs"></i> <span>{{$data_each->deposit}}</span>
                                <meta itemprop="value" content="61062/Month">
                                </h3>
                                <h5>
                                Deposit
                                </h5>
                            </div>
                            <div class="col-sm-3" itemprop="valueReference" itemscope="" itemtype="http://schema.org/PropertyValue">
                                <h3>
                                <meta itemprop="name" content="Property Value">
                                <i class="icon-inr icon-x icon inr_resale" title="Rs"></i><span> {{$data_each->rent}} </span>
                                <meta itemprop="value" content="8700000">
                                </h3>
                                <h5>
                                <i class="icon-inr icon-x icon inr_resale" title="Rs"></i> Rent
                                </h5>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-4">
                            <div class="col-md-4">
      							<div class="slider-for">
                                    @if(@$data_each->images)
                                    @foreach(@$data_each->images as $image)
    								<a href="{{ asset('assets/images/real-estate/'.@$image->image_name) }}" class="item-slick"><img src="{{ asset('assets/images/real-estate/'.@$image->image_name) }}" alt="Alt"></a>
                                    @endforeach
                                    @endif
    								<!-- <a href="http://farm9.staticflickr.com/8382/8558295631_0f56c1284f_b.jpg" class="item-slick"><img src="http://farm9.staticflickr.com/8382/8558295631_0f56c1284f_b.jpg" alt="Alt"></a>
    								<a href="http://farm9.staticflickr.com/8225/8558295635_b1c5ce2794_b.jpg" class="item-slick"><img src="http://farm9.staticflickr.com/8225/8558295635_b1c5ce2794_b.jpg" alt="Alt"></a>
    								<a href="http://farm9.staticflickr.com/8383/8563475581_df05e9906d_b.jpg" class="item-slick"><img src="http://farm9.staticflickr.com/8383/8563475581_df05e9906d_b.jpg" alt="Alt"></a> -->
    							</div>
    							<!-- <div class="slider-nav">
    								<div class="item-slick"><img src="http://farm9.staticflickr.com/8242/8558295633_f34a55c1c6_b.jpg" alt="Alt"></div>
    								<div class="item-slick"><img src="http://farm9.staticflickr.com/8382/8558295631_0f56c1284f_b.jpg" alt="Alt"></div>
    								<div class="item-slick"><img src="http://farm9.staticflickr.com/8225/8558295635_b1c5ce2794_b.jpg" alt="Alt"></div>
    								<div class="item-slick"><img src="http://farm9.staticflickr.com/8383/8563475581_df05e9906d_b.jpg" alt="Alt"></div>
    							</div> -->
                            </div>

                        <!-- <div class="col-md-4">
                            <img src="https://images.nobroker.in/images/ff8081816c713de6016c74d19978588d/ff8081816c713de6016c74d19978588d_38382_85657_large.jpg" style="height: auto; width: 100%; top: -79px;">
                        </div> -->
                        <div class="col-md-8">
                            <div class="row detail-summary open_detail_page" data-id="ff8081816c713de6016c74d19978588d" data-url="https://www.nobroker.in/property/buy/1-BHK-apartment-for-sale-in-Thane-West-mumbai/ff8081816c713de6016c74d19978588d/detail">
                                <div class="col-sm-12">
                                    <div class="row summary-row border-bottom text-align-left">

                                        <div class="info-card property-facing col-sm-6 col-xs-6 solid-border-right text-align-left">

                                            <h5 class="semi-bold">{{$data_each->vastu}}</h5>
                                            <div class="list-card-subtext"> Facing </div>
                                        </div>
                                        <div class="info-card property-age col-sm-6 col-xs-6 text-align-left" itemprop="valueReference" itemscope="" itemtype="http://schema.org/PropertyValue">
                                            <h5 class="semi-bold">{{$data_each->floor}} </h5>
                                            <div class="list-card-subtext"> Floor</div>
                                        </div>
                                    </div>
                                    <div class="row summary-row text-align-left border-bottom">
                                        <div class="info-card property-bathrooms col-sm-6 col-xs-6 solid-border-right text-align-left">
                                            <h5 class="semi-bold"> {{$data_each->flat_specification}} </h5>
                                            <div class="list-card-subtext">Flat</div>
                                        </div>
                                        <div class="info-card property-parking col-sm-6 col-xs-6 ">
                                            <h5 class="semi-bold"> {{$data_each->preferred_tenant_type}}</h5>
                                            <div class="list-card-subtext"> Tenant Type</div>
                                        </div>
                                    </div>
                                    <div class="row summary-row text-align-left">
                                        <div class="info-card property-bathrooms col-sm-6 col-xs-6 solid-border-right text-align-left">
                                            <h5 class="semi-bold"> {{$data_each->oc_recieved}} </h5>
                                            <div class="list-card-subtext">O.C Recieved</div>
                                        </div>
                                        <div class="info-card property-parking col-sm-6 col-xs-6 ">
                                            <h5 class="semi-bold">{{$data_each->parking}}</h5>
                                            <div class="list-card-subtext"> Parking </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="button" id="get_more_details_btn" real_estate_id='{{$data_each->id}}' class="btn btn-primary btn-block btn-primary mt-2">Get More Details</button>












                    </div>
            </div>
            </div>
        </div>
    </div>
@endforeach






</div>

   <!--  <div class="container">

        <div class="card p-5">

            <div class="row">
                <form class="form-horizontal col-md-12 col-sm-12">
                    <h5 class="mb-0">Personl Information</h5>
                    <hr class="mt-2">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <label class="control-label">Name of the Owner *</label>
                                <div class="col-md-12 col-sm-12 no-padding">
                                    <input type="text" class="form-control" placeholder="eg. John Snow"  />
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Mobile No *</label>
                                    <div class="col-md-12 col-sm-12 no-padding">
                                        <input type="text" class="form-control" placeholder="eg. 0123987653"  />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Optional No</label>
                                    <div class="col-md-12 col-sm-12 no-padding">
                                        <input type="text" class="form-control" placeholder="eg. 9876543210"  />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <label class="control-label">Residence Address</label>
                                <div class="col-md-12 col-sm-12 no-padding">
                                    <input type="text" class="form-control" placeholder="eg. Abc Xyz, 12345, New York."  />
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Whether TNRWA member?</label><br>
                                    <div class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" id="customRadioInline1" name="customRadioInline2" class="custom-control-input">
                                      <label class="custom-control-label" for="customRadioInline1">Yes</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" id="customRadioInline2" name="customRadioInline2" class="custom-control-input">
                                      <label class="custom-control-label" for="customRadioInline2">No</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h5 class="mb-0 mt-5">Details of Property</h5>
                    <hr class="mt-2">
                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                            <div class="form-group">
                                <label class="control-label">Add. of flat to be rented-out/ For sale</label>
                                <div class="col-md-12 col-sm-12 no-padding">
                                    <input type="text" class="form-control" placeholder="eg. 1" />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <label class="control-label">Condition of flat</label><br>
                            <div class="custom-control custom-radio custom-control-inline">
                              <input type="radio" id="customRadioInline4" name="customRadioInline1" class="custom-control-input">
                              <label class="custom-control-label" for="customRadioInline4">Furnished</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                              <input type="radio" id="customRadioInline5" name="customRadioInline1" class="custom-control-input">
                              <label class="custom-control-label" for="customRadioInline5">Semi Furnished</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                              <input type="radio" id="customRadioInline6" name="customRadioInline1" class="custom-control-input">
                              <label class="custom-control-label" for="customRadioInline6">Unfurnished</label>
                            </div>
                        </div>
                        <div class="col-sm-5 col-md-5">
                            <label class="control-label">Available from/ To</label>
                            <div class="col-md-12 col-sm-12 no-padding">
                                <input type="text" id="availabilty_picker" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-7 col-md-7">
                            <label for="validationTooltipUsername">Offer</label>
                            <div class="row">
                                <div class="col-md-5 col-sm-5 no-padding">
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="validationTooltipUsernamePrepend">Deposit</span>
                                    </div>
                                    <input type="text" class="form-control" id="validationTooltipUsername" placeholder="" aria-describedby="validationTooltipUsernamePrepend" required>
                                  </div>
                                </div>
                                <div class="col-md-5 col-sm-5 no-padding ml-3">
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="validationTooltipUsernamePrepend">Rent</span>
                                    </div>
                                    <input type="text" class="form-control" id="validationTooltipUsername" placeholder="" aria-describedby="validationTooltipUsernamePrepend" required>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row mt-3">
                        <div class="col-sm-4 col-md-4">
                            <label class="control-label">Maintenance cleared upto</label>
                            <div class="col-md-12 col-sm-12 no-padding">
                                <input type="text" class="form-control" placeholder=""  />
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-4">
                            <label class="control-label">Other uncleared Dues / Libilities </label>
                            <div class="col-md-12 col-sm-12 no-padding">
                                <input type="text" class="form-control" placeholder=""  />
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-4">
                            <div class="form-group">
                                <label class="control-label">O.C Recieved</label><br>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline10" name="customRadioInline1" class="custom-control-input">
                                  <label class="custom-control-label" for="customRadioInline10">Yes</label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline11" name="customRadioInline1" class="custom-control-input">
                                  <label class="custom-control-label" for="customRadioInline11">No</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-sm-2 col-md-2">
                            <label class="control-label">Carpet Area of flat</label>
                            <div class="col-md-12 col-sm-12 no-padding">
                                <input type="text" class="form-control" placeholder=""  />
                            </div>
                        </div>
                        <div class="col-sm-2 col-md-2">
                            <label class="control-label">Build up Area of flat</label>
                            <div class="col-md-12 col-sm-12 no-padding">
                                <input type="text" class="form-control" placeholder=""  />
                            </div>
                        </div>
                        <div class="col-sm-2 col-md-2">
                            <div class="form-group">
                                <label class="control-label">Floor</label><br>
                                <div class="col-md-12 col-sm-12 no-padding">
                                    <input type="text" class="form-control" placeholder=""  />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Flat Specification</label>
                                <select class="form-control" id="exampleFormControlSelect1">
                                  <option>1 RK</option>
                                  <option>1 BHK</option>
                                  <option>2 BHK</option>
                                  <option>BHK</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Preferred Tenant Type</label>
                                <select class="form-control" id="exampleFormControlSelect1">
                                  <option>Family</option>
                                  <option>Batchlors</option>
                                  <option>Company</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-2 col-md-2">
                            <div class="form-group">
                                <label class="control-label">Lift</label><br>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline13" name="customRadioInline15" class="custom-control-input">
                                  <label class="custom-control-label" for="customRadioInline13">Yes</label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline14" name="customRadioInline15" class="custom-control-input">
                                  <label class="custom-control-label" for="customRadioInline14">No</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label class="control-label">Parking 2/4 Wheeler</label><br>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline15" name="customRadioInline16" class="custom-control-input">
                                  <label class="custom-control-label" for="customRadioInline15">Yes</label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline16" name="customRadioInline16" class="custom-control-input">
                                  <label class="custom-control-label" for="customRadioInline16">No</label>
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-2 col-md-2">
                            <div class="form-group">
                                <label class="control-label">Gas Pipeline</label><br>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline17" name="customRadioInline17" class="custom-control-input">
                                  <label class="custom-control-label" for="customRadioInline17">Yes</label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline18" name="customRadioInline17" class="custom-control-input">
                                  <label class="custom-control-label" for="customRadioInline18">No</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label class="control-label">24 HRs. Secyrity, CCTV</label><br>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline19" name="customRadioInline19" class="custom-control-input">
                                  <label class="custom-control-label" for="customRadioInline19">Yes</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline20" name="customRadioInline19" class="custom-control-input">
                                  <label class="custom-control-label" for="customRadioInline20">No</label>
                                </div>

                            </div>
                        </div>

                        <div class="col-sm-2 col-md-2">
                            <div class="form-group">
                                <label class="control-label">Watter Supply</label><br>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline21" name="customRadioInline21" class="custom-control-input">
                                  <label class="custom-control-label" for="customRadioInline21">Yes</label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline22" name="customRadioInline21" class="custom-control-input">
                                  <label class="custom-control-label" for="customRadioInline22">No</label>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Description of flat which you wish to specify</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                </form>


        </div>


    </div> -->








<!-- model starts -->

 <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="get_more_details_modal">
    <div class="modal-dialog modal-lg modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
               <h5 class="modal-title" id="flat_address"></h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
               </button>
             </div>
             <div class="modal-body">
                 <div class="col-md-12 mb-4">
                     <!-- <h5>Porperty Details</h5>
                     <p id="description"></p> -->
                     <div class="text-center">
                          <h3 class="mb-0">CONTACT US <br> 9869017994 / 9987504918</h3>
                          <p class="text-muted">For more details contact us</p>
                     </div>
                 </div>
                 <div class="overview open_detail_page" data-id="ff8081816c713de6016c74d19978588d" data-url="https://www.nobroker.in/property/buy/1-BHK-apartment-for-sale-in-Thane-West-mumbai/ff8081816c713de6016c74d19978588d/detail">
                     <div class="row">
                         <div itemprop="valueReference" itemscope="" itemtype="http://schema.org/PropertyValue" class="col-sm-3 solid-border-right" style="border-right:1px solid #EBEBEB;">
                             <h3>
                                 <meta itemprop="name" content="property size">
                                 <span id="railway_station">0.5</span><!-- <span itemprop="unitText"> km</span> -->
                                 <meta itemprop="value" content="630">
                             </h3>
                             <h5>
                             Railway station
                             </h5>
                         </div>
                         <div itemprop="valueReference" itemscope="" itemtype="http://schema.org/PropertyValue" class="col-sm-3 solid-border-right" style="border-right:1px solid #EBEBEB;">
                             <h3>
                                 <meta itemprop="name" content="property size">
                                 <span id="build_up_area">630</span>/<span id="carpet_area">630</span><span itemprop="unitText"> sqft</span>
                                 <meta itemprop="value" content="630">
                             </h3>
                             <h5>
                             Builtup / Carpet
                             </h5>
                         </div>
                         <div class="col-sm-3 solid-border-right" itemprop="valueReference" itemscope="" itemtype="http://schema.org/PropertyValue">
                             <h3>
                             <meta itemprop="name" content="Estimated EMI">
                             <i class="icon-inr icon-x icon inr_resale" title="Rs"></i> <span id="deposit">61,062</span>
                             <meta itemprop="value" content="61062/Month">
                             </h3>
                             <h5>
                             Deposit
                             </h5>
                         </div>
                         <div class="col-sm-3" itemprop="valueReference" itemscope="" itemtype="http://schema.org/PropertyValue">
                             <h3>
                             <meta itemprop="name" content="Property Value">
                             <i class="icon-inr icon-x icon inr_resale" title="Rs"></i><span id="rent"></span>
                             <meta itemprop="value" content="8700000">
                             </h3>
                             <h5>
                             <i class="icon-inr icon-x icon inr_resale" title="Rs"></i> Rent
                             </h5>
                         </div>
                     </div>
                 </div>
                 <div class="row mt-4">
                    <!-- <div class="col-md-4">
                         <div class="slider-for" id="modal_image">
                          @if(@$data_each->images)
                             @foreach(@$data_each->images as $image)
                             <a href="{{ asset('assets/images/real-estate/'.@$image->image_name) }}" class="item-slick"><img src="{{ asset('assets/images/real-estate/'.@$image->image_name) }}" alt="Alt"></a>
                             @endforeach
                             @endif
                             <a href="http://farm9.staticflickr.com/8382/8558295631_0f56c1284f_b.jpg" class="item-slick"><img src="http://farm9.staticflickr.com/8382/8558295631_0f56c1284f_b.jpg" alt="Alt"></a>
                             <a href="http://farm9.staticflickr.com/8225/8558295635_b1c5ce2794_b.jpg" class="item-slick"><img src="http://farm9.staticflickr.com/8225/8558295635_b1c5ce2794_b.jpg" alt="Alt"></a>
                             <a href="http://farm9.staticflickr.com/8383/8563475581_df05e9906d_b.jpg" class="item-slick"><img src="http://farm9.staticflickr.com/8383/8563475581_df05e9906d_b.jpg" alt="Alt"></a> --
                         </div>
                         <!- <img src="https://images.nobroker.in/images/ff8081816c713de6016c74d19978588d/ff8081816c713de6016c74d19978588d_38382_85657_large.jpg" style="height: auto; width: 100%; top: -79px;">
                     </div>-->
                     <div class="col-md-12">
                         <div class="row detail-summary open_detail_page" data-id="ff8081816c713de6016c74d19978588d" data-url="https://www.nobroker.in/property/buy/1-BHK-apartment-for-sale-in-Thane-West-mumbai/ff8081816c713de6016c74d19978588d/detail">
                             <div class="col-sm-12">
                                 <div class="row summary-row border-bottom text-align-left">

                                     <div class="info-card property-facing col-sm-6 col-xs-6 solid-border-right text-align-left">

                                         <h5 class="semi-bold" id="facing"></h5>
                                         <div class="list-card-subtext"> Facing </div>
                                     </div>
                                     <div class="info-card property-age col-sm-6 col-xs-6 text-align-left" itemprop="valueReference" itemscope="" itemtype="http://schema.org/PropertyValue">
                                         <h5 class="semi-bold" id="floor">3rd </h5>
                                         <div class="list-card-subtext"> Floor</div>
                                     </div>
                                 </div>
                                 <div class="row summary-row text-align-left border-bottom">
                                     <div class="info-card property-bathrooms col-sm-6 col-xs-6 solid-border-right text-align-left">
                                         <h5 class="semi-bold" id="flat_specification">  </h5>
                                         <div class="list-card-subtext">Flat Specification</div>
                                     </div>
                                     <div class="info-card property-parking col-sm-6 col-xs-6 ">
                                         <h5 class="semi-bold" id="preferred_tenant_type"> </h5>
                                         <div class="list-card-subtext"> Tenant Type </div>
                                     </div>
                                 </div>
                                 <div class="row summary-row text-align-left">
                                     <div class="info-card property-bathrooms col-sm-6 col-xs-6 solid-border-right text-align-left">
                                         <h5 class="semi-bold" id="oc_recieved"> </h5>
                                         <div class="list-card-subtext">O.C Recieved</div>
                                     </div>
                                     <div class="info-card property-parking col-sm-6 col-xs-6 ">
                                         <h5 class="semi-bold" id="parking"></h5>
                                         <div class="list-card-subtext"> Parking </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>

                 <div class="mt-4"></div>
                 <div class="row">
                     <div class="col-md-6">
                         <div class="card card-body">
                             <div class="border-bottom pt-3 pb-3">
                                <!-- <i class="fa fa-user mr-2 text-dark"></i> -->
                                <strong class="text-dark">Available</strong>
                                <span class="float-right" id="availabe"></span>
                             </div>
                             <div class="border-bottom pt-3 pb-3">
                                <!-- <i class="fa fa-user mr-2 text-dark"></i> -->
                                <strong class="text-dark">Condition of flat</strong>
                                <span class="float-right" id="flat_condition"></span>
                             </div>
                             <div class="border-bottom pt-3 pb-3">
                                <!-- <i class="fa fa-user mr-2 text-dark"></i> -->
                                <strong class="text-dark">Maintenance cleared upto</strong>
                                <span class="float-right" id="maintenance_cleared_upto"></span>
                             </div>
                             <div class="pt-3 pb-3">
                                <!-- <i class="fa fa-user mr-2 text-dark"></i> -->
                                <strong class="text-dark">Other Dues</strong>
                                <span class="float-right" id="other_unclear_dues"></span>
                             </div>
                         </div>
                     </div>
                     <div class="col-md-6">
                         <div class="card card-body">
                             <div class="border-bottom pt-3 pb-3">
                                <!-- <i class="fa fa-user mr-2 text-dark"></i> -->
                                <strong class="text-dark">Lift</strong>
                                <span class="float-right" id="lift"></span>
                             </div>
                             <div class="border-bottom pt-3 pb-3">
                                <!-- <i class="fa fa-user mr-2 text-dark"></i> -->
                                <strong class="text-dark">Gas Pipeline</strong>
                                <span class="float-right" id="gas_pipeline"></span>
                             </div>
                             <div class="border-bottom pt-3 pb-3">
                                <!-- <i class="fa fa-user mr-2 text-dark"></i> -->
                                <strong class="text-dark">24 HRs. Secyrity, CCTV</strong>
                                <span class="float-right" id="cctv"></span>
                             </div>
                             <div class="pt-3 pb-3">
                                <!-- <i class="fa fa-user mr-2 text-dark"></i> -->
                                <strong class="text-dark">Water Supply</strong>
                                <span class="float-right" id="water_supply"></span>
                             </div>
                         </div>
                     </div>
                     <!-- <div class="col-md-12 mt-4">
                         <div class="text-center">
                              <h3 class="mb-0">CONTACT US <br> 9869017994 / 9987504918</h3>
                              <p class="text-muted">For more details contact us</p>
                         </div>
                     </div> -->
                 </div>

             <!-- <div class="modal-footer">
                 <button type="button" class="btn btn-primary">Save changes</button> --
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
             </div> -->
        </div>
      </div>
    </div>
</div>


                        <!-- modal ends -->

  </section>
   <input type="hidden" id="token" name="_token" value="{{csrf_token()}}"/>
  <!--================ Sponsor section End =================-->

  <!-- ================ start footer Area ================= -->
@include('include.front.footer')
  <!-- ================ End footer Area ================= -->

<!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>

<script type="text/javascript">
     var token = $('#token').val();
    $("#availabilty_picker").daterangepicker();

function addSlider(){
  var $carousel = $('.slider-for');

  $carousel
    .slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      fade: true,
      adaptiveHeight: false,
      prevArrow:'<span class="slick-prev"><i class="fa fa-angle-left"></i></span>',
      nextArrow:'<span class="slick-next"><i class="fa fa-angle-right"></i></span>',
      // asNavFor: '.slider-nav'
    })

    .magnificPopup({
      type: 'image',
      delegate: 'a:not(.slick-cloned)',
      closeOnContentClick: false,
      tLoading: 'Загрузка...',
      mainClass: 'mfp-zoom-in mfp-img-mobile',
      image: {
        verticalFit: true,
        tError: '<a href="%url%">Фото #%curr%</a> не загрузилось.'
      },
      gallery: {
        enabled: true,
        navigateByImgClick: true,
        tCounter: '<span class="mfp-counter">%curr% из %total%</span>', // markup of counte
        preload: [0,1] // Will preload 0 - before current, and 1 after the current image
      },
      zoom: {
        enabled: true,
        duration: 300
      },
      removalDelay: 300, //delay removal by X to allow out-animation
      callbacks: {
        open: function() {
          //overwrite default prev + next function. Add timeout for css3 crossfade animation
          $.magnificPopup.instance.next = function() {
            var self = this;
            self.wrap.removeClass('mfp-image-loaded');
            setTimeout(function() { $.magnificPopup.proto.next.call(self); }, 120);
          };
          $.magnificPopup.instance.prev = function() {
            var self = this;
            self.wrap.removeClass('mfp-image-loaded');
            setTimeout(function() { $.magnificPopup.proto.prev.call(self); }, 120);
          };
          var current = $carousel.slick('slickCurrentSlide');
          $carousel.magnificPopup('goTo', current);
        },
        imageLoadComplete: function() {
          var self = this;
          setTimeout(function() { self.wrap.addClass('mfp-image-loaded'); }, 16);
        },
        beforeClose: function() {
          $carousel.slick('slickGoTo', parseInt(this.index));
        }
      }
    });
}

addSlider();

//     $(function() {
// // Card's slider

//   // $('.slider-nav').slick({
//   //   slidesToShow: 3,
//   //   slidesToScroll: 1,
//   //   asNavFor: '.slider-for',
//   //   dots: false,
//   //   centerMode: false,
//   //   focusOnSelect: true,
//   //   variableWidth: true
//   // });


// });
</script>

<script type="text/javascript">
    $(document).ready(function(){


    $(document).on('click','#get_more_details_btn',function(){
        var real_estate_id = $(this).attr("real_estate_id");
        $.ajax({
            'url':path+'get-real-estate-details',
            type:"get",
            data:{_token:token,real_estate_id:real_estate_id},
            success:function(data,status){

                    $('#flat_address').text(data['flat_specification']+' flat for '+data['type']+' in '+data['flat_address']);
                    $('#build_up_area').text(data['build_up_area']);
                    $('#carpet_area').text(data['carpet_area']);
                    $('#deposit').text(data['deposit']);
                    $('#rent').text(data['rent']);
                    $('#facing').text(data['vastu']);
                    $('#floor').text(data['floor']);
                    $('#flat_specification').text(data['flat_specification']);
                    $('#preferred_tenant_type').text(data['preferred_tenant_type']);
                    $('#oc_recieved').text(data['oc_recieved']);
                    $('#parking').text(data['parking']);
                    $('#availabe').text(data['available_from']+' - '+data['available_to']);
                    $('#flat_condition').text(data['flat_condition']);
                    $('#maintenance_cleared_upto').text(data['maintenance_cleared_upto']);
                    $('#other_unclear_dues').text(data['other_unclear_dues']);
                    $('#lift').text(data['lift']);
                    $('#gas_pipeline').text(data['gas_pipeline']);
                    $('#cctv').text(data['cctv']);
                    $('#water_supply').text(data['water_supply']);
                    $('#description').text(data['description']);
                    var image_data = '';

                    if (data['images']) {
                      $.each(data['images'],function(key,value){

                        image_data += '<a href="'+path+'/assets/images/real-estate/'+value['image_name']+'" class="item-slick"><img src="'+path+'/assets/images/real-estate/'+value['image_name']+'" alt="Alt"></a>';

                      });
                    }

                    console.log(image_data);
                    $('#modal_image').html(image_data);
                    addSlider();




            }
        });
        $("#get_more_details_modal").modal('show');


    });
    });
</script>

<script type="text/javascript">
  $('#spec_filter').change(function(){
    applyFilter();
  });
  $('#type_filter').change(function(){
    applyFilter();
  });

  function applyFilter(){
    var type = $("#type_filter").val();
    var spec_filter = $('#spec_filter').val();
    window.location.href = path+'real-estate/'+type+'/'+spec_filter;
  }
</script>
</body>
</html>
