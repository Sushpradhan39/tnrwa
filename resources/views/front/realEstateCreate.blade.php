<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>TNRWA - Real Estate Form</title>
  @include('include.front.head')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/3.0.5/daterangepicker.min.css" />
  <style>
  .file {
    visibility: hidden;
    position: absolute;
  }
  </style>
</head>
<body>

  <!--================ Header Menu Area start =================-->
  @include('include.front.header')
  <!--================Header Menu Area =================-->


  <!--================Hero Banner Area Start =================-->
  <section class="hero-banner hero-banner-sm">
    <div class="container text-center">
      <h2>Real Estate</h2>
      <nav aria-label="breadcrumb" class="banner-breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ asset('/') }}">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Real Estate</li>
        </ol>
      </nav>
    </div>
  </section>
  <!--================Hero Banner Area End =================-->

  <!--================ Sponsor section Start =================-->
  <section class="section-padding--small sponsor-bg">
    <div class="container">
        <!-- <div class="row">

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header bg-white">
                        <h5 class="card-title mb-0">Property</h5>
                        <span class="sub-title">This is ajkjklaf</span>
                    </div>
                    <div class="card-body">
                        <div class="overview open_detail_page" data-id="ff8081816c713de6016c74d19978588d" data-url="https://www.nobroker.in/property/buy/1-BHK-apartment-for-sale-in-Thane-West-mumbai/ff8081816c713de6016c74d19978588d/detail">
                            <div class="row">
                                <div class="col-sm-1 solid-border-right" style="border-right:1px solid #EBEBEB;">
                                    <div><img data-toggle="tooltip" title="" src="//assets.nobroker.in/static/img/resale/title_unverif.png" data-original-title="Title not Verified"></div>
                                    <div><img data-toggle="tooltip" title="" src="//assets.nobroker.in/static/img/resale/loan_verif.png" data-original-title="Under Loan"></div>
                                </div>
                                <div itemprop="valueReference" itemscope="" itemtype="http://schema.org/PropertyValue" class="col-sm-4 solid-border-right" style="border-right:1px solid #EBEBEB;">
                                    <h3>
                                        <meta itemprop="name" content="property size">
                                        <span>630</span><span itemprop="unitText"> sqft</span>
                                        <meta itemprop="value" content="630">
                                    </h3>
                                    <h5>
                                    Builtup
                                    </h5>
                                </div>
                                <div class="col-sm-4 solid-border-right" itemprop="valueReference" itemscope="" itemtype="http://schema.org/PropertyValue">
                                    <h3>
                                    <meta itemprop="name" content="Estimated EMI">
                                    <i class="icon-inr icon-x icon inr_resale" title="Rs"></i> <span>61,062/Month</span>
                                    <meta itemprop="value" content="61062/Month">
                                    </h3>
                                    <h5>
                                    Estimated EMI
                                    </h5>
                                </div>
                                <div class="col-sm-3" itemprop="valueReference" itemscope="" itemtype="http://schema.org/PropertyValue">
                                    <h3>
                                    <meta itemprop="name" content="Property Value">
                                    <i class="icon-inr icon-x icon inr_resale" title="Rs"></i><span> 87 Lacs</span>
                                    <meta itemprop="value" content="8700000">
                                    </h3>
                                    <h5>
                                    <i class="icon-inr icon-x icon inr_resale" title="Rs"></i> 13,809 per sq. ft.
                                    </h5>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-4">
                            <div class="col-md-4">
                                <img src="https://images.nobroker.in/images/ff8081816c713de6016c74d19978588d/ff8081816c713de6016c74d19978588d_38382_85657_large.jpg" style="height: auto; width: 100%; top: -79px;">
                            </div>
                            <div class="col-md-8">
                                <div class="row detail-summary open_detail_page" data-id="ff8081816c713de6016c74d19978588d" data-url="https://www.nobroker.in/property/buy/1-BHK-apartment-for-sale-in-Thane-West-mumbai/ff8081816c713de6016c74d19978588d/detail">
                                    <div class="col-sm-12">
                                    <div class="row summary-row border-bottom text-align-left">
                                    <div class="info-card property-facing col-sm-6 col-xs-6 solid-border-right text-align-left">
                                    <h5 class="semi-bold">East
                                    </h5>
                                    <div class="list-card-subtext"> Facing </div>
                                    </div>
                                    <div class="info-card property-age col-sm-6 col-xs-6 text-align-left" itemprop="valueReference" itemscope="" itemtype="http://schema.org/PropertyValue">
                                    <h5 class="semi-bold">3-5 years </h5>
                                    <div class="list-card-subtext"> Property Age</div>
                                    </div>
                                    </div>
                                    <div class="row summary-row text-align-left">
                                    <div class="info-card property-bathrooms col-sm-6 col-xs-6 solid-border-right text-align-left">
                                    <h5 class="semi-bold">
                                    2
                                    </h5>
                                    <div class="list-card-subtext">
                                    Bathrooms
                                    </div>
                                    </div>
                                    <div class="info-card property-parking col-sm-6 col-xs-6 ">
                                    <h5 class="semi-bold">
                                    Bike
                                    </h5>
                                    <div class="list-card-subtext"> Parking </div>
                                    </div>
                                    </div>
                                    </div>
                                    </div>

                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Large modal</button>

                                    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                      <div class="modal-dialog modal-lg modal-dialog modal-dialog-centered">
                                        <div class="modal-content">

                                        </div>
                                      </div>
                                    </div>


                                    <button data-sponsored="true"  class="btn btn-block btn-primary mt-2">Get More Details</button>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->

        <div class="card p-5">

            <div class="row">
                <form class="form-horizontal col-md-12 col-sm-12" id="real_estate_form">
                  <input type='hidden' name='_token' value='{{csrf_token()}}'/>
                    <h5 class="mb-0">Personl Information</h5>
                    <hr class="mt-2">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <label class="control-label">Name of the Owner *</label>
                                <div class="col-md-12 col-sm-12 no-padding">
                                    <input type="text" name="owner_name" class="form-control" placeholder="" required="" />
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Mobile No *</label>
                                    <div class="col-md-12 col-sm-12 no-padding">
                                        <input type="text" name="mobile_number" class="form-control" placeholder="" required=""  />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Optional No</label>
                                    <div class="col-md-12 col-sm-12 no-padding">
                                        <input type="text" name="optional_number" class="form-control" placeholder=""  />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <label class="control-label">Residence Address</label>
                                <div class="col-md-12 col-sm-12 no-padding">
                                    <input type="text" class="form-control" name="address" placeholder=""  />
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Whether TNRWA member?</label><br>
                                    <div class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" id="customRadioInline1" name="tnrwa_member" class="custom-control-input" value="yes" >
                                      <label class="custom-control-label" for="customRadioInline1">Yes</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                      <input type="radio" id="customRadioInline2" name="tnrwa_member" class="custom-control-input" value="no" checked="">
                                      <label class="custom-control-label" for="customRadioInline2">No</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <h5 class="mb-0 mt-5">Details of Property</h5>
                    <hr class="mt-2">

                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                            <label class="control-label">Type: </label>
                            <div class="col-md-12 col-sm-12 no-padding">
                                <select class="form-control" name="type" id="exampleFormControlSelect3" required="">
                                  <option value="">Select Type</option>
                                  <option value="sale">Sale</option>
                                  <option value="rent">Rent</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                            <div class="form-group">
                                <label class="control-label">Add. of flat to be rented-out/ For sale</label>
                                <div class="col-md-12 col-sm-12 no-padding">
                                    <input type="text" class="form-control" name="flat_address" placeholder="" required="" />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <label class="control-label">Condition of flat</label><br>
                            <div class="custom-control custom-radio custom-control-inline">
                              <input type="radio" id="customRadioInline4" name="flat_condition" class="custom-control-input" value="furnished" >
                              <label class="custom-control-label" for="customRadioInline4">Furnished</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                              <input type="radio" id="customRadioInline5" name="flat_condition" class="custom-control-input" value="Semi Furnished">
                              <label class="custom-control-label" for="customRadioInline5">Semi Furnished</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                              <input type="radio" id="customRadioInline6" name="flat_condition" class="custom-control-input" value="Unfurnished" checked="">
                              <label class="custom-control-label" for="customRadioInline6">Unfurnished</label>
                            </div>
                        </div>
                        <div class="col-sm-5 col-md-5">
                            <label class="control-label">Available from/ To</label>
                            <div class="col-md-12 col-sm-12 no-padding">
                                <input type="text" id="availabilty_picker" name="available_from_to" class="form-control" value="available_from_to" />
                            </div>
                        </div>
                        <div class="col-sm-7 col-md-7">
                            <label for="validationTooltipUsername">Offer</label>
                            <div class="row">
                                <div class="col-md-5 col-sm-5 no-padding">
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="validationTooltipUsernamePrepend">Deposit</span>
                                    </div>
                                    <input type="text" class="form-control" id="validationTooltipUsername" name="deposit" placeholder="" aria-describedby="validationTooltipUsernamePrepend" >
                                  </div>
                                </div>
                                <div class="col-md-5 col-sm-5 no-padding ml-3">
                                  <div class="input-group">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="validationTooltipUsernamePrepend">Rent</span>
                                    </div>
                                    <input type="text" class="form-control" id="validationTooltipUsername" name="rent" placeholder="" aria-describedby="validationTooltipUsernamePrepend" >
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row mt-3">
                        <div class="col-sm-4 col-md-4">
                            <label class="control-label">Maintenance cleared upto</label>
                            <div class="col-md-12 col-sm-12 no-padding">
                                <input type="text" class="form-control" placeholder=""  name="maintenance_cleared_upto" />
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-4">
                            <label class="control-label">Other uncleared Dues / Libilities </label>
                            <div class="col-md-12 col-sm-12 no-padding">
                                <input type="text" class="form-control" placeholder="" name="other_unclear_dues" />
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-4">
                            <div class="form-group">
                                <label class="control-label">O.C Recieved</label><br>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline10" name="oc_recieved" class="custom-control-input" value="yes" >
                                  <label class="custom-control-label" for="customRadioInline10">Yes</label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline11" name="oc_recieved" class="custom-control-input" value="no" checked="">
                                  <label class="custom-control-label" for="customRadioInline11">No</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-sm-2 col-md-2">
                            <label class="control-label">Carpet Area of flat</label>
                            <div class="col-md-12 col-sm-12 no-padding">
                                <input type="text" class="form-control" placeholder=""  name="carpet_area" />
                            </div>
                        </div>
                        <div class="col-sm-2 col-md-2">
                            <label class="control-label">Build up Area of flat</label>
                            <div class="col-md-12 col-sm-12 no-padding">
                                <input type="text" class="form-control" placeholder="" name="build_up_area" />
                            </div>
                        </div>
                        <div class="col-sm-2 col-md-2">
                            <div class="form-group">
                                <label class="control-label">Floor</label><br>
                                <div class="col-md-12 col-sm-12 no-padding">
                                    <input type="text" class="form-control" placeholder="" name="floor" />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Flat Specification</label>
                                <select class="form-control" id="exampleFormControlSelect1" name="flat_specification">
                                  <option value="1 RK">1 RK</option>
                                  <option value="1 BHK">1 BHK</option>
                                  <option value="2 BHK">2 BHK</option>
                                  <option value="3 BHK">3 BHK</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Preferred Tenant Type</label>
                                <select class="form-control" name="preferred_tenant_type" id="exampleFormControlSelect1">
                                  <option value="Family">Family</option>
                                  <option value="Batchlors">Batchlors</option>
                                  <option value="Company">Company</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-2 col-md-2">
                            <div class="form-group">
                                <label class="control-label">Lift</label><br>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline13" name="Lift" class="custom-control-input" value="yes">
                                  <label class="custom-control-label" for="customRadioInline13">Yes</label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline14" name="Lift" class="custom-control-input" value="no" checked="">
                                  <label class="custom-control-label" for="customRadioInline14">No</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label class="control-label">Parking 2/4 Wheeler</label><br>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline15" name="parking" class="custom-control-input" value="yes">
                                  <label class="custom-control-label" for="customRadioInline15">Yes</label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline16" name="parking" class="custom-control-input" value="no" checked="">
                                  <label class="custom-control-label" for="customRadioInline16">No</label>
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-2 col-md-2">
                            <div class="form-group">
                                <label class="control-label">Gas Pipeline</label><br>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline17" name="gas_pipeline" class="custom-control-input" value="yes">
                                  <label class="custom-control-label" for="customRadioInline17">Yes</label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline18" name="gas_pipeline" class="custom-control-input" value="no" checked="">
                                  <label class="custom-control-label" for="customRadioInline18">No</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label class="control-label">24 HRs. Secyrity, CCTV</label><br>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline19" name="cctv" class="custom-control-input" value="yes">
                                  <label class="custom-control-label" for="customRadioInline19">Yes</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline20" name="cctv" class="custom-control-input" value="no" checked="">
                                  <label class="custom-control-label" for="customRadioInline20">No</label>
                                </div>

                            </div>
                        </div>

                        <div class="col-sm-2 col-md-2">
                            <div class="form-group">
                                <label class="control-label">Water Supply</label><br>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline21" name="water_supply" class="custom-control-input" value="yes">
                                  <label class="custom-control-label" for="customRadioInline21">Yes</label>
                                </div>

                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="customRadioInline22" name="water_supply" class="custom-control-input" value="no" checked="">
                                  <label class="custom-control-label" for="customRadioInline22">No</label>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                      <div class="col-sm-4 col-md-4">
                          <div class="form-group">
                              <label class="control-label">Distance from nearest railway station</label><br>
                              <div>
                                <input type="text" id="" name="distance_from_nearest_railway_station" class="form-control">

                              </div>


                          </div>
                        </div>

                         <div class="col-sm-4 col-md-4">

                              <div class="form-group">
                                  <label for="exampleFormControlSelect1">Vastu</label>
                                  <select class="form-control" id="exampleFormControlSelect1" name="vastu">
                                    <option>East</option>
                                    <option>West</option>
                                    <option>South</option>
                                    <option>North</option>
                                  </select>
                              </div>
                          </div>

                          <div class="col-sm-4 col-md-4">
                            <label>Select Images</label>
                              <!-- <div class="custom-file">

                              <input type="file"  name="files[]"  class="custom-file-input" id="customFileLang" lang="es" multiple>
                              <label class="custom-file-label" for="customFileLang">Select Images</label>
                            </div> -->
                            <!--<div class="form-group">

                              <!--  <div class="input-group mb-3"> -->
                                 <!-- <input type="file" name="files[]"  multiple> -->
                                 <!-- <div class="input-group-prepend">
                                   <span class="input-group-text" id="basic-addon1"><i class="fas fa-paperclip"></i></span>
                                 </div> -->
                                 <!-- <input type="text" class="form-control custom-file-inpu" placeholder="Upload File" aria-label="Upload File" aria-describedby="basic-addon1"> -->
                                <!--  <div class="input-group-append">
                                   <button class="browse input-group-text btn btn-primary" id="basic-addon2">  Browse</button>
                                 </div> -->
                               <!-- </div> --
                           </div> -->

                             <div class="input-group">
                               <div class="custom-file">
                                   <input type="file"  name="files[]"  multiple class="custom-file-input" id="myInput" aria-describedby="myInput">
                                   <label class="custom-file-label" for="myInput">Choose file</label>
                               </div>
                           </div>
                          <!-- <div class="form-group">
                            <input type="file" name="files[]" multiple>
                          </div> -->
                        </div>

                    </div>




                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Description of flat which you wish to specify</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="description"></textarea>
                            </div>

                            <button type="submit" class="btn btn-primary float-right">Submit</button>
                        </div>


                    </div>



                </form>


        </div>


    </div>
  </section>
  <!--================ Sponsor section End =================-->

  <!-- ================ start footer Area ================= -->
@include('include.front.footer')
  <!-- ================ End footer Area ================= -->

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script type="text/javascript">
    $("#availabilty_picker").daterangepicker();
    document.querySelector('.custom-file-input').addEventListener('change',function(e){
          //var fileName = document.getElementById("myInput").files[0].name;
          var fileName = document.getElementById("myInput").files;
          var nextSibling = e.target.nextElementSibling
          var arrlen = fileName.length ;
          var i;
          for ( i = 0; i < fileName.length; i++) {
             // text += cars[i] + "<br>";
             var len = fileName.length;
              var nam = fileName[i].name ;
              if ( len <= 1 ) {
                 nextSibling.innerText = nam;
             }else {
                nextSibling.innerText = fileName.length + ' files' ;
             }

            }
          //nextSibling.innerText = fileName
        })
</script>
 <script type="text/javascript">
        // path = {!! json_encode(url('/')) !!} + '/';
      $('#real_estate_form').submit(function(e){
      e.preventDefault();
       $.ajax({
         url:path+'real-estate/create',
         type:'post',
         data:new FormData(this),
         contentType:false,
         processData:false,
         success:function(data,status)
         {
            if(data == 1){

                swal({
                        title: 'Success!',
                        text: "Thank You!Your request has been submited successfully.",
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Ok'
                    }).then(function (result) {
                        if (result.value) {
                          window.location.href = path+'real-estate';
                        }
                    });

            }else{
                alert(data);
            }


         }
       });
      });


   </script>
 </body>
</html>
