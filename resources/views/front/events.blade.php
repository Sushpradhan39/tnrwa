<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>TNRWA - Events</title>
	@include('include.front.head')
</head>
<body>
	@include('include.front.header')
	<section class="hero-banner hero-banner-sm">
		<div class="container text-center">
	      	<h2>Events</h2>
	      	<nav aria-label="breadcrumb" class="banner-breadcrumb hide">
		        <ol class="breadcrumb">
		          	<li class="breadcrumb-item"><a href="#">Home</a></li>
		          	<li class="breadcrumb-item active" aria-current="page">Gallery</li>
		        </ol>
		    	</nav>
					<p style="font-size: 16px;" class="breadcrumbs_desc">Events play very important part in everyone's life. These events come and go but the learning and the knowledge remains with us forever</p>
		</div>
	</section>
	<section class="section-padding--large gallery-area">
		<div class="container">

				<div class="row no-gutters">

                @foreach($events as $Key => $event)
					<div style="padding: 5px;" class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<a href="{{ asset('event_details/'.@$event->id) }}">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 card_background card-speaker no-padding">
								<img src="{{ asset('assets/images/events/'.@$event->display_image) }}" />
								<div class="speaker-footer">
					           <h4>{{@$event->title}}</h4>
										 <p>{{@$event->sub_title}}</p>
					      </div>
							</div>
						</a>
					</div>
                 @endforeach
				
				
				</div>

			</div>
		</div>
	</section>
	@include('include.front.footer')
</body>
</html>
