<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>TNRWA - Contact Us</title>

    @include('include.front.head')

</head>
<body>

    @include('include.front.header')

  <!--================Hero Banner Area Start =================-->
  <section class="hero-banner hero-banner-sm">
    <div class="container text-center">
      <h2>Contact Us</h2>
      <nav aria-label="breadcrumb" class="banner-breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ asset('/') }}">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
        </ol>
      </nav>
    </div>
  </section>
  <!--================Hero Banner Area End =================-->


  <!-- ================ contact section start ================= -->
  <section class="section-margin--large">
    <div class="container">
      <div class="d-none d-sm-block mb-5 pb-4">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15083.563467454154!2d72.89156612907297!3d19.068535293473644!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7c620eaa8e63b%3A0x9cb668d788fca729!2sTilak%20Nagar%20Residents%20Welfare%20Association!5e0!3m2!1sen!2sin!4v1566665788539!5m2!1sen!2sin" width="100%" height="480" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        <!-- <div id="map" style="height: 480px;"></div>
        <script>
          function initMap() {
            var uluru = {lat: -25.363, lng: 131.044};
            var grayStyles = [
              {
                featureType: "all",
                stylers: [
                  { saturation: -90 },
                  { lightness: 50 }
                ]
              },
              {elementType: 'labels.text.fill', stylers: [{color: '#A3A3A3'}]}
            ];
            var map = new google.maps.Map(document.getElementById('map'), {
              center: {lat: -31.197, lng: 150.744},
              zoom: 9,
              styles: grayStyles,
              scrollwheel:  false
            });
          }

        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpfS1oRGreGSBU5HHjMmQ3o5NLw7VdJ6I&callback=initMap"></script> -->

      </div>


      <div class="row">
        <div class="col-12">
          <h2 class="contact-title">Get in Touch</h2>
        </div>
        <div class="col-lg-8">
          <form class="form-contact contact_form"  id="contact_form" >
             <input type='hidden' name='_token' value='{{csrf_token()}}'/>
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <input class="form-control" name="name" id="name" type="text" placeholder="Enter your name" required="">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <input class="form-control" name="email_id" id="email" type="email" placeholder="Enter email address">
                </div>
              </div>
              <div class="col-12">
                <div class="form-group">
                  <input class="form-control" name="contact_no" id="contact_no" type="text" placeholder="Enter Contact No" required="">
                </div>
              </div>
              <div class="col-12">
                <div class="form-group">
                  <input class="form-control" name="subject" id="subject" type="text" placeholder="Enter Subject">
                </div>
              </div>
            </div>
              <div class="col-12">
                <div class="form-group">
                    <textarea class="form-control w-100" name="message" id="message" cols="30" rows="9" placeholder="Enter Message"></textarea>
                </div>
              </div>
            <div class="form-group mt-3">
              <button type="submit" class="button button-contactForm">Send Message</button>
            </div>
          </form>


        </div>

        <div class="col-lg-4">
          <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-home"></i></span>
            <div class="media-body">
              <h3>Tilak Nagar Residents Welfare Association</h3>
              <p style="margin-bottom: 5px;">Registration no.: GBBSD-1101/15</p>
              <p style="margin-bottom: 5px;">Build.No.21/738 A-Wing,</p>
              <p style="margin-bottom: 5px;">Tilak Nagar Chembur,</p>
              <p>Mumbai - 400 089</p>
            </div>
          </div>
          <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-tablet"></i></span>
            <div class="media-body">
              <h3><a href="tel:454545654">9869017994/ 9987504918</a></h3>
              <p>Mon to Fri 9am to 6pm</p>
            </div>
          </div>
          <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-email"></i></span>
            <div class="media-body">
              <h3><a href="mailto:support@colorlib.com">info@tnrwa.org</a></h3>
              <p>Send us your query anytime!</p>
            </div>
          </div>
        </div>














        <div class="col-md-4 col-lg-3 mb-4 mb-md-0">

        </div>
        <div class="col-md-8 col-lg-9">

        </div>
      </div>
    </div>
  </section>
	<!-- ================ contact section end ================= -->

    @include('include.front.footer')

</body>
 <script type="text/javascript">
        // path = {!! json_encode(url('/')) !!} + '/';
      $('#contact_form').submit(function(e){
      e.preventDefault();
       $.ajax({
         url:path+'contact',
         type:'post',
         data:new FormData(this),
         contentType:false,
         processData:false,
         success:function(data,status)
         {


             swal({
                     title: 'Success!',
                     text: "Thank You!Your request has been submited successfully.",
                     type: 'success',
                     showCancelButton: false,
                     confirmButtonColor: '#3085d6',
                     confirmButtonText: 'Ok'
                 }).then(function (result) {
                     if (result.value) {
                       location.reload();
                     }
                 });
         }
       });
      });
   </script>
</html>
