<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>TNRWA - News</title>
	@include('include.front.head')
</head>
<body>
	@include('include.front.header')
	<section class="hero-banner hero-banner-sm">
		<div class="container text-center">
	      	<h2>News</h2>
	      	<nav aria-label="breadcrumb" class="banner-breadcrumb hide">
		        <ol class="breadcrumb">
		          	<li class="breadcrumb-item"><a href="#">Home</a></li>
		          	<li class="breadcrumb-item active" aria-current="page">Gallery</li>
		        </ol>
		    	</nav>
					<p style="font-size: 16px;" class="breadcrumbs_desc">Usually when we hear or read something new, we just compare it to our own ideas. If it is the same, we accept it and say that it is correct. If it is not, we say it is incorrect. In either case, we learn nothing.</p>
		</div>
	</section>
	<section class="section-padding--large gallery-area">
		<div class="container">

				<div class="row no-gutters">

                @foreach($news as $Key => $news_each)
					<div style="padding: 5px;" class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<a href="{{ asset('news_details/'.@$news_each->id) }}">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 card_background card-speaker no-padding">
								<img src="{{ asset('assets/images/news/'.@$news_each->display_image) }}" />
								<div class="speaker-footer">
					           <h4>{{@$news_each->title}}</h4>
										 <p>{{@$news_each->sub_title}}</p>
					      </div>
							</div>
						</a>
					</div>
                 @endforeach
				
				
				</div>

			</div>
		</div>
	</section>
	@include('include.front.footer')
</body>
</html>
