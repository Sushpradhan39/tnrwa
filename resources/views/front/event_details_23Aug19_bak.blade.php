<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>TNRWA - Events</title>
	@include('include.front.head')
</head>
<body>
	@include('include.front.header')
	<section class="hero-banner hero-banner-sm">
		<div class="container text-center">
	      	<h2>{{@$event->title}}</h2>
					<p>{{@$event->sub_title}}</p>
	      	<nav aria-label="breadcrumb" class="banner-breadcrumb">
		        <ol class="breadcrumb hide">
		          	<li class="breadcrumb-item"><a href="#">Home</a></li>
		          	<li class="breadcrumb-item"><a href="">Events</a></li>
		          	<li class="breadcrumb-item active" aria-current="page">Gallery</li>
		        </ol>
		    </nav>
				<p style="font-size: 17px;">Events play very important part in everyone's life. These events come and go but the learning and the knowledge remains with us forever</p>
		</div>
	</section>
	<section class="section-padding--large gallery-area">
		<div class="container">

			<div style="font-size: 20px;margin-bottom: 35px;" class="row">
				<div class="innovative-wrapper">
		            <p>{{@$event->description}}</p>
		        </div>
			</div>

				<div class="row no-gutters" style="margin-bottom: 20px;">
					<div style="padding-bottom: 50px;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center pb-98px">
						<h2 class="primary-text">Event Videos</h2>
						<img src="{{ asset('assets/front/img/home/section-style.png') }}" />
					</div>

					<div style="padding: 5px;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<?php $vedio_links = explode(',', $event->vedio_links) ?>
						@foreach($vedio_links as $key => $link)
						<div style="float: left" class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 card_background card-speaker no-padding">
									<iframe style="border: none;" width="100%" height="250" src="{{@$link}}"
	        					allowfullscreen="allowfullscreen"
	        					mozallowfullscreen="mozallowfullscreen"
	        					msallowfullscreen="msallowfullscreen"
	        					oallowfullscreen="oallowfullscreen"
	        					webkitallowfullscreen="webkitallowfullscreen"
									></iframe>
								</div>
							</div>
							@endforeach

					</div>
				</div>

			<div class="row no-gutters">

				<div style="padding-bottom: 50px;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center pb-98px">
					<h2 class="primary-text">Event Images</h2>
					<img src="{{ asset('assets/front/img/home/section-style.png') }}" />
				</div>

          @foreach($event->images as $key => $image)
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<a href="#">
								<div class="single-imgs relative">
									<img class="card-img rounded-0" src="{{ asset('assets/images/events/'.$image->image_name) }}" />
									<div class="overlay">
			                <div class="overlay-content">
			                  	<div class="overlay-icon">
			                    	<i class="ti-plus"></i>
			                  	</div>
			                </div>
			            </div>
								</div>
							</a>
						</div>
          @endforeach
					
			</div>
		</div>
	</section>
	@include('include.front.footer')
</body>
</html>
