  <header class="header_area">
    <div class="main_menu">
      <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container box_1620">
          <a class="navbar-brand logo_h" href="index.html"><img src="{{ asset('assets/images/logo-top.png') }}" alt=""></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
            <ul class="nav navbar-nav menu_nav justify-content-end">
              <li class="nav-item"><a class="nav-link" href="{{ asset('/') }}">Home</a></li>
              <li class="nav-item"><a class="nav-link" href="{{ asset('about_us') }}">About Us</a></li>
              <li class="nav-item"><a class="nav-link" href="{{ asset('news') }}">News</a></li>
              <li class="nav-item"><a class="nav-link" href="{{ asset('events') }}">Events</a></li>
              <li class="nav-item"><a class="nav-link" href="{{ asset('real-estate/all/all') }}">Real Estate</a></li>
              <!-- <li class="nav-item hide"><a class="nav-link" href="#.">Gallery</a></li> -->
              <li class="nav-item"><a class="nav-link" href="{{ asset('contact_us') }}">Contact</a></li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  </header>
  <script type="text/javascript">
    path = {!! json_encode(url('/')) !!} + '/';
  </script>
