   <link rel="icon" href="img/Fevicon.png" type="image/png">
  <link rel="stylesheet" href="{{ asset('assets/front/vendors/bootstrap/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/front/vendors/fontawesome/css/all.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/front/vendors/themify-icons/themify-icons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/front/vendors/linericon/style.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/front/vendors/owl-carousel/owl.theme.default.min.css')}}">
  <link rel="stylesheet" href="{{ asset('assets/front/vendors/owl-carousel/owl.carousel.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/front/css/magnific-popup.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/front/vendors/flat-icon/font/flaticon.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/front/css/style.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/css/sweetalert.css') }}" />