<footer class="footer-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4">
          <div class="single-footer-widget">
            <h6>About TNRWA</h6>
            <p>
              The world has become so fast paced that people don’t want to stand by reading a page of information to be  they would much rather look at a presentation and understand the message. It has come to a point where images and videos are used more to
            </p>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
          <div class="single-footer-widget">
            <h6>Navigation Links</h6>
            <div class="row">
              <div class="col">
                <ul>
                  <li><a href="{{ asset('/') }}">Home</a></li>
                  <li><a href="{{ asset('about_us') }}">About Us</a></li>
                  <li><a href="{{ asset('events') }}">Events</a></li>
                </ul>
              </div>
              <div class="col">
                <ul>
                  <li><a href="#">News</a></li>
                  <li class="hide"><a href="#">Gallery</a></li>
                  <li><a href="{{ asset('contact_us') }}">Contact</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
          <div class="single-footer-widget">
            <h6>Our Location</h6>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 location_list">
                <ul>
                    <li class="head">Tilak Nagar Residents Welfare Association</li>
                    <li>Registration no.: GBBSD-1101/15</li>
                    <li>Build.No.21/738 A-Wing,</li>
                    <li>Tilak Nagar Chembur,</li>
                </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="container">
        <div class="row align-items-center">
          <p class="col-lg-6 col-sm-12 footer-text m-0 text-center text-lg-left">
			         Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Developed By <a href="http://www.tecnovos.in/demo" target="_blank">Tecnovos Infotech</a>
          </p>
          <p class="col-lg-2 col-sm-12 footer-text m-0 text-center text-lg-left">
              <a rel="nofollow noopener" target="_blank" title="smallseotools.com/visitor-hit-counter/">
              <img src="https://smallseotools.com/counterDisplay?code=03735b39c13976c8e7534f8dde07ff46&style=0015&pad=9&type=page&initCount=0" alt="smallseotools.com/visitor-hit-counter/" border="0">
              </a>

          </p>
          <div class="col-lg-4 col-sm-12 footer-social text-center text-lg-right">
            <a href="#"><i class="fab fa-facebook-f"></i></a>
            <a href="#"><i class="fab fa-twitter"></i></a>
          </div>
        </div>
      </div>
    </div>
  </footer>

  <script src="{{ asset('assets/front/vendors/jquery/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('assets/front/vendors/bootstrap/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('assets/front/vendors/owl-carousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('assets/front/vendors/Magnific-Popup/jquery.magnific-popup.min.js') }}"></script>
  <script src="{{ asset('assets/front/js/jquery.ajaxchimp.min.js') }}"></script>
  <script src="{{ asset('assets/front/js/mail-script.js') }}"></script>
  <script src="{{ asset('assets/front/js/countdown.js') }}"></script>
  <script src="{{ asset('assets/front/js/jquery.magnific-popup.min.js') }}"></script>
  <script src="{{ asset('assets/front/js/main.js') }}"></script>
  <script src="{{ asset('assets/js/sweetalert.js') }}"></script>