<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Tecnovos Infotech</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="{{ asset("assetsimg/favicon.png") }}" rel="icon">
  <link href="{{ asset("assets/img/apple-touch-icon.png") }}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,600,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="{{ asset("assets/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="{{ asset("assets/font-awesome/css/font-awesome.min.css") }}" rel="stylesheet">
  <link href="{{ asset("assets/animate/animate.min.css") }}" rel="stylesheet">
  <link href="{{ asset("assets/ionicons/css/ionicons.min.css") }}" rel="stylesheet">
  <link href="{{ asset("assets/owlcarousel/assets/owl.carousel.min.css") }}" rel="stylesheet">
  <link href="{{ asset("assets/lightbox/css/lightbox.min.css") }}" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="{{ asset("assets/css/style.css") }}" rel="stylesheet">

  <!-- =======================================================
    Theme Name: Rapid
    Theme URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>
  <!--==========================
  Header
  ============================-->
  <header id="header">

    <div id="topbar">
      <div class="container">
        <div class="social-links">
          <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
          <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
          <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
          <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
        </div>
      </div>
    </div>

    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <h1 class="text-light"><a href="#intro" class="scrollto"><span>Tecnovos</span></a></h1> -->
        <h1 class="text-light"><a href="#intro" class="scrollto"><img src="{{ asset("assets/img/tecnovos_demo.jpeg") }}" /></a></h1>
        <!-- <a href="#header" class="scrollto"><img src="img/logo.png" alt="" class="img-fluid"></a> -->
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li class="active"><a href="#intro">Home</a></li>
          <li><a href="#about">About Us</a></li>
          <li><a href="#services">Services</a></li>
          <li><a href="#portfolio">Portfolio</a></li>
          <li style="display: none" class="drop-down"><a href="">Drop Down</a>
            <ul>
              <li><a href="#">Drop Down 1</a></li>
              <li class="drop-down"><a href="#">Drop Down 2</a>
                <ul>
                  <li><a href="#">Deep Drop Down 1</a></li>
                  <li><a href="#">Deep Drop Down 2</a></li>
                  <li><a href="#">Deep Drop Down 3</a></li>
                  <li><a href="#">Deep Drop Down 4</a></li>
                  <li><a href="#">Deep Drop Down 5</a></li>
                </ul>
              </li>
              <li><a href="#">Drop Down 3</a></li>
              <li><a href="#">Drop Down 4</a></li>
              <li><a href="#">Drop Down 5</a></li>
            </ul>
          </li>
          <li><a href="#footer">Contact Us</a></li>
        </ul>
      </nav><!-- .main-nav -->

    </div>
  </header><!-- #header -->

  <!--==========================
    Intro Section
  ============================-->
  <section id="intro" class="clearfix">
    <div class="container d-flex h-100">
      <div class="row justify-content-center align-self-center">
        <div class="col-md-6 intro-info order-md-first order-last">
          <h2>Rapid Solutions<br>for Your <span>Business!</span></h2>
          <div>
            <a href="#about" class="btn-get-started scrollto">Get Started</a>
          </div>
        </div>

        <div class="col-md-6 intro-img order-md-last order-first">
          <img src="{{ asset("assets/img/intro-img.svg") }}" alt="" class="img-fluid">
        </div>
      </div>

    </div>
  </section><!-- #intro -->

  <main id="main">

    <!--==========================
      About Us Section
    ============================-->
    <section id="about">

      <div class="container">
        <div class="row">

          <div class="col-lg-5 col-md-6">
            <div class="about-img">
              <img src="{{ asset("assets/img/about-img.jpg") }}" alt="">
            </div>
          </div>

          <div class="col-lg-7 col-md-6">
            <div class="about-content">
              <h2>About Us</h2>
              <p>
                Tecnovas Infotech is only hub for integration of information, design, and technology. We are team of aspiring developers with experience in website design and web development of corporate websites, small business websites, personal websites and e-commerce shopping websites.
Nowadays personal web sites play a vital role in the marketing and campaigning for the organizations.
</p><p>
We have observed that many of the organizations does not have personal web sites. So we interact with them personally or through mails on same. They were inspired and came forward to start up for development of site for his/her organization. We have created web sites for a variety of different projects, providing professional web site design services to small and corporate businesses, shopping sites, individuals and organizations hence resulting in delivering best of our services and meeting the customer expectation.
</p><p>
For us your vision is our challenge.
              </p>
              <!-- <h3>No Road Is Long With A Good Company.</h3>
              <p>We are software development company that partners with its clients to simplify and strengthen and transform their digital world.</p>
              <p>Aut dolor id. Sint aliquam consequatur ex ex labore. Et quis qui dolor nulla dolores neque. Aspernatur consectetur omnis numquam quaerat. Sed fugiat nisi. Officiis veniam molestiae. Et vel ut quidem alias veritatis repudiandae ut fugit. Est ut eligendi aspernatur nulla voluptates veniam iusto vel quisquam. Fugit ut maxime incidunt accusantium totam repellendus eum error. Et repudiandae eum iste qui et ut ab alias.</p>
              <ul>
                <li><i class="ion-android-checkmark-circle"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat.</li>
                <li><i class="ion-android-checkmark-circle"></i> Duis aute irure dolor in reprehenderit in voluptate velit.</li>
                <li><i class="ion-android-checkmark-circle"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda mastiro dolore eu fugiat nulla pariatur.</li>
              </ul> -->
            </div>
          </div>
        </div>
      </div>

    </section><!-- #about -->


    <!--==========================
      Services Section
    ============================-->
    <section id="services" class="section-bg">
      <div class="container">

        <header class="section-header">
          <h3>Services</h3>
          <p>We provide IT services for Software Development, Mobile , Ecommerce.</p>
        </header>

        <div class="row">

          <div class="col-md-6 col-lg-4 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon" style="background: #fceef3;"><i class="ion-ios-analytics-outline" style="color: #ff689b;"></i></div>
              <h4 class="title"><a href="">UI/UX Design</a></h4>
              <p class="description">We have proficient designers who are specialist in designing websites that are responsive and can attract target customers.</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon" style="background: #eafde7;"><i class="ion-ios-speedometer-outline" style="color:#41cf2e;"></i></div>
              <h4 class="title"><a href="">Software Development</a></h4>
              <p class="description">We have developed and delivered customized software applications. Our expertise can be seen in the applications we have developed.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 wow bounceInUp" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon" style="background: #fff0da;"><i class="ion-ios-bookmarks-outline" style="color: #e98e06;"></i></div>
              <h4 class="title"><a href="">Digital Marketing</a></h4>
              <p class="description">We have a team of digital marketing experts who can improve your searches for more visibility compared to your competitors.</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon" style="background: #e6fdfc;"><i class="ion-ios-paper-outline" style="color: #3fcdc7;"></i></div>
              <h4 class="title"><a href="">Application Development</a></h4>
              <p class="description">We help clients realize the undeniable potential of this platform by building powerful  Android/Ios applications.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-4 wow bounceInUp" data-wow-delay="0.2s" data-wow-duration="1.4s">
            <div class="box">
              <div class="icon" style="background: #e1eeff;"><i class="ion-ios-world-outline" style="color: #2282ff;"></i></div>
              <h4 class="title"><a href="">Ecommerce</a></h4>
              <p class="description">Having a online retailer is must when you want to online buyers into loyal customers.</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #services -->

    <!--==========================
      Why Us Section
    ============================-->
    <section id="why-us" class="wow fadeIn">
      <div class="container-fluid">

        <header class="section-header">
          <h3>Why choose us?</h3>
          <!-- <p>Laudem latine persequeris id sed, ex fabulas delectus quo. No vel partiendo abhorreant vituperatoribus.</p> -->
        </header>

        <div class="row">

          <div class="col-lg-6">
            <div class="why-us-img">
              <img src="{{asset("assets/img/why-us.jpg")}}" alt="" class="img-fluid">
            </div>
          </div>

          <div class="col-lg-6">
            <div class="why-us-content">
             <!--  <p>Molestiae omnis numquam corrupti omnis itaque. Voluptatum quidem impedit. Odio dolorum exercitationem est error omnis repudiandae ad dolorum sit.</p>
              <p>
                Explicabo repellendus quia labore. Non optio quo ea ut ratione et quaerat. Porro facilis deleniti porro consequatur
                et temporibus. Labore est odio.

                Odio omnis saepe qui. Veniam eaque ipsum. Ea quia voluptatum quis explicabo sed nihil repellat..
              </p> -->

              <div class="features wow bounceInUp clearfix">
                <!-- <i class="fa fa-diamond" style="color: #f058dc;"></i> -->
                <h4> Weâ€™re Experienced </h4>
                <p>Our team is made up of qualified web designers and developers. When it comes to creating websites that work, we really know our stuff. Weâ€™ve helped Many of businesses to bring their ideas to life.</p>
              </div>

              <div class="features wow bounceInUp clearfix">
                <!-- <i class="fa fa-object-group" style="color: #ffb774;"></i> -->
                <h4>Weâ€™re Affordable</h4>
                <p>We believe professional doesnâ€™t have to mean expensive. Weâ€™re all about creating high quality websites that every business can afford, with simple pricing that lets you benefit from a professionally designed site and easy cash flow management. .</p>
              </div>

              <div class="features wow bounceInUp clearfix">
                <!-- <i class="fa fa-language" style="color: #589af1;"></i> -->
                <h4>We Listen</h4>
                <p>Your website is about your business, not ours. One of our consultant will meet with you to learn about your business, and the sort of website you need. Then weâ€™ll help you get everything ready for your new site to be created.</p>
              </div>
              <div class="features wow bounceInUp clearfix">
                <!-- <i class="fa fa-language" style="color: #589af1;"></i> -->
                <h4>We Make It Easy</h4>
                <p>Everything we do including our website editor.  No jargon. No hidden costs. Just brilliant websites that are easy to edit, look great on any device, rank well on Google, and are built for success.</p>
              </div>
              <div class="features wow bounceInUp clearfix">
                <!-- <i class="fa fa-language" style="color: #589af1;"></i> -->
                <h4>We Never Stop</h4>
                <p>A great website can only become greater. This is why our team of experts are always working on new developments and features. As a customer, youâ€™ll automatically benefit - so your website will never go out of date.</p>
              </div>
            </div>

          </div>

        </div>

      </div>

      <!-- <div class="container">
        <div class="row counters">

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">274</span>
            <p>Clients</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">421</span>
            <p>Projects</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">1,364</span>
            <p>Hours Of Support</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">18</span>
            <p>Hard Workers</p>
          </div>

        </div>

      </div> -->
    </section>

    <!--==========================
      Call To Action Section
    ============================-->
    <section id="call-to-action" class="wow fadeInUp">
      <div class="container">
        <div class="row">
          <div class="col-lg-9 text-center text-lg-left">
            <h3 class="cta-title">Call To Action</h3>
            <p class="cta-text"> Drive your bussiness forward faster with online services.</p>
          </div>
          <div class="col-lg-3 cta-btn-container text-center">
            <a class="cta-btn align-middle" href="#footer">Call To Action</a>
          </div>
        </div>

      </div>
    </section><!-- #call-to-action -->

    <!--==========================
      Features Section
    ============================-->
    <section id="features" style="display: none;">
      <div class="container">

        <div class="row feature-item">
          <div class="col-lg-6 wow fadeInUp">
            <img src="{{asset("assets/img/features-1.svg")}}" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 wow fadeInUp pt-5 pt-lg-0">
            <h4>Voluptatem dignissimos provident quasi corporis voluptates sit assumenda.</h4>
            <p>
              Ipsum in aspernatur ut possimus sint. Quia omnis est occaecati possimus ea. Quas molestiae perspiciatis occaecati qui rerum. Deleniti quod porro sed quisquam saepe. Numquam mollitia recusandae non ad at et a.
            </p>
            <p>
              Ad vitae recusandae odit possimus. Quaerat cum ipsum corrupti. Odit qui asperiores ea corporis deserunt veritatis quidem expedita perferendis. Qui rerum eligendi ex doloribus quia sit. Porro rerum eum eum.
            </p>
          </div>
        </div>

        <div class="row feature-item mt-5 pt-5" style="display: none;">
          <div class="col-lg-6 wow fadeInUp order-1 order-lg-2">
            <img src="{{asset("assets/img/features-2.svg")}}" class="img-fluid" alt="">
          </div>

          <div class="col-lg-6 wow fadeInUp pt-4 pt-lg-0 order-2 order-lg-1">
            <h4>Neque saepe temporibus repellat ea ipsum et. Id vel et quia tempora facere reprehenderit.</h4>
            <p>
             Delectus alias ut incidunt delectus nam placeat in consequatur. Sed cupiditate quia ea quis. Voluptas nemo qui aut distinctio. Cumque fugit earum est quam officiis numquam. Ducimus corporis autem at blanditiis beatae incidunt sunt.
            </p>
            <p>
              Voluptas saepe natus quidem blanditiis. Non sunt impedit voluptas mollitia beatae. Qui esse molestias. Laudantium libero nisi vitae debitis. Dolorem cupiditate est perferendis iusto.
            </p>
            <p>
              Eum quia in. Magni quas ipsum a. Quis ex voluptatem inventore sint quia modi. Numquam est aut fuga mollitia exercitationem nam accusantium provident quia.
            </p>
          </div>

        </div>

      </div>
    </section><!-- #about -->

    <!--==========================
      Portfolio Section
    ============================-->
    <section id="portfolio" class="section-bg">
      <div class="container">

        <header class="section-header">
          <h3 class="section-title">Our Portfolio</h3>
        </header>

       <!--  <div class="row">
          <div class="col-lg-12">
            <ul id="portfolio-flters">
              <li data-filter="*" class="filter-active">All</li>
              <li data-filter=".filter-app">App</li>
              <li data-filter=".filter-card">Card</li>
              <li data-filter=".filter-web">Web</li>
            </ul>
          </div>
        </div> -->

        <div class="row portfolio-container">

          <div class="col-lg-4 col-md-6 portfolio-item filter-app">
            <div class="portfolio-wrap">
              <img src="{{asset("assets/img/deltas-img-final.png")}}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="https://deltascart.com">Deltas</a></h4>
                <!-- <p>App</p> -->
                <div>
                  <!-- <a href="https://deltascart.com" data-lightbox="portfolio" data-title="App 1" class="link-preview" title="Preview"><i class="ion ion-eye"></i></a> -->
                  <a href="https://deltascart.com" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web" data-wow-delay="0.1s">
            <div class="portfolio-wrap">
              <img src="{{asset("assets/img/tnrwa-img-final.png")}}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="http://tecnovos.in/tnrwa/">Tnrwa.org</a></h4>
               <!--  <p>Web</p> -->
                <div>
                  <!-- <a href="http://tecnovos.in/tnrwa/" class="link-preview" data-lightbox="portfolio" data-title="Web 3" title="Preview"><i class="ion ion-eye"></i></a> -->
                  <a href="http://tecnovos.in/tnrwa/" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app" data-wow-delay="0.2s">
            <div class="portfolio-wrap">
              <img src="{{asset("assets/img/sanivani-img-final.png")}}" class="img-fluid" alt="">
              <div class="portfolio-info">
                <h4><a href="http://sanjivanielectronics.in/">Sanivani Electronincs</a></h4>
               <!--  <p>App</p> -->
                <div>
                 <!--  <a href="http://sanjivanielectronics.in/" class="link-preview" data-lightbox="portfolio" data-title="App 2" title="Preview"><i class="ion ion-eye"></i></a> -->
                  <a href="http://sanjivanielectronics.in/" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #portfolio -->

    <!--==========================
      Clients Section
    ============================-->
    <!-- <section id="testimonials">
      <div class="container">

        <header class="section-header">
          <h3>Testimonials</h3>
        </header>

        <div class="row justify-content-center">
          <div class="col-lg-8">

            <div class="owl-carousel testimonials-carousel wow fadeInUp">

              <div class="testimonial-item">
                <img src="{{ asset("assets/img/testimonial-1.jpg")}}" class="testimonial-img" alt="">
                <h3>Saul Goodman</h3>
                <h4>Ceo &amp; Founder</h4>
                <p>
                  Proin iaculis purus consequat sem cure digni ssim donec porttitora entum suscipit rhoncus. Accusantium quam, ultricies eget id, aliquam eget nibh et. Maecen aliquam, risus at semper.
                </p>
              </div>

              <div class="testimonial-item">
                <img src="{{asset("assets/img/testimonial-2.jpg")}}" class="testimonial-img" alt="">
                <h3>Sara Wilsson</h3>
                <h4>Designer</h4>
                <p>
                  Export tempor illum tamen malis malis eram quae irure esse labore quem cillum quid cillum eram malis quorum velit fore eram velit sunt aliqua noster fugiat irure amet legam anim culpa.
                </p>
              </div>

              <div class="testimonial-item">
                <img src="{{asset("assets/img/testimonial-3.jpg")}}" class="testimonial-img" alt="">
                <h3>Jena Karlis</h3>
                <h4>Store Owner</h4>
                <p>
                  Enim nisi quem export duis labore cillum quae magna enim sint quorum nulla quem veniam duis minim tempor labore quem eram duis noster aute amet eram fore quis sint minim.
                </p>
              </div>

              <div class="testimonial-item">
                <img src="{{asset("assets/img/testimonial-4.jpg")}}" class="testimonial-img" alt="">
                <h3>Matt Brandon</h3>
                <h4>Freelancer</h4>
                <p>
                  Fugiat enim eram quae cillum dolore dolor amet nulla culpa multos export minim fugiat minim velit minim dolor enim duis veniam ipsum anim magna sunt elit fore quem dolore labore illum veniam.
                </p>
              </div>

            </div>

          </div>
        </div>
      </div>
    </section> -->

    <!-- #testimonials -->

    <!--==========================
      Clients Section
    ============================-->
    <section id="clients" class="wow fadeInUp">
      <div class="container">

        <header class="section-header">
          <h3>Our Clients</h3>
        </header>

        <div class="owl-carousel clients-carousel">
          <img src="{{asset("assets/img/deltas.png")}}" alt="">
           <img src="{{asset("assets/img/logo-top.png")}}" alt="">

         <!-- <img src="{{asset("assets/img/client-3.png")}}" alt="">
          <img src="{{asset("assets/img/client-4.png")}}" alt="">
          <img src="{{asset("assets/img/client-5.png")}}" alt="">
          <img src="{{asset("assets/img/client-6.png")}}" alt="">
          <img src="{{asset("assets/img/client-7.png")}}" alt="">
          <img src="{{asset("assets/img/client-8.png")}}" alt=""> -->
        </div>

      </div>
    </section><!-- #clients -->

  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer" class="section-bg">
    <div class="footer-top">
      <div class="container">

        <div class="row">

          <div class="col-lg-6">

            <div class="row">

                <div class="col-sm-6">

                  <div class="footer-info">
                    <h3>Tecnovos</h3>
                    <p>We are software development company that partners with its clients to simplify and strengthen and transform their digital world.</p>
                  </div>

                  <div class="footer-links">
                    <h4>Contact Us</h4>
                    <p>
                      Moraj Manor, Sect - 14, <br>
                      Sanpada, Palm Beach Road,<br>
                      Navi Mumbai, Maharshtra. <br>
                      <strong>Phone:</strong> +91 7977066345,9029965339<br>
                      <strong>Email:</strong> info@tecnovos.in<br>
                    </p>
                  </div>

                  <div class="footer-newsletter" style="display: none;">
                    <h4>Our Newsletter</h4>
                    <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna veniam enim veniam illum dolore legam minim quorum culpa amet magna export quem.</p>
                    <form action="" method="post">
                      <input type="email" name="email"><input type="submit"  value="Subscribe">
                    </form>
                  </div>

                </div>

                <div class="col-sm-6">
                  <div class="footer-links">
                    <h4>Useful Links</h4>
                    <ul>
                      <li><a href="#">Home</a></li>
                      <li><a href="#">About us</a></li>
                      <li><a href="#">Services</a></li>
                      <li><a href="#">Portfolio</a></li>
                      <li><a href="#">Contact Us</a></li>
                    </ul>

                  </div>

                  <div class="social-links" style="display: none;">
                    <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                    <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                    <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                  </div>

                </div>

            </div>

          </div>

          <div class="col-lg-6">

            <div class="form">

              <h4>Send us a message</h4>
              <p>Please fill out the quick form and we will be in touch with lightning speed.</p>
              <form id="contact_form">
                     <input type='hidden' name='_token' value='{{csrf_token()}}'/>
                <div class="form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validation"></div>
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" name="email_id" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                  <div class="validation"></div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="contact_no" id="contact_no" placeholder="Contact No" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                  <div class="validation"></div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                  <div class="validation"></div>
                </div>
                <div class="form-group">
                  <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                  <div class="validation"></div>
                </div>

               <!--  <div id="sendmessage">Your message has been sent. Thank you!</div>
                <div id="errormessage"></div> -->

                <div class="text-center"><button type="submit" title="Send Message">Send Message</button></div>
              </form>
            </div>

          </div>
        </div>

      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Tecnovos Infotech</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
        -->
        Designed by <a href="https://bootstrapmade.com/">Tecnovos Infotech</a>
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script src="{{ asset("assets/jquery/jquery.min.js") }}"></script>
  <script src="{{ asset("assets/jquery/jquery-migrate.min.js") }}"></script>
  <script src="{{ asset("assets/bootstrap/js/bootstrap.bundle.min.js") }}"></script>
  <script src="{{ asset("assets/easing/easing.min.js") }}"></script>
  <script src="{{ asset("assets/mobile-nav/mobile-nav.js") }}"></script>
  <script src="{{ asset("assets/wow/wow.min.js") }}"></script>
  <script src="{{ asset("assets/waypoints/waypoints.min.js")}}"></script>
  <script src="{{ asset("assets/counterup/counterup.min.js") }}"></script>
  <script src="{{ asset("assets/owlcarousel/owl.carousel.min.js") }}"></script>
  <script src="{{ asset("assets/isotope/isotope.pkgd.min.js") }}"></script>
  <script src="{{ asset("assets/lightbox/js/lightbox.min.js") }}"></script>
  <!-- Contact Form JavaScript File -->
 <!--  <script src="{{ asset("assets/contactform/contactform.js") }}"></script> -->

  <!-- Template Main Javascript File -->
  <script src="{{ asset("assets/js/main.js") }}"></script>

</body>
 <script type="text/javascript">
        path = {!! json_encode(url('/')) !!}+'/';
 </script>
 <script type="text/javascript">
      $('#contact_form').submit(function(e){
      e.preventDefault();
       $.ajax({
         url:path+'contact',
         type:'post',
         data:new FormData(this),
         contentType:false,
         processData:false,
         success:function(data,status)
         {
          alert(data);
             //  $(document).ready(function(){
             // swal({
             //         title: 'Success!',
             //         text: "Thank You!Your request has been submited successfully.",
             //         type: 'success',
             //         showCancelButton: false,
             //         confirmButtonColor: '#3085d6',
             //         confirmButtonText: 'Ok'
             //     }).then(function (result) {
             //         if (result.value) {
             //           location.reload();
             //         }
             //     });
             //       });
         }
       });
      });
   </script>
</html>