<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@home');
Route::get('/home', 'HomeController@home');
Route::get('/events', 'HomeController@events');
Route::get('/event_details/{id?}', 'HomeController@event_details');

Route::get('news', 'HomeController@news');
Route::get('/news_details/{id?}', 'HomeController@news_details');

Route::get('/about_us', 'HomeController@about_us');
// Route::get('/event_details', 'HomeController@event_details');

Route::get('/contact_us', 'HomeController@contact_us');
Route::post('contact','HomeController@contactUsForm');
Route::post('member-register','HomeController@memberRegister');
Route::post('service-register','HomeController@serviceRegister');
// real estate form creation
Route::get('/real-estate/create', 'HomeController@realEstateCreate');
Route::post('/real-estate/create', 'HomeController@realEstateAdd');

Route::get('/real-estate/{type?}/{flat_specification?}', 'HomeController@realEstateList');
Route::get('get-real-estate-details','HomeController@getRealEstateDetails');






Route::get('admin','AdminController@login');
Route::get('admin/login','AdminController@login');
Route::post('admin/auth','AdminController@auth');
Route::any('admin/logout','AdminController@logout');

Route::get('admin/reset-password','AdminController@resetPassword');
Route::post('admin/changePassword','AdminController@changePassword');


Route::get('/admin/add-event', 'AdminController@addEvent');
Route::post('admin/add_event','AdminController@insertEvent');
Route::get('/admin/view-event', 'AdminController@viewEvent');
Route::delete('admin/delete_event','AdminController@deleteEvent');
Route::get('/admin/edit-event/{id?}', 'AdminController@editEvent');
Route::post('admin/update-event','AdminController@updateEvent');

Route::delete('admin/delete_event_image','AdminController@deleteEventImage');
Route::post('admin/add_other_event_images','AdminController@insertOtherEventImages');


Route::get('admin/create_news', 'AdminController@addNews');
Route::post('admin/add_news','AdminController@insertNews');
Route::get('admin/view_news', 'AdminController@viewNews');
Route::delete('admin/delete_news','AdminController@deleteNews');
Route::get('/admin/edit-news/{id?}', 'AdminController@editNews');
Route::post('admin/update-news','AdminController@updateNews');

Route::delete('admin/delete_news_image','AdminController@deleteNewsImage');
Route::post('admin/add_other_news_images','AdminController@insertOtherNewsImages');


Route::get('admin/real-estate/{status?}','AdminController@getRealEstates');
Route::post('admin/update-real-estate-status','AdminController@updateRealEstateStatus');
