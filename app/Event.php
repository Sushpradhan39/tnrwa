<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Event extends Model
{
    public function insertEvent($data){
          DB::beginTransaction();
     try {
          


          $event_data = array(
         	'title'=>@$data['title'],
         	'sub_title'=>@$data['sub_title'],
         	'description'=>@$data['description'],
          'vedio_links'=>@$data['vedio_links'],
          'drive_link'=>@$data['drive_link']
         	);

         if(@$data['display_image']){
           $dfile = $data['display_image']; 
           $fileName = rand(0,999).date('ymdHis'); 
           $ext = $dfile->getClientOriginalExtension();
           $destinationPath = base_path('assets/images/events');
           $fileSucc = $dfile->move($destinationPath,$fileName.'.'.$ext); 
            if(@$fileSucc){
              $event_data['display_image'] = $fileName.'.'.$ext;
            }	
         }
            
       $event_id = DB::table('events')->insertGetId($event_data);
       // if(@$data['other_images']){
       //       $files = $data['other_images'];   
       //  $fileInfo = array();

       //  for ($i=0;$i<count($files);$i++) 
       //  {
       //    if(@$files[$i])
       //    {
       //     $file = $files[$i];
       //     $fileName = rand(0,999).date('ymdHis'); 
       //     $ext = $file->getClientOriginalExtension();
       //     $destinationPath = base_path('assets/images/events');
       //     $fileSucc = $file->move($destinationPath,$fileName.'.'.$ext); 
       //     if(@$fileSucc)
       //     {
       //      $fileInfo[$i]['event_id'] = $event_id;
       //      $fileInfo[$i]['image_name'] = $fileName.'.'.$ext;
       //     }
       //    }
       //  }
       //      DB::table('event_images')->insert($fileInfo);
       // }
        
            DB::commit();
          return 1;
        } catch (\Exception $e) {
            DB::rollback();
            return 2;
     }
    }

    public function updateEvent($data){
          DB::beginTransaction();
     try {
          


          $event_data = array(
          'title'=>@$data['title'],
          'sub_title'=>@$data['sub_title'],
         'vedio_links'=>@$data['vedio_links'],
          'drive_link'=>@$data['drive_link']
          );

         if(@$data['display_image']){
           $dfile = $data['display_image']; 
           $fileName = rand(0,999).date('ymdHis'); 
           $ext = $dfile->getClientOriginalExtension();
           $destinationPath = base_path('assets/images/events');
           $fileSucc = $dfile->move($destinationPath,$fileName.'.'.$ext); 
            if(@$fileSucc){
              $event_data['display_image'] = $fileName.'.'.$ext;
            } 
         }
            
       $event_id = DB::table('events')->where(array('id'=>$data['event_id']))->update($event_data);
      
            DB::commit();
          return 1;
        } catch (\Exception $e) {
            DB::rollback();
            return 2;
     }
    }

    public function insertOtherEventImages($data){

      $event_id = $data['event_id'];
      $files = $data['other_images'];   
        $fileInfo = array();

        for ($i=0;$i<count($files);$i++) 
        {
          if(@$files[$i])
          {
           $file = $files[$i];
           $fileName = rand(0,999).date('ymdHis'); 
           $ext = $file->getClientOriginalExtension();
           $destinationPath = base_path('assets/images/events');
           $fileSucc = $file->move($destinationPath,$fileName.'.'.$ext); 
           if(@$fileSucc)
           {
            $fileInfo[$i]['event_id'] = $event_id;
            $fileInfo[$i]['image_name'] = $fileName.'.'.$ext;
           }
          }
        }
            DB::table('event_images')->insert($fileInfo);
    }

    public function getEvents(){
      return DB::table('events')->where(array('status'=>0))->orderBy('id','desc')->get();
    }
    public function getEventDetails($id){
      $event = DB::table('events')->where(array('status'=>0,'id'=>$id))->first();
     // $event->images = DB::table('event_images')->where(array('status'=>0,'event_id'=>$id))->orderBy('id','desc')->get();
      return $event;

    }
    public function deleteEvent($event_id){
          DB::table('events')->where(array('id'=>$event_id))->update(array('status'=>1));
      return 1;
    }
    public function deleteEventImage($image_id){
          DB::table('event_images')->where(array('id'=>$image_id))->update(array('status'=>1));
      return 1;
    }


}
