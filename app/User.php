<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use Illuminate\Support\Facades\Hash;
use DB;
use Session;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function authModel($mobile_number,$password)
    {
           if (Auth::attempt(['mobile_number' => $mobile_number, 'password' => $password,'user_type'=>0,'status'=>0])) 
     {
          return 1;
    }else
        {
            return 2;
        }
    }
    public function changePasswordAdmin($data)
  {
          if (Auth::attempt(['user_type'=>0,'password' => $data['old_password']])) 
     {
          DB::table('users')->where(array('user_type'=>0))->update(array('password'=>Hash::make($data['confirm_password'])));
          return 1;
     }else
     {
      return 2;
     }
  }

}
