<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class RealEstate extends Model
{
    public function addRealEstate($data){
    	
    	  DB::beginTransaction();
     try {

    	$available_from_to = explode('-', @$data['available_from_to']);
		$data['available_from'] = date('Y-m-d',strtotime(@$available_from_to[0]));
		$data['available_to'] = date('Y-m-d',strtotime(@$available_from_to[1]));
		unset($data['available_from_to']);

		$files = @$data['files'];
    	unset($data['files']);


    	$id = DB::table('real_estates')->insertGetId($data);

    	if($files){
    		   for ($i=0;$i<count($files);$i++) 
    		        {
    		          if(@$files[$i])
    		          {
    		           $file = $files[$i];
    		           $fileName = rand(0,999).date('ymdHis'); 
    		           $ext = $file->getClientOriginalExtension();
    		           $destinationPath = base_path('assets/images/real-estate');
    		           $fileSucc = $file->move($destinationPath,$fileName.'.'.$ext); 
    		           if(@$fileSucc)
    		           {
    		            $fileInfo[$i]['real_estate_id'] = $id;
    		            $fileInfo[$i]['image_name'] = $fileName.'.'.$ext;
    		           }
    		          }
    		        }
    		        DB::table('real_estate_images')->insert($fileInfo);
    	}

    	   DB::commit();
          return 1;
        } catch (\Exception $e) {
            DB::rollback();
           return $e->getMessage();
     }
   }
   public function getRealEstates($status,$type = '',$flat_specification=''){
    $where_data = [];
    $where_data['status'] = $status;
    if($type != 'all' & $type != ''){
      $where_data['type'] = $type;
    }
    if($flat_specification != 'all' & $flat_specification != ''){
      $where_data['flat_specification'] = $flat_specification;
    }
    
    $real_estates = DB::table('real_estates')->select("*")->where($where_data)->orderBy('updated_at','DESC')->get();

    foreach ($real_estates as $key => $real_estate) {
      $images = DB::table('real_estate_images')->select('*')->where(array('real_estate_id'=>@$real_estate->id,'status'=>0))->get();
      $real_estates[$key]->images = $images;
    }

      return $real_estates;
   }


   public function updateRealEstate($udata,$real_estate_id){
    DB::table("real_estates")->where('id',$real_estate_id)->update($udata);
    return 1;
   }
   public function getRealEstateDetails($real_estate_id){
    $data = DB::table('real_estates')->select('*')->where(array('id'=>$real_estate_id))->first();
    $images = DB::table('real_estate_images')->select('*')->where(array('real_estate_id'=>@$real_estate_id,'status'=>0))->get();
    $data->images = @$images;
    return $data;   
   }
}
