<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class News extends Model
{
    public function insertNews($data){
          DB::beginTransaction();
     try {
          


          $event_data = array(
         	'title'=>@$data['title'],
         	'sub_title'=>@$data['sub_title'],
         	'description'=>@$data['description'],
          'vedio_links'=>@$data['vedio_links'],
          'drive_link'=>@$data['drive_link']
         	);

         if(@$data['display_image']){
           $dfile = $data['display_image']; 
           $fileName = rand(0,999).date('ymdHis'); 
           $ext = $dfile->getClientOriginalExtension();
           $destinationPath = base_path('assets/images/news');
           $fileSucc = $dfile->move($destinationPath,$fileName.'.'.$ext); 
            if(@$fileSucc){
              $event_data['display_image'] = $fileName.'.'.$ext;
            }	
         }
            
       $news_id = DB::table('news')->insertGetId($event_data);
        
     // if(@$data['other_images']){
     //    $files = $data['other_images'];   
     //    $fileInfo = array();

     //    for ($i=0;$i<count($files);$i++) 
     //    {
     //      if(@$files[$i])
     //      {
     //       $file = $files[$i];
     //       $fileName = rand(0,999).date('ymdHis'); 
     //       $ext = $file->getClientOriginalExtension();
     //       $destinationPath = base_path('assets/images/news');
     //       $fileSucc = $file->move($destinationPath,$fileName.'.'.$ext); 
     //       if(@$fileSucc)
     //       {
     //        $fileInfo[$i]['news_id'] = $news_id;
     //        $fileInfo[$i]['image_name'] = $fileName.'.'.$ext;
     //       }
     //      }
     //    }
     // DB::table('news_images')->insert($fileInfo);
     //  }

            DB::commit();
          return 1;
        } catch (\Exception $e) {
            DB::rollback();
            return 2;
     }
    }


     public function updateNews($data){
          DB::beginTransaction();
     try {
          


          $news_data = array(
          'title'=>@$data['title'],
          'sub_title'=>@$data['sub_title'],
          'description'=>@$data['description'],
          'vedio_links'=>@$data['vedio_links'],
          'drive_link'=>@$data['drive_link']
          );

         if(@$data['display_image']){
           $dfile = $data['display_image']; 
           $fileName = rand(0,999).date('ymdHis'); 
           $ext = $dfile->getClientOriginalExtension();
           $destinationPath = base_path('assets/images/news');
           $fileSucc = $dfile->move($destinationPath,$fileName.'.'.$ext); 
            if(@$fileSucc){
              $news_data['display_image'] = $fileName.'.'.$ext;
            } 
         }
          
       $news_id = DB::table('news')->where(array('id'=>$data['news_id']))->update($news_data);
      
            DB::commit();
          return 1;
        } catch (\Exception $e) {
            DB::rollback();
            return 2;
     }
    }


     public function insertOtherNewsImages($data){

      $news_id = $data['news_id'];
      $files = $data['other_images'];   
        $fileInfo = array();

        for ($i=0;$i<count($files);$i++) 
        {
          if(@$files[$i])
          {
           $file = $files[$i];
           $fileName = rand(0,999).date('ymdHis'); 
           $ext = $file->getClientOriginalExtension();
           $destinationPath = base_path('assets/images/news');
           $fileSucc = $file->move($destinationPath,$fileName.'.'.$ext); 
           if(@$fileSucc)
           {
            $fileInfo[$i]['news_id'] = $news_id;
            $fileInfo[$i]['image_name'] = $fileName.'.'.$ext;
           }
          }
        }
            DB::table('news_images')->insert($fileInfo);
    }

    public function getNews(){
      return DB::table('news')->where(array('status'=>0))->orderBy('id','desc')->get();
    }
    public function getNewsDetails($id){
      $news = DB::table('news')->where(array('status'=>0,'id'=>$id))->first();
      // $news->images = @DB::table('news_images')->where(array('status'=>0,'news_id'=>$id))->orderBy('id','desc')->get();
      return $news;

    }
    
    public function deleteNews($news_id){
   DB::table('news')->where(array('id'=>$news_id))->update(array('status'=>1));
      return 1;
    }
     public function deleteNewsImage($image_id){
          DB::table('news_images')->where(array('id'=>$image_id))->update(array('status'=>1));
      return 1;
    }
}
