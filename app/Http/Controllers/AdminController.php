<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Event;
use App\News;
use Auth;
use App\RealEstate;

class AdminController extends Controller
{
	  function __construct()
	  {
	    $user = new User();
	    $this->user = $user;
	    $event = new Event();
	    $this->event = $event;
	    $news = new News();
	    $this->news = $news;
      $realEstate = new RealEstate();
      $this->realEstate = $realEstate;
	  }

	  public function login()
        {
            return view('admin/login');
        }

      public function auth(Request $request)
        {
             $this->validate($request, [
                'mobile_number' => 'required',
        		'password' => 'required',
              ]);

	         $mobile_number = $request->input('mobile_number');
	         $password = $request->input('password');
	         return $this->user->authModel($mobile_number,$password);
         }

       public function logout()
        {
          Auth::logout();
          return redirect('admin');
        }
        public function resetPassword(){
        	 return view('admin/reset-password');
        }
        public function changePassword(Request $request)
        {
           $this->validate($request, [
          'old_password' => 'required',
          'new_password' => 'required',
          'confirm_password' => 'required|same:new_password'
        ]);      
           return $this->user->changePasswordAdmin($request->all());
        }




	    public function addEvent(){
	        return view('admin/add_event');
	    }
	    public function insertEvent(Request $request){
	    	 $this->validate($request, [
          'title' => 'required',
          'description' => 'required',
          'display_image' => 'required'
         ]);
             return $this->event->insertEvent($request->all());
       }
       public function updateEvent(Request $request){
       	  $this->validate($request, [
          'title' => 'required',
          'description' => 'required'
         ]);
       	return $this->event->updateEvent($request->all());  
       }
       public function insertOtherEventImages(Request $request)
       {
       	 $this->validate($request, [
          'title' => 'other_images',
         ]);
             return $this->event->insertOtherEventImages($request->all());
       }
	    public function viewEvent(){
	    	$events = $this->event->getEvents();
	    	return view('admin/view_event')->with('events',$events);
	    }
	    public function deleteEvent(Request $request){
	    	 $event_id = $request->input('event_id');
          return $this->event->deleteEvent($event_id);
	    }
	    public function deleteEventImage(Request $request){
	    	 $image_id = $request->input('image_id');
          return $this->event->deleteEventImage($image_id);
	    }
	    

	     public function addNews(){
	        return view('admin/add_news');
	    }
	    public function insertNews(Request $request){
	    	 $this->validate($request, [
          'title' => 'required',
          'description' => 'required',
          'display_image' => 'required'
         ]);
             return $this->news->insertNews($request->all());
       }
       public function updateNews(Request $request){
       	  $this->validate($request, [
          'title' => 'required',
          // 'sub_title' => 'required',
          'description' => 'required'
         ]);
       	return $this->news->updateNews($request->all());  
       }
	    public function viewNews(){
	    	$news = $this->news->getNews();
	        return view('admin/view_news')->with('news',$news);
	    }
	    public function deleteNews(Request $request){
	    	 $news_id = $request->input('news_id');
          return $this->news->deleteNews($news_id);
	    }
	    public function deleteNewsImage(Request $request){
	    	 $image_id = $request->input('image_id');
          return $this->news->deleteNewsImage($image_id);
	    }
	     public function insertOtherNewsImages(Request $request)
       {
       	 $this->validate($request, [
          'title' => 'other_images',
         ]);
             return $this->news->insertOtherNewsImages($request->all());
       }

			public function editEvent($id){
				 $event = $this->event->getEventDetails($id);
				 return view("admin/edit_event")->with('event',@$event);
			}
			public function editNews($id){
				 $news = $this->news->getNewsDetails($id);
				 return view("admin/edit_news")->with('news',@$news);
			}
      public function getRealEstates($status){

        $data = $this->realEstate->getRealEstates($status);
        return view('admin/real-estates')->with('data',$data)->with('status',$status);

      }
      public function updateRealEstateStatus(Request $request){
        $data = $request->all();
        $real_estate_id = $data['real_estate_id'];
        $udata = [];
        $udata['status'] = $data['status'];
        return $this->realEstate->updateRealEstate($udata,$real_estate_id);
      }

}
