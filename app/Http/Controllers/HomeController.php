<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\News;
use App\User;
use App\RealEstate;
use DB;
class HomeController extends Controller
{
    function __construct()
      {
        $user = new User();
        $this->user = $user;
        $event = new Event();
        $this->event = $event;
        $news = new News();
        $this->news = $news;
        $realEstate = new RealEstate();
        $this->realEstate = $realEstate;
      }

    public function home(){
        return view('front.home');
    }

    public function events(){
        $events = $this->event->getEvents();
    	return view('front.events')->with('events',$events);
    }

    public function event_details($id){
      $event = $this->event->getEventDetails($id);
        return view('front.event_details')->with('event',$event);
    }

    public function news(){
        $news = $this->news->getNews();
        return view('front.news')->with('news',$news);
    }
    public function news_details($id){
      $news = $this->news->getNewsDetails($id);
      return view('front.news_details')->with('news',$news);
    }


    public function about_us(){
    	return view('front.about');
    }

    public function contact_us(){
    	return view('front.contact');
    }

    public function contactUsForm(Request $request,\Illuminate\Mail\Mailer $mailer)
    {
     $message = 'Name :'.@$request->input('name').'<br/>
        Contact No : '.@$request->input('contact_no').',<br/>
        Email Id : '.@$request->input('email_id').',<br/>
          Subject : '.@$request->input('subject').',<br/>
        Message :'.@$request->input('message'); 
        $title[0] = 'New Enquiry form Details';
        $title[1] = $message;

       $mailer->to('sushant.pradhan5@gmail.com')->queue(new \App\Mail\TnrwaMailer($title));
    }

    public function memberRegister(Request $request,\Illuminate\Mail\Mailer $mailer)
    {
        $member = $request->all();
        unset($member['_token']);
        DB::table('members')->insert($member);

     $message = 'Name :'.@$request->input('name').'<br/>
        Contact No : '.@$request->input('contact_no').',<br/>
        Email Id : '.@$request->input('email_id').',<br/>
        Address :'.@$request->input('address'); 
        $title[0] = 'New Member Details';
        $title[1] = $message;

       $mailer->to('sushant.pradhan5@gmail.com')->queue(new \App\Mail\TnrwaMailer($title));
        return 1;
    }
    public function serviceRegister(Request $request,\Illuminate\Mail\Mailer $mailer)
    {
        $member = $request->all();
        unset($member['_token']);
        DB::table('services')->insert($member);

     $message = 'Name :'.@$request->input('name').'<br/>
        Contact No : '.@$request->input('contact_no').',<br/>
        Email Id : '.@$request->input('email_id').',<br/>
        Description :'.@$request->input('description'); 
        $title[0] = 'New Service Details';
        $title[1] = $message;

       $mailer->to('sushant.pradhan5@gmail.com')->queue(new \App\Mail\TnrwaMailer($title));
        return 1;
    }
    
    public function realEstateCreate(){
    	return view('front.realEstateCreate');
    }
    public function realEstateAdd(Request $request){
        $data = $request->all();
        unset($data['_token']);
       return  $this->realEstate->addRealEstate($data);
    }
    public function realEstateList($type = '',$flat_specification=''){
        
        $status = 'active';
        $data = $this->realEstate->getRealEstates($status,$type,$flat_specification);
        return view('front.realEstateList')->with('data',$data)->with('type',$type)->with('flat_specification',$flat_specification); 
    }
    public function getRealEstateDetails(Request $request){
        $real_estate_id = $request->input('real_estate_id');
        $data = (array)$this->realEstate->getRealEstateDetails($real_estate_id);
        return $data;
    }
 
}
