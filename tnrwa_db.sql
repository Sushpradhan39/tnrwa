-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 19, 2020 at 12:32 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tnrwa_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `sub_title` varchar(255) DEFAULT NULL,
  `description` varchar(5000) NOT NULL,
  `vedio_links` varchar(1000) DEFAULT NULL,
  `drive_link` varchar(255) DEFAULT NULL,
  `display_image` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `title`, `sub_title`, `description`, `vedio_links`, `drive_link`, `display_image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Safety measures presentation seminar along with Asst. Commissioner of Police', NULL, 'With the help of Tilak nagar police station and respective officers, a seminar was conducted to help and guide safety of tilak nagar residents. Safety guide line progrogram was arranged. Asst. Commissioner of Police alongwith his expert team gave guidelines and information of safety of citizens. Area Police officer have assured to help the residents.', NULL, NULL, '365190824135535.jpg', 0, '2019-08-24 13:55:35', '2019-08-24 13:55:35'),
(2, 'Plantation of Trees - WORLD ENVIRONMENT DAY', NULL, 'On 6th June 2015 at 10 a.m. we have planted trees on road going to Tilak nagar Railway Station. Residents as well as dignatories , Seniorcitizens, School Childrens have participated in this good cause. Our Slogan : \'PLANT TREES SAVE EARTH.\'', NULL, NULL, '871190824135619.jpg', 0, '2019-08-24 13:56:19', '2019-08-24 13:56:19'),
(3, 'Achievement: Successfully stopped 4G towers', NULL, 'Tilaknagar is a middle class residence locality and are busy with their day to day work. Cable laying was going  and we thought that it is for BEST supply cables. Not taken any objection while digging/laying time . After a while it has come to notice that Relience Co. is laying the cables for 4G towers and those towers will be erected in most of the playgrounds and garden in tilak nagar. Some experts said it is harmful to the health of not only childrens playing in the playground and gardens but it is also harmful to senior citizens going for jogging in the morning and in the evening. We have tried to stop the laying those cables and it was not easy to stop as it was the wrk from the Stronges/powerful firm called Relience co.. Thus the time came when we few core committee members formed registered Society and fought legaly and finally we got the work stopped immediately & ultimately.', NULL, NULL, '307190824135647.jpg', 0, '2019-08-24 13:56:47', '2019-08-24 13:56:47'),
(4, 'Successful Event: Tilak Nagar January - 2018', 'January - 2018', 'Tilak Nagar Residents Welfare Association Proudly presents \"Tilak Nagar FESTIVAL\". This festival wil host activities in brief : Sports - Culture - Music - Art - Talent & much more. This festival puts up an objective of bringing together all societies and joining hands to make our society clean green and ideal colony to livein, to blend in. The festival aims at getting more people involved in colony welfare issues and thus evolve it to a better place to live in. This event was held at Tilak Nagar Kridangan. Ground was filled with people and participants surrounded by various stalls on housing artifacts and other things to interest people. Event plans to spread JOY all around. Sports for children were organised by TNRWA. Highlight of the event is to form a human chain to stand for \"SAVE ATMOSPHERE NO PLASTIC\" and a Bicycle rally.', NULL, NULL, '918190824135742.jpeg', 0, '2019-08-24 13:57:42', '2019-08-24 13:57:42'),
(5, 'Successful Event: Housing Conference August - 2018', 'August - 2018', 'Housing Conference organised by TNRWA today overwhelming response from the residents, around 300 plus residents marked their presence early morning despite Sunday, at Sarvoday Buddha Vihar Hall. Hon\'ble MLA Mangeshji Kudalkar sir, Corporator Sushamji Sawant saheb, Chairperson of Mumbai District Federation Smt Chhyatai Ajagaonkar madam, Director of Housing Federation and Editor of Housing Times Anushri Tai Malgaonkar, Ex President of Mumbai Federation Adv Shri D S Vader, President of Auditors Association Mr Rahul Patilsaheb, Sr. Architect Mr Rajendra Karnik Sir guided the residents on various burning issues faced by the residents viz. OC, upgradation of Infrastructure, Parking issues, functioning Of Housing Societies And its day to day affairs, Audits n Accounts etc.', NULL, NULL, '141190824135820.jpeg', 0, '2019-08-24 13:58:20', '2019-08-24 13:58:20'),
(6, 'Successful Event: Cleanliness Drive September - 2018', 'August - 2018', 'Clean is what we do to anything that we would like to start new. Hence on 29th September 2018 Saturday at 7.00 am, we would like start with \"Cleanliness Drive at Our Own Tilak Nagar Station\". At the instance of Central Railway Cleanliness drive initiated by TNRWA with the assistance of residents of Tilak Nagar and Jyesth Nagrik Sangha and Samuha as well. As Cleanliness is next to Godliness we invoke Almighty\'s Abundant blessings with this Noble cause. There was great response more then 150 residents participated in this drive including school going n college going students. The motto of TNRWA: TILAK NAGAR WELFARE ASSOCIATION, is \"Clean,Green and Ideal Colony\" It is also dream of TNRWA to make Tilak Nagar Station as a Smart Station in Mumbai and Tilak Nagar as Smart Colony as well.', NULL, NULL, '208190824135851.jpg', 0, '2019-08-24 13:58:51', '2019-08-24 13:58:51'),
(7, 'Successful Event: Interactive Seminar on \"Self Redevelopment\"', 'August - 2018', 'We had a Super Sunday today where Mr. Chandershekar Prabhu helped us in delimting our beliefs on \'Self Redevelopment\'. From the word go he was spot on with regards to the building our beliefs with regards to scope of society redevelopment. Starting from perceptions that we have in terms of Self Redevelopment. *TNRWA* gave an opportunity to reinvent ourselfs with regards to our *Living Space.* Interactive seminar showcased the solutions to all the opportunities or perspectives that we have with regards to *Self-Development.* Judiciary, Regulatory, Compliance, Safety and Security of Finance, Infrastructure, Licienceing and permissions, all the netigreties beyond our resourcefulness. As the session progressed the beliefs of self development started becoming true as he touched all aspects of getting finance from banks through different schemes in place. How to source a great arthitect to create a great project layout plan How the risk does not rest with an individual or committee as all decisions to be taken at General Meets. How to get licensing and paperwork done by appointing Liason Architecture. How the project management can we handed over to the best companies in the world. Participants where enlighten with seamless knowledge that can be put into action along with the time periods for the same. In the question hour sessions, queries where asked with regards to collector\'s plot, stratergiseing sale of flats, minimum tenants majority required for loan approval, etc. The occasion was graced by almost 250+ residents of New/Tilak Nagar along with Our Honourable MLA Mr. Mahesh Kudalkar to *\"Demystify Self-development\"* It was *www.tnrwa.org* initiative.', NULL, '1l2tQA534z-m5KzD5JNWxalwwlyZuox2m', '374190824140402.jpg', 0, '2019-08-24 14:04:02', '2019-08-24 17:25:30'),
(8, 'Festival 2019', 'January - 2019', '**TNRWA* *FEST 2019** *Peaceful Mind & Healthy Body is the first step towards all success.* Endeavouring the cause *Tilak Nagar Residents Welfare Association (TNRWA)* has organized a Yog Shibir for the residents of Tilak Nagar to introduce \"Yog Sadhana\" for peaceful mind, good health & body. Celebrated Yoga Teachers, Mrs. Shubhangi Shah, Mrs Anjali Sinhasan, Mrs Ujwala Shinde and Dr C D Karve will guide the participants to experience the closeness of peace and tranquility of serene elements of nature. Come and experience the paradise to be on earth called TILAK NAGAR in this stressfull lifestyle of ours called BUSY LIFE. Date : 19th January 2019 Place : Ground opposite \'Khule Natya Graha\' Lokmanya Tilak Kridangan Tilak Nagar Time : morning 7 to 8:30 All the participants are requested to occupy seats by 6.45 am in the morning. Come and enjoy the serenity of nature and calmness of Body & Mind!!!\r\n\r\n19th & 20th January, 2019, Annual Sports and Cultural events for residents of Tilak Nagar, Bicycle Rally, Marathon, Dream run, Carraom, Badminton, Athletics, Dance,Drawings for kids in age groups, Tilak Nagar Talent Hunt,events for almost all residents, Foodies and much more,participate in large numbers and enjoy, hurry up registration counter will start soon at TNRWA hub opp bldg No. 19. ...\r\n\r\n....TNRWA', NULL, '1y7Pc73jZPevePy8CrVSrR8wXIJ0peDix', '511190824140447.jpeg', 0, '2019-08-24 14:04:47', '2019-08-24 17:58:13');

-- --------------------------------------------------------

--
-- Table structure for table `event_images`
--

CREATE TABLE `event_images` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event_images`
--

INSERT INTO `event_images` (`id`, `event_id`, `image_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, '213190810161033jpg', 0, '2019-08-10 16:10:33', '2019-08-10 16:10:33'),
(2, 2, '700190810161033jpg', 0, '2019-08-10 16:10:33', '2019-08-10 16:10:33'),
(3, 3, '45190810161627jpg', 0, '2019-08-10 16:16:27', '2019-08-10 16:16:27'),
(4, 3, '699190810161627jpg', 0, '2019-08-10 16:16:27', '2019-08-10 16:16:27'),
(5, 4, '548190810161716.jpg', 0, '2019-08-10 16:17:16', '2019-08-10 16:17:16'),
(6, 4, '552190810161716.jpg', 1, '2019-08-10 16:17:16', '2019-08-17 16:52:28'),
(7, 5, '50190811105519.JPG', 0, '2019-08-11 10:55:19', '2019-08-11 10:55:19'),
(8, 5, '331190811105519.JPG', 1, '2019-08-11 10:55:19', '2019-08-12 16:33:19'),
(9, 5, '317190811105519.jpg', 1, '2019-08-11 10:55:19', '2019-08-12 15:52:13'),
(10, 5, '357190813161052.JPG', 0, '2019-08-13 16:10:53', '2019-08-13 16:10:53'),
(11, 5, '774190813161052.JPG', 0, '2019-08-13 16:10:53', '2019-08-13 16:10:53'),
(12, 5, '38190813161052.jpg', 0, '2019-08-13 16:10:53', '2019-08-13 16:10:53'),
(13, 5, '392190813161257.jpg', 0, '2019-08-13 16:12:57', '2019-08-13 16:12:57'),
(14, 5, '200190813161257.JPG', 0, '2019-08-13 16:12:57', '2019-08-13 16:12:57'),
(15, 5, '761190813161257.JPG', 0, '2019-08-13 16:12:57', '2019-08-13 16:12:57'),
(16, 5, '527190813161257.jpg', 1, '2019-08-13 16:12:57', '2019-08-13 16:13:04'),
(17, 12, '433190818114459.JPG', 0, '2019-08-18 11:44:59', '2019-08-18 11:44:59'),
(18, 12, '70190818114459.JPG', 0, '2019-08-18 11:44:59', '2019-08-18 11:44:59'),
(19, 13, '656190818121652.JPG', 1, '2019-08-18 12:16:52', '2019-08-18 12:17:25'),
(20, 13, '983190818121652.JPG', 0, '2019-08-18 12:16:52', '2019-08-18 12:16:52');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(255) NOT NULL,
  `address` varchar(555) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `name`, `email_id`, `mobile_number`, `address`, `created_at`, `updated_at`, `status`) VALUES
(1, 'sushant pradhan', 'pradhansushant19@gmail.com', '7977066345', '123\r\ntest', '2019-09-07 16:27:23', '2019-09-07 16:27:23', 0),
(2, 'umesh', NULL, '456798', NULL, '2019-09-07 16:43:03', '2019-09-07 16:43:03', 0),
(3, 'umesh', NULL, '456798', NULL, '2019-09-07 16:43:24', '2019-09-07 16:43:24', 0),
(4, 'sushant pradhan', 'sushant.pradhan5@gmail.com', '08097034337', '3 samrat  ashok nagar kalva thane', '2020-07-18 07:12:15', '2020-07-18 07:12:15', 0),
(5, 'sushant pradhan', 'sushant.pradhan5@gmail.com', '08097034337', '3 samrat  ashok nagar kalva thane', '2020-07-18 07:12:16', '2020-07-18 07:12:16', 0),
(6, 'sushant pradhan', 'sushant.pradhan5@gmail.com', '08097034337', '3 samrat  ashok nagar kalva thane', '2020-07-18 07:12:46', '2020-07-18 07:12:46', 0);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `sub_title` varchar(255) NOT NULL,
  `description` varchar(5000) NOT NULL,
  `vedio_links` varchar(1000) DEFAULT NULL,
  `drive_link` varchar(255) DEFAULT NULL,
  `display_image` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `sub_title`, `description`, `vedio_links`, `drive_link`, `display_image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Tilak Nagar Bldg. Map .. Yes You have it now', 'Download and share !!', 'Ever got confused with Bldg. numbers and its location. Finally we have a detailed map of our society.\r\n\r\nTo download the map just click on the link below ..', NULL, NULL, '274190824142321.jpg', 0, '2019-08-24 14:23:21', '2019-08-24 14:23:21'),
(2, 'Complaints & Grieviences - MHADA', 'Online activity !!', 'Do you know you can lodge a complaint & grieviences online to respective MHADA??\r\n\r\nFollow these following steps to lodge it online to MHADA..', NULL, NULL, '965190824142400.jpg', 0, '2019-08-24 14:24:00', '2019-08-24 14:24:00'),
(3, 'Complaints & Grieviences - BMC', 'Online activity !!', 'Do you know you can lodge a complaint & grieviences online to respective Bombay Muncipal Corporation-BMC??\r\n\r\nFollow these following steps to lodge it online to BMC..', NULL, NULL, '341190824142431.jpg', 0, '2019-08-24 14:24:31', '2019-08-24 14:24:31'),
(4, 'Tilak Nagar festival 1st Jan to 3rd Jan 2016', 'info related to festival !!', 'TNRWA plans to shed all boundaries and bring all men, women, children & elders to a common platform that will cover sports, music and many more events\r\n\r\nFind forms and ads related to the event..', NULL, NULL, '370190824142501.jpg', 0, '2019-08-24 14:25:01', '2019-08-24 14:25:01'),
(5, 'Tilak Nagar festival January 2018', 'info related to festival !!', 'Tilak Nagar Residents Welfare Association Proudly presents \"Tilak Nagar FESTIVAL\". This festival wil host activities in brief : Sports - Culture - Music - Art - Talent & much more.\r\n\r\nFind forms and ads related to the event..', NULL, NULL, '835190824142549.jpeg', 0, '2019-08-24 14:25:49', '2019-08-24 14:25:49'),
(6, 'Cleanliness Drive September - 2018', 'info related to cleanliness drive !!', 'At the instance of Central Railway Cleanliness drive initiated by TNRWA with the assistance of residents of Tilak Nagar and Jyesth Nagrik Sangha and Samuha as well. The motto of TNRWA is \" Clean,Green and Ideal Colony\" It is also dream of TNRWA to make Tilak Nagar Station as a Smart Station in Mumbai and Tilak Nagar as Smart Colony as well.\r\n\r\nCheckout our Gallery and Events for more details related to the event..', NULL, NULL, '633190824142632.jpg', 0, '2019-08-24 14:26:32', '2019-08-24 14:26:32'),
(7, 'Inaugrated : TNRWA Meeting HUB', 'November-2018', 'TNRWA meeting Hub inaugurated on 1st November,2018 opp bldg No.19. It gives TNRWA immense pleasure in inagrauting our first \"Public Meeting Place\" called TNRWA HUB. TNRWA is proud to have another feather in it\'s cap by being placed as always in the minds of the residence.', NULL, NULL, '96190824142716.jpg', 0, '2019-08-24 14:27:16', '2019-08-24 14:27:16'),
(8, '100 Tilak Nagar buildings gets MHADA notice', 'January-2019', 'The Maharashtra Housing and Area Development Authority (MHADA) has issued notices to 100-odd residential buildings in Tilak Nagar, asking them to comply with fire safety norms and secure occupation certificates (OCs), in the wake of the fire in the area in December that killed five people.\r\n\r\nFor more details check the article link button \"Click here\" below.', NULL, NULL, '178190824142745.jpg', 0, '2019-08-24 14:27:45', '2019-08-24 14:27:45'),
(9, 'YOGA is the key for a happy Life!!', 'January-2019', '* TNRWA FEST* \r\nOur start of the new year Festival has begun and its gained lot of entries from our Tilak nagar. Some schedule to attend or best case PARTICIPATE.\r\nSchedule for 19-JAN event:- At Lokmanya Tilak Kridangan, Open Theater :- \r\n1. Yoga Camp morning 7 am. \r\n2. Physiotherapy session at 9 am. \r\n3. Medical Camp at 9.30 am.\r\n\r\nFor festival images and updates please click the button \"Click here\" below.', NULL, '1y7Pc73jZPevePy8CrVSrR8wXIJ0peDix', '535190824142821.jpg', 0, '2019-08-24 14:28:21', '2019-08-24 16:38:21');

-- --------------------------------------------------------

--
-- Table structure for table `news_images`
--

CREATE TABLE `news_images` (
  `id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news_images`
--

INSERT INTO `news_images` (`id`, `news_id`, `image_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '953190810165855.jpg', 0, '2019-08-10 16:58:55', '2019-08-10 16:58:55'),
(2, 1, '526190810165855.jpg', 0, '2019-08-10 16:58:55', '2019-08-10 16:58:55'),
(3, 2, '706190811165735.JPG', 0, '2019-08-11 16:57:35', '2019-08-11 16:57:35'),
(4, 2, '880190811165735.JPG', 0, '2019-08-11 16:57:35', '2019-08-11 16:57:35'),
(5, 2, '474190811165735.jpg', 0, '2019-08-11 16:57:35', '2019-08-11 16:57:35'),
(6, 3, '641190811165959.JPG', 0, '2019-08-11 16:59:59', '2019-08-11 16:59:59'),
(7, 3, '454190811165959.JPG', 0, '2019-08-11 16:59:59', '2019-08-11 16:59:59'),
(8, 3, '634190811165959.jpg', 1, '2019-08-11 16:59:59', '2019-08-13 16:57:12'),
(9, 3, '937190813170044.jpg', 1, '2019-08-13 17:00:44', '2019-08-13 17:01:31'),
(10, 3, '0190813170044.JPG', 1, '2019-08-13 17:00:44', '2019-08-13 17:01:28'),
(11, 3, '501190813170122.jpg', 0, '2019-08-13 17:01:22', '2019-08-13 17:01:22'),
(12, 3, '707190813170122.JPG', 0, '2019-08-13 17:01:22', '2019-08-13 17:01:22'),
(13, 3, '434190813170122.JPG', 0, '2019-08-13 17:01:22', '2019-08-13 17:01:22'),
(14, 5, '210190814175650.jpg', 0, '2019-08-14 17:56:50', '2019-08-14 17:56:50'),
(15, 7, '331190818111958.JPG', 0, '2019-08-18 11:19:58', '2019-08-18 11:19:58'),
(16, 7, '819190818111958.jpg', 0, '2019-08-18 11:19:58', '2019-08-18 11:19:58'),
(17, 7, '286190818111958.JPG', 1, '2019-08-18 11:19:58', '2019-08-18 11:20:46'),
(18, 7, '897190818112115.JPG', 0, '2019-08-18 11:21:15', '2019-08-18 11:21:15'),
(19, 7, '176190818112115.JPG', 0, '2019-08-18 11:21:15', '2019-08-18 11:21:15'),
(20, 2, '690200630103546.jpg', 0, '2020-06-30 10:35:46', '2020-06-30 10:35:46'),
(21, 2, '365200630103546.jpg', 0, '2020-06-30 10:35:46', '2020-06-30 10:35:46');

-- --------------------------------------------------------

--
-- Table structure for table `real_estates`
--

CREATE TABLE `real_estates` (
  `id` int(11) NOT NULL,
  `owner_name` varchar(255) NOT NULL,
  `mobile_number` varchar(255) NOT NULL,
  `optional_number` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `address` varchar(1000) DEFAULT NULL,
  `tnrwa_member` varchar(255) DEFAULT NULL,
  `flat_address` varchar(1000) DEFAULT NULL,
  `flat_condition` varchar(255) DEFAULT NULL,
  `available_from` date DEFAULT NULL,
  `available_to` date DEFAULT NULL,
  `deposit` int(11) DEFAULT NULL,
  `rent` int(11) DEFAULT NULL,
  `maintenance_cleared_upto` varchar(255) DEFAULT NULL,
  `other_unclear_dues` varchar(255) DEFAULT NULL,
  `oc_recieved` varchar(255) DEFAULT NULL,
  `carpet_area` int(11) DEFAULT NULL,
  `build_up_area` int(11) DEFAULT NULL,
  `floor` int(11) DEFAULT NULL,
  `flat_specification` varchar(255) DEFAULT NULL,
  `preferred_tenant_type` varchar(255) DEFAULT NULL,
  `lift` varchar(255) DEFAULT NULL,
  `parking` varchar(255) DEFAULT NULL,
  `gas_pipeline` varchar(255) DEFAULT NULL,
  `cctv` varchar(255) DEFAULT NULL,
  `water_supply` varchar(255) DEFAULT NULL,
  `distance_from_nearest_railway_station` varchar(255) DEFAULT NULL,
  `vastu` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'pending' COMMENT '0=pending;1=active;2= rejected;3=closed',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `real_estates`
--

INSERT INTO `real_estates` (`id`, `owner_name`, `mobile_number`, `optional_number`, `type`, `address`, `tnrwa_member`, `flat_address`, `flat_condition`, `available_from`, `available_to`, `deposit`, `rent`, `maintenance_cleared_upto`, `other_unclear_dues`, `oc_recieved`, `carpet_area`, `build_up_area`, `floor`, `flat_specification`, `preferred_tenant_type`, `lift`, `parking`, `gas_pipeline`, `cctv`, `water_supply`, `distance_from_nearest_railway_station`, `vastu`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Surendra Pradhan', '8097034337', '7977066345', '', '123,abc', 'no', '123,xyz', 'Unfurnished', '0000-00-00', '0000-00-00', 50000, 4000, '1000', '200', 'no', 650, 850, 2, '1 BHK', 'Family', 'no', 'no', 'no', 'no', 'no', '1 km', 'North', 'test', 'active', '2020-06-30 05:09:00', '2020-07-01 15:13:30'),
(2, 'john snow', '7977066345', '7977066345', 'rent', '123,abc', 'yes', '123,xyz', 'furnished', '0000-00-00', '0000-00-00', 50000, 4000, '1000', '200', 'yes', 650, 850, 2, '1 BHK', 'Batchlors', 'yes', 'yes', 'no', 'no', 'no', '1 km', 'West', 'test', 'active', '2020-06-30 10:35:46', '2020-07-19 06:31:55'),
(3, 'john snow', '7977066345', '7977066345', 'sale', '123,abc', 'yes', '123,xyz', 'furnished', '0000-00-00', '0000-00-00', 50000, 4000, '1000', '200', 'yes', 650, 850, 2, '1 BHK', 'Batchlors', 'yes', 'yes', 'no', 'no', 'no', '1 km', 'East', 'test', 'active', '2020-06-30 10:36:18', '2020-07-19 06:31:36'),
(4, 'john snow', '7977066345', '7977066345', 'sale', '123,abc', 'yes', '123,xyz', 'furnished', '0000-00-00', '0000-00-00', 50000, 4000, '1000', '200', 'yes', 650, 850, 2, '1 BHK', 'Batchlors', 'yes', 'yes', 'no', 'no', 'no', '1 km', 'East', 'test', 'active', '2020-06-30 10:36:38', '2020-07-19 06:30:41'),
(5, 'jayesh kolekar', '8652707086', '7977066345', 'sale', '123,abc', 'no', '123,xyz', 'Semi Furnished', '2020-06-30', '2020-06-30', 50000, 2000, '1000', '200', 'yes', 650, 850, 2, '3 BHK', 'Family', 'no', 'no', 'no', 'no', 'no', '1 km', 'West', 'test test', 'active', '2020-06-30 10:55:13', '2020-07-19 06:30:39'),
(6, 'Surendra Pradhan', '7977066345', '7977066345', 'sale', '123,abc', 'no', '123,xyz', 'Semi Furnished', '2020-07-01', '2020-07-01', 50000, 4000, '1000', '200', 'yes', 650, 850, 2, '1 BHK', 'Batchlors', 'yes', 'yes', 'yes', 'yes', 'yes', '1 km', 'South', 'test', 'active', '2020-07-01 15:11:36', '2020-07-19 06:30:28'),
(7, 'Surendra Pradhan', '8097034337', '7977066345', 'rent', '123,abc', 'no', '123,xyz', 'furnished', '2020-07-18', '2020-08-29', 50000, 4000, '1000', '200', 'no', 650, 850, 2, '1 BHK', 'Batchlors', 'no', 'no', 'no', 'no', 'no', '1 km', 'West', 'test', 'active', '2020-07-18 06:18:25', '2020-07-19 06:31:45'),
(8, 'sushant pradhan', '08097034337', '7977066345', 'sale', '3 samrat  ashok nagar kalva thane', 'no', '123,xyz', 'Unfurnished', '2020-07-19', '2020-07-19', 50000, 4000, '1000', '200', 'no', 650, 850, 2, '1 RK', 'Family', 'no', 'no', 'no', 'no', 'no', '1 km', 'East', 'test', 'pending', '2020-07-19 06:52:07', '2020-07-19 06:52:07'),
(9, 'sushant pradhan', '08097034337', '7977066345', 'sale', '3 samrat  ashok nagar kalva thane', 'no', '123,xyz', 'Unfurnished', '2020-07-19', '2020-07-19', 50000, 4000, '1000', '200', 'no', 650, 850, 2, '1 RK', 'Family', 'no', 'no', 'no', 'no', 'no', '1 km', 'East', 'test', 'pending', '2020-07-19 06:53:33', '2020-07-19 06:53:33'),
(10, 'sushant pradhan', '07977066345', NULL, 'sale', '123', 'no', '123,abc apartment, xyz nagar, kalva (w)-400605', 'Unfurnished', '2020-07-19', '2020-07-19', 50000, 4000, NULL, NULL, 'no', NULL, NULL, NULL, '1 RK', 'Family', 'no', 'no', 'no', 'no', 'no', NULL, 'East', NULL, 'pending', '2020-07-19 07:01:37', '2020-07-19 07:01:37');

-- --------------------------------------------------------

--
-- Table structure for table `real_estate_images`
--

CREATE TABLE `real_estate_images` (
  `id` int(11) NOT NULL,
  `real_estate_id` int(11) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `real_estate_images`
--

INSERT INTO `real_estate_images` (`id`, `real_estate_id`, `image_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 4, '510200630103638.jpg', 0, '2020-06-30 10:36:38', '2020-06-30 10:36:38'),
(2, 4, '827200630103638.jpg', 0, '2020-06-30 10:36:38', '2020-06-30 10:36:38'),
(3, 5, '20200630105513.jpg', 0, '2020-06-30 10:55:13', '2020-06-30 10:55:13'),
(4, 5, '332200630105513.jpg', 0, '2020-06-30 10:55:13', '2020-06-30 10:55:13'),
(5, 6, '759200701151136.jpg', 0, '2020-07-01 15:11:36', '2020-07-01 15:11:36'),
(6, 6, '714200701151136.jpg', 0, '2020-07-01 15:11:36', '2020-07-01 15:11:36'),
(7, 7, '225200718061825.jpg', 0, '2020-07-18 06:18:25', '2020-07-18 06:18:25'),
(8, 7, '835200718061825.jpg', 0, '2020-07-18 06:18:25', '2020-07-18 06:18:25'),
(9, 7, '411200718061825.jpg', 0, '2020-07-18 06:18:25', '2020-07-18 06:18:25'),
(10, 8, '42200719065207.jpg', 0, '2020-07-19 06:52:07', '2020-07-19 06:52:07'),
(11, 8, '840200719065207.jpg', 0, '2020-07-19 06:52:07', '2020-07-19 06:52:07'),
(12, 8, '265200719065207.jpg', 0, '2020-07-19 06:52:07', '2020-07-19 06:52:07'),
(13, 9, '765200719065333.jpg', 0, '2020-07-19 06:53:33', '2020-07-19 06:53:33'),
(14, 9, '304200719065333.jpg', 0, '2020-07-19 06:53:33', '2020-07-19 06:53:33'),
(15, 9, '248200719065333.jpg', 0, '2020-07-19 06:53:33', '2020-07-19 06:53:33'),
(16, 10, '591200719070137.jpg', 0, '2020-07-19 07:01:37', '2020-07-19 07:01:37'),
(17, 10, '785200719070137.jpg', 0, '2020-07-19 07:01:37', '2020-07-19 07:01:37'),
(18, 10, '247200719070137.jpg', 0, '2020-07-19 07:01:37', '2020-07-19 07:01:37');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(255) NOT NULL,
  `description` varchar(555) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `email_id`, `mobile_number`, `description`, `created_at`, `updated_at`, `status`) VALUES
(1, 'sushant pradhan', 'pradhansushant19@gmail.com', '7977066345', 'test desc', '2019-09-07 16:39:26', '2019-09-07 16:39:26', 0),
(2, 'umesh', NULL, '9876544567', NULL, '2019-09-07 16:41:56', '2019-09-07 16:41:56', 0),
(3, 'testx abc', 'sush@gmail.com', '8652707086', 'test', '2019-09-07 16:45:34', '2019-09-07 16:45:34', 0),
(4, 'testx abc', 'sush@gmail.com', '8652707086', 'test', '2019-09-07 16:45:53', '2019-09-07 16:45:53', 0),
(5, 'sushant pradhan', 'sushant.pradhan5@gmail.com', '08097034337', NULL, '2020-07-18 07:13:07', '2020-07-18 07:13:07', 0),
(6, 'sushant pradhan', 'sushant.pradhan5@gmail.com', '08097034337', NULL, '2020-07-18 07:13:12', '2020-07-18 07:13:12', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `mobile_number` varchar(11) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_type` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `mobile_number`, `email_id`, `password`, `user_type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', '0123456789', 'admin@gmail.com', '$2y$10$hFQleJu1ujhqVgQxvfUAb.g85fQ.CDhprfceFE7oRlkQZFXSpRlp6', 0, 0, '2019-08-08 17:40:02', '2019-08-12 16:19:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_images`
--
ALTER TABLE `event_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_images`
--
ALTER TABLE `news_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `real_estates`
--
ALTER TABLE `real_estates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `real_estate_images`
--
ALTER TABLE `real_estate_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `event_images`
--
ALTER TABLE `event_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `news_images`
--
ALTER TABLE `news_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `real_estates`
--
ALTER TABLE `real_estates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `real_estate_images`
--
ALTER TABLE `real_estate_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
